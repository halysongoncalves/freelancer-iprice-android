package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.domain.LocationClient;
import br.com.iprice.domain.LocationClientImpl;
import br.com.iprice.presentation.presenter.HomePresenter;
import br.com.iprice.presentation.presenter.HomePresenterImpl;
import br.com.iprice.presentation.ui.fragment.ExploreFragment_;
import br.com.iprice.presentation.ui.fragment.FeedFragment_;
import br.com.iprice.presentation.ui.fragment.SearchFragment_;
import br.com.iprice.presentation.ui.fragment.SellerFragment_;
import br.com.iprice.presentation.ui.fragment.WishlistFragment_;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity implements HomeView {
    private static final int REQUEST_CODE = 3435;
    private static final int REQUEST_CHECK_SETTINGS = 5321;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @ViewById(R.id.activity_home_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.activity_home_drawer_layout)
    DrawerLayout drawerLayout;

    @ViewById(R.id.activity_home_navigation_view)
    NavigationView navigationView;

    @ViewById(R.id.activity_home_progress)
    ProgressBar progressBar;

    @ViewById(R.id.activity_home_container)
    View viewContent;

    @Bean(HomePresenterImpl.class)
    HomePresenter homePresenter;

    @OptionsMenuItem(R.id.menu_search_search_view)
    MenuItem menuItemSearch;

    @Bean(LocationClientImpl.class)
    LocationClient locationClient;

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Status status;

    @AfterViews
    void afterViews() {
        homePresenter.attachView(this);
    }

    @Override
    public void setupToolbar() {
        setSupportActionBar(toolbar);
    }

    @Override
    public void connectLocation() {
        locationClient.setConnectionCallback(this);
        locationClient.connect();
    }

    @Override
    public void setTitleToolbar(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @OnActivityResult(REQUEST_CODE)
    void onResultProfile(int resultCode) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                redirectToFeed();
                break;
            default:
                break;
        }
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public NavigationView getNavigationView() {
        return navigationView;
    }

    @Override
    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    @Override
    public void setupNavigationView(NavigationView navigationView, DrawerLayout drawerLayout) {
        navigationView.setNavigationItemSelectedListener(this);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.application_name,
                R.string.application_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    public void fragmentTransaction(final Fragment fragment, final int fragmentName) {
        String title = getString(fragmentName);

        setTitleToolbar(title);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.activity_home_container, fragment, title)
                .commit();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            return actionBarDrawerToggle.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);

        if (menuItem.isChecked()) {
            return true;
        }

        switch (menuItem.getItemId()) {
            case R.id.menu_navigation_view_item_feed:
                fragmentTransaction(FeedFragment_.builder().build(), R.string.fragment_feed_title);
                setChecked(menuItem);
                break;

            case R.id.menu_navigation_view_item_seller:
                fragmentTransaction(SellerFragment_.builder().build(), R.string.fragment_seller_title);
                setChecked(menuItem);
                break;

            case R.id.menu_navigation_view_item_search:
                fragmentTransaction(SearchFragment_.builder().build(), R.string.fragment_search_title);
                setChecked(menuItem);
                break;

            case R.id.menu_navigation_view_item_wish_list:
                fragmentTransaction(WishlistFragment_.builder().build(), R.string.fragment_wish_list_title);
                setChecked(menuItem);
                break;

            case R.id.menu_navigation_view_item_explore:
                fragmentTransaction(ExploreFragment_.builder().build(), R.string.fragment_explore_title);
                setChecked(menuItem);
                break;

            case R.id.menu_navigation_view_item_my_points:
                MyPointsActivity_.intent(this).start();
                break;

            case R.id.menu_navigation_view_item_profile:
                ProfileActivity_.intent(this).startForResult(REQUEST_CODE);
                break;

            case R.id.menu_navigation_view_item_sort:
                SortActivity_.intent(this).start();
                break;

            case R.id.menu_navigation_view_item_exit:
                homePresenter.logout();
                LoginActivity_.intent(HomeActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                break;

            default:
                break;
        }

        return false;
    }

    private void setChecked(MenuItem menuItem) {
        menuItem.setChecked(true);
    }

    @Override
    public void showDialogTryAgain(String message) {
    }

    @Override
    public void showDialogConnection() {
    }

    @Override
    public void showDialogUnauthorized(String message) {
    }

    @Override
    public void hideDialog() {
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
    }

    @Override
    public void redirectToFeed() {
        navigationView.getMenu().getItem(0).setChecked(true);
        fragmentTransaction(FeedFragment_.builder().build(), R.string.fragment_feed_title);
    }

    @Override
    public void redirectToSearch() {
        navigationView.getMenu().getItem(2).setChecked(true);
        fragmentTransaction(SearchFragment_.builder().build(), R.string.fragment_search_title);
    }

    @OnActivityResult(REQUEST_CHECK_SETTINGS)
    void onResultGpsEnabled(int resultCode) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                locationClient.startUpdateLocation();
                break;

            default:
                showDialogEnableGps(status);
                break;
        }
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showContent() {
        viewContent.setVisibility(VISIBLE);
    }

    @Override
    public void onConnected(double latitude, double longitude) {
        hideProgress();
        fragmentTransaction(FeedFragment_.builder().build(), R.string.fragment_feed_title);
        showContent();
    }

    @Override
    public void onConnectionFailed() {
    }

    @Override
    public void onResult(Status status) {
        this.status = status;

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                locationClient.startUpdateLocation();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                showDialogEnableGps(status);
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                showDialogEnableGps(status);
                break;
        }
    }

    private void showDialogEnableGps(Status status) {
        try {
            if (status != null) {
                status.startResolutionForResult(this, REQUEST_CHECK_SETTINGS);
            }
        } catch (IntentSender.SendIntentException sendIntentException) {
            Log.i(RegisterActivity.class.getSimpleName(), "PendingIntent unable to execute request.");
        }
    }
}

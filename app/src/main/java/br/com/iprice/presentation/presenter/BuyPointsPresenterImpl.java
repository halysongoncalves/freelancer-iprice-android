package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.MyPointsService;
import br.com.iprice.domain.MyPointsServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.event.InvalidQuantityBuyPointsEvent;
import br.com.iprice.model.event.PointsUpdatedEvent;
import br.com.iprice.model.event.ValidQuantityBuyPointsEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.activity.BuyPointsView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class BuyPointsPresenterImpl implements BuyPointsPresenter {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(MyPointsServiceImpl.class)
    MyPointsService myPointsService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    private BuyPointsView buyPointsView;

    @Override
    public void attachView(BuyPointsView buyPointsView, float valueInitial) {
        this.buyPointsView = buyPointsView;
        this.buyPointsView.setupToolbar();
        this.buyPointsView.showMyPoints();
        this.buyPointsView.showValueTotal(valueInitial);
    }

    @Override
    public void registerBus() {
        requestErrorService.registerBus();
        myPointsService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        requestErrorService.unregisterBus();
        myPointsService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void validateBuyPoints(String points) {
        buyPointsView.showDialogValidateBuyPoints();
        myPointsService.validateBuyPoints(points);
    }

    @Override
    public void updatePoints(String points) {
        buyPointsView.showDialogSave();
        myPointsService.addNewPoints(points);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadInvalidQuantityBuyPointsEvent(InvalidQuantityBuyPointsEvent invalidQuantityBuyPointsEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showInvalidPoints();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadValidQuantityBuyPointsEvent(ValidQuantityBuyPointsEvent validQuantityBuyPointsEvent) {
        buyPointsView.hideDialog();
        buyPointsView.redirectToPayPal(validQuantityBuyPointsEvent.getBuyPoints());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPointsUpdatedEvent(PointsUpdatedEvent pointsUpdatedEvent) {
        buyPointsView.hideDialog();
        buyPointsView.redirectToMyPoints(pointsUpdatedEvent.getPointsVO().getPoint());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        buyPointsView.hideDialog();
        userService.logout();
        buyPointsView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        buyPointsView.hideDialog();
        buyPointsView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

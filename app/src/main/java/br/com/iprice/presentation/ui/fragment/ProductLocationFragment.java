package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.InstanceState;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_product_location)
public class ProductLocationFragment extends Fragment implements ProductLocationView {
    private static final int ZOOM = 15;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @FragmentArg(FeedFragment.EXTRA_PRODUCT)
    @InstanceState
    ProductVO productVO;

    @AfterViews
    void afterViews() {
        setupGoogleMaps();
    }

    @Override
    public void setupGoogleMaps() {
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_product_location_support_map)).getMapAsync(this);
    }

    @Override
    public void showDialogTryAgain(String message) {
    }

    @Override
    public void showDialogConnection() {
    }

    @Override
    public void showDialogUnauthorized(String message) {
    }

    @Override
    public void hideDialog() {
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);

        if ((productVO != null) && (productVO.getLocation() != null)) {
            double[] location = productVO.getLocation();
            LatLng latLng = new LatLng(location[0], location[1]);

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM));
            googleMap.addMarker(new MarkerOptions()
                    .position(latLng));
        }
    }


}

package br.com.iprice.presentation.ui.activity;

import android.support.annotation.DrawableRes;

import java.util.List;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.views.BaseActivityView;
import br.com.iprice.presentation.ui.views.ViewWrapper;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ProfileView extends BaseActivityView, ViewWrapper.OnItemClickListener {
    void setupRecyclerView();

    void showPhoto(String photoUrl);

    void hideProgress();

    void showContentData();

    void showName(String name);

    void showNumberPost(int post);

    void showFlag(@DrawableRes int flag);

    void showCategory(String category);

    void insertAllProductsVO(List<ProductVO> productVOs);

    void showRecyclerView();

    void showEmptyState();

    void showDialogLoading();

    void hideRecyclerView();
}

package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.viewholder.FeedViewHolder;
import br.com.iprice.presentation.ui.viewholder.FeedViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class FeedAdapter extends BaseRecyclerViewAdapter<ProductVO, FeedViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected FeedViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return FeedViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ProductVO, FeedViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }

}
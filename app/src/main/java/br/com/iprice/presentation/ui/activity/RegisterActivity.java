package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.io.File;

import br.com.iprice.R;
import br.com.iprice.domain.LocationClient;
import br.com.iprice.domain.LocationClientImpl;
import br.com.iprice.presentation.presenter.RegisterPresenter;
import br.com.iprice.presentation.presenter.RegisterPresenterImpl;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A login screen that offers login via email/password.
 */
@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity implements RegisterView {
    public static final int REQUEST_CODE_REGISTER = 4332;
    private static final int REQUEST_CODE_CHOICE_IMAGE_FROM_GALLERY = 3253;
    private static final int REQUEST_CHECK_SETTINGS = 5321;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @ViewById(R.id.activity_register_toolbar)
    Toolbar toolbar;
    @ViewById(R.id.activity_register_image_view_profile)
    CircleImageView circleImageView;
    @ViewById(R.id.activity_register_text_input_name)
    TextInputLayout textInputLayoutName;
    @ViewById(R.id.activity_register_text_input_email)
    TextInputLayout textInputLayoutEmail;
    @ViewById(R.id.activity_register_text_input_password)
    TextInputLayout textInputLayoutPassword;
    @ViewById(R.id.activity_register_edit_text_name)
    AppCompatEditText appCompatEditTextName;
    @ViewById(R.id.activity_register_edit_text_email)
    AppCompatEditText appCompatEditTextEmail;
    @ViewById(R.id.activity_register_edit_text_password)
    AppCompatEditText appCompatEditTextPassword;
    @Bean(RegisterPresenterImpl.class)
    RegisterPresenter registerPresenter;
    @Bean(LocationClientImpl.class)
    LocationClient locationClient;
    @InstanceState
    File file;
    private MaterialDialog materialDialog;
    private Status status;

    @AfterViews
    void afterViews() {
        registerPresenter.attachView(this);
    }

    @Override
    protected void onStart() {
        registerPresenter.registerBus();
        super.onStart();
    }

    @Override
    protected void onStop() {
        registerPresenter.unregisterBus();
        super.onStop();
    }

    @Override
    public void setupToolbar() {
        if (null != toolbar) {
            setSupportActionBar(toolbar);
        }
        if (null != getSupportActionBar()) {
            getSupportActionBar().setTitle(R.string.activity_register_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OptionsItem(android.R.id.home)
    void clickMenuHome() {
        finish();
    }

    @OnActivityResult(REQUEST_CODE_CHOICE_IMAGE_FROM_GALLERY)
    void onResultChoiceImageFromGallery(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                registerPresenter.selectedImageFromGallery(data.getData());
                break;
            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CHECK_SETTINGS)
    void onResultGpsEnabled(int resultCode) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                locationClient.startUpdateLocation();
                break;

            default:
                showDialogEnableGps(status);
                break;
        }
    }

    @Override
    public void clearMessageError() {
        textInputLayoutName.setErrorEnabled(false);
        textInputLayoutName.setError(null);

        textInputLayoutEmail.setErrorEnabled(false);
        textInputLayoutEmail.setError(null);

        textInputLayoutPassword.setErrorEnabled(false);
        textInputLayoutPassword.setError(null);
    }

    @Override
    public void showInvalidName() {
        textInputLayoutName.setError(getString(R.string.invalid_name));
    }

    @Override
    public void showInvalidEmail() {
        textInputLayoutEmail.setError(getString(R.string.invalid_email));
    }

    @Override
    public void showInvalidPassword() {
        textInputLayoutPassword.setError(getString(R.string.invalid_password));
    }

    @Override
    public void showDialogValidateUser() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_validate_data))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showDialogLoading() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_loading_data))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(RegisterActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void showDialogLoadImage() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_loading_data))
                .progress(true, 0)
                .show();
    }

    @Override
    public void showImageSelectedFromGallery(File file) {
        this.file = file;

        Drawable drawableCompat = ContextCompat.getDrawable(this, R.drawable.placeholder_user);
        Picasso.with(this.getApplicationContext()).load(file)
                .placeholder(R.drawable.placeholder_user)
                .resize(drawableCompat.getIntrinsicWidth(), drawableCompat.getIntrinsicHeight())
                .into(circleImageView);
    }

    @Override
    public void redirectToHome() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void connectLocation() {
        locationClient.setConnectionCallback(this);
        locationClient.connect();
    }

    @Override
    public void showDialogInvalidUser() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_email_already_exists))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Click(R.id.activity_register_button_register)
    void clickRegister() {
        validateFields();
    }

    @Click(R.id.activity_register_content_photo)
    void clickAddPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        }
        startActivityForResult(Intent.createChooser(intent, getString(R.string.activity_register_title_chooser)), REQUEST_CODE_CHOICE_IMAGE_FROM_GALLERY);
    }

    @Override
    public void onConnected(double latitude, double longitude) {
    }

    @Override
    public void onConnectionFailed() {
    }

    @Override
    public void onResult(Status status) {
        this.status = status;

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                locationClient.startUpdateLocation();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                showDialogEnableGps(status);
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                showDialogEnableGps(status);
                break;
        }
    }

    private void showDialogEnableGps(Status status) {
        try {
            if (status != null) {
                status.startResolutionForResult(this, REQUEST_CHECK_SETTINGS);
            }
        } catch (IntentSender.SendIntentException sendIntentException) {
            Log.i(RegisterActivity.class.getSimpleName(), "PendingIntent unable to execute request.");
        }
    }

    private void validateFields() {
        registerPresenter.validateFields(appCompatEditTextName.getText().toString(), appCompatEditTextEmail.getText().toString(),
                appCompatEditTextPassword.getText().toString(), file);
    }

}


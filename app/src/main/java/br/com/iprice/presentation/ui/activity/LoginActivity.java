package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;

import br.com.iprice.R;
import br.com.iprice.domain.LocationClient;
import br.com.iprice.domain.LocationClientImpl;
import br.com.iprice.presentation.presenter.LoginPresenter;
import br.com.iprice.presentation.presenter.LoginPresenterImpl;

/**
 * A login screen that offers login via email/password.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity implements LoginView {
    private static final int REQUEST_CHECK_SETTINGS = 5321;
    private static final int REQUEST_CODE_LOGIN_FACEBOOK = 64206;
    private static final String PROFILE = "public_profile";
    private static final String BIRTH_DAY = "user_birthday";
    private static final String EMAIL = "email";
    @ViewById(R.id.activity_login_text_input_email)
    TextInputLayout textInputLayoutEmail;
    @ViewById(R.id.activity_login_text_input_password)
    TextInputLayout textInputLayoutPassword;
    @ViewById(R.id.activity_login_edit_text_email)
    AppCompatEditText appCompatEditTextEmail;
    @ViewById(R.id.activity_login_edit_text_password)
    AppCompatEditText appCompatEditTextPassword;
    @Bean(LoginPresenterImpl.class)
    LoginPresenter loginPresenter;
    @Bean(LocationClientImpl.class)
    LocationClient locationClient;
    private Status status;
    private CallbackManager callbackManager;
    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        loginPresenter.attachView(this);
    }

    @Override
    protected void onStart() {
        loginPresenter.registerBus();
        super.onStart();
    }

    @Override
    protected void onStop() {
        loginPresenter.unregisterBus();
        super.onStop();
    }

    @OnActivityResult(REQUEST_CODE_LOGIN_FACEBOOK)
    void onResultFacebook(int resultCode, Intent data) {
        switch (resultCode) {
            default:
                callbackManager.onActivityResult(REQUEST_CODE_LOGIN_FACEBOOK, resultCode, data);
                break;
        }
    }

    @OnActivityResult(RegisterActivity.REQUEST_CODE_REGISTER)
    void onResultRegister(int resultCode) {
        switch (resultCode) {
            case RESULT_OK:
                redirectToHome();
                break;

            default:
                showDialogEnableGps(status);
                break;
        }
    }

    @OnActivityResult(REQUEST_CHECK_SETTINGS)
    void onResultGpsEnabled(int resultCode) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                locationClient.startUpdateLocation();
                break;

            default:
                showDialogEnableGps(status);
                break;
        }
    }

    @Override
    public void connectLocation() {
        locationClient.setConnectionCallback(this);
        locationClient.connect();
    }

    @Override
    public void setupFacebookSdk() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    @Override
    public void clearMessageError() {
        textInputLayoutEmail.setErrorEnabled(false);
        textInputLayoutEmail.setError(null);

        textInputLayoutPassword.setErrorEnabled(false);
        textInputLayoutPassword.setError(null);
    }


    @Override
    public void showInvalidEmail() {
        textInputLayoutEmail.setError(getString(R.string.invalid_email));
    }

    @Override
    public void showInvalidPassword() {
        textInputLayoutPassword.setError(getString(R.string.invalid_password));
    }

    @Override
    public void showDialogValidateUser() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_validate_data))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showDialogLoading() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_loading_data))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showTermsUse(String termsUse) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.activity_login_dialog_title_terms_use))
                .content(termsUse)
                .positiveText(getString(R.string.dialog_button_close))
                .positiveColorRes(android.R.color.black)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        hideDialog();
                    }
                })
                .show();
    }

    @Override
    public void redirectToHome() {
        HomeActivity_
                .intent(this)
                .start();
        finish();
    }

    @Override
    public void onConnected(double latitude, double longitude) {
    }

    @Override
    public void onConnectionFailed() {
    }

    @Override
    public void onResult(Status status) {
        this.status = status;

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                locationClient.startUpdateLocation();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                showDialogEnableGps(status);
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                showDialogEnableGps(status);
                break;
        }
    }

    @Override
    public void showDialogTryAgainFacebook() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_try_again)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList(PROFILE, EMAIL, BIRTH_DAY));
                    }
                })
                .show();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_try_again)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        loginPresenter.validateFields(appCompatEditTextEmail.getText().toString(), appCompatEditTextPassword.getText().toString());
                    }
                })
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(LoginActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Click(R.id.activity_login_button_get_int)
    void clickGetInt() {
        loginPresenter.validateFields(appCompatEditTextEmail.getText().toString(), appCompatEditTextPassword.getText().toString());
    }

    @Click(R.id.activity_login_text_view_terms_use)
    void clickTermsUse() {
        loginPresenter.recoverTermsUse(getResources().openRawResource(R.raw.terms_use));
    }

    @Click(R.id.activity_login_button_facebook)
    void clickGetInFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList(PROFILE, EMAIL, BIRTH_DAY));
    }

    @Click(R.id.activity_login_text_view_register)
    void clickRegister() {
        RegisterActivity_
                .intent(this)
                .startForResult(RegisterActivity.REQUEST_CODE_REGISTER);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        loginPresenter.recoverUserFacebook(AccessToken.getCurrentAccessToken());
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onError(FacebookException error) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(R.string.dialog_error_title)
                .content(R.string.dialog_error_message_generic)
                .positiveText(R.string.dialog_button_try_again)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList(PROFILE, EMAIL, BIRTH_DAY));
                    }
                })
                .show();
    }

    private void showDialogEnableGps(Status status) {
        try {
            if (status != null) {
                status.startResolutionForResult(this, REQUEST_CHECK_SETTINGS);
            }
        } catch (IntentSender.SendIntentException sendIntentException) {
            Log.i(RegisterActivity.class.getSimpleName(), "PendingIntent unable to execute request.");
        }
    }
}


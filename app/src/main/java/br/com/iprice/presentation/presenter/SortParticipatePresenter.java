package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.AllParticipateSortEmptyEvent;
import br.com.iprice.model.event.AllParticipateSortEvent;
import br.com.iprice.model.event.http.BadRequestSortParticipateEvent;
import br.com.iprice.model.event.http.ConvertErrorSortParticipateEvent;
import br.com.iprice.model.event.http.ForbiddenSortParticipateEvent;
import br.com.iprice.model.event.http.GenericErrorSortParticipateEvent;
import br.com.iprice.model.event.http.NetworkErrorSortParticipateEvent;
import br.com.iprice.model.event.http.NotFoundSortParticipateEvent;
import br.com.iprice.model.event.http.ServerErrorSortParticipateEvent;
import br.com.iprice.model.event.http.UnauthorizedSortParticipateEvent;
import br.com.iprice.presentation.ui.fragment.SortParticipateView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SortParticipatePresenter extends Bus {
    void attachView(SortParticipateView sortParticipateView);

    void recoverAllSortParticipate();

    void onLoadAllParticipateSortEvent(AllParticipateSortEvent allParticipateSortEvent);

    void onLoadAllParticipateSortEmptyEvent(AllParticipateSortEmptyEvent allParticipateSortEmptyEvent);

    void onLoadNetworkErrorSortParticipateEvent(NetworkErrorSortParticipateEvent networkErrorSortParticipateEvent);

    void onLoadConvertErrorSortParticipateEvent(ConvertErrorSortParticipateEvent convertErrorSortParticipateEvent);

    void onLoadGenericErrorSortParticipateEvent(GenericErrorSortParticipateEvent genericErrorSortParticipateEvent);

    void onLoadBadRequestSortParticipateEvent(BadRequestSortParticipateEvent badRequestSortParticipateEvent);

    void onLoadUnauthorizedSortParticipateEvent(UnauthorizedSortParticipateEvent unauthorizedSortParticipateEvent);

    void onLoadForbiddenSortParticipateEvent(ForbiddenSortParticipateEvent forbiddenSortParticipateEvent);

    void onLoadNotFoundSortParticipateEvent(NotFoundSortParticipateEvent notFoundSortParticipateEvent);

    void onLoadServerErrorSortParticipateEvent(ServerErrorSortParticipateEvent serverErrorSortParticipateEvent);
}

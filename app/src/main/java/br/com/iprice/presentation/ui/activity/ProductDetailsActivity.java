package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.LinkedList;
import java.util.List;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.SectionBeanVO;
import br.com.iprice.presentation.ui.adapter.TabLayoutAdapter;
import br.com.iprice.presentation.ui.fragment.FeedFragment;
import br.com.iprice.presentation.ui.fragment.ProductInfoFragment_;
import br.com.iprice.presentation.ui.fragment.ProductLocationFragment_;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EActivity(R.layout.activity_product_details)
public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsView {
    @ViewById(R.id.view_toolbar_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.activity_product_details_tab_layout)
    TabLayout tabLayout;

    @ViewById(R.id.activity_product_details_viewpager)
    ViewPager viewPager;

    @Extra(FeedFragment.EXTRA_PRODUCT)
    @InstanceState
    ProductVO productVO;

    @AfterViews
    void afterViews() {
        setupToolbar();
        setupViewPager(getViewPager(), recoverSectionsFragment(productVO));
        setupTabLayout(getTabLayout(), getViewPager());
    }

    @Override
    public void setupToolbar() {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.activity_product_details_title);
        }
    }

    @Override
    public void setupViewPager(final ViewPager viewPager, final TabLayoutAdapter tabLayoutAdapter) {
        viewPager.setAdapter(tabLayoutAdapter);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    public void setupTabLayout(TabLayout tabLayout, ViewPager viewPager) {
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public TabLayoutAdapter recoverSectionsFragment(ProductVO productVO) {
        List<SectionBeanVO> sectionBeanVOList = new LinkedList<>();
        sectionBeanVOList.add(new SectionBeanVO(getString(R.string.fragment_product_info_title),
                ProductInfoFragment_.builder()
                        .arg(FeedFragment.EXTRA_PRODUCT, productVO)
                        .build()));

        sectionBeanVOList.add(new SectionBeanVO(getString(R.string.fragment_product_location_title),
                ProductLocationFragment_.builder()
                        .arg(FeedFragment.EXTRA_PRODUCT, productVO)
                        .build()));

        return new TabLayoutAdapter(getSupportFragmentManager(), sectionBeanVOList);
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewPager;
    }

    @OptionsItem(android.R.id.home)
    void clickMenuHome() {
        finish();
    }

    @Override
    public void showDialogTryAgain(String message) {
    }

    @Override
    public void showDialogConnection() {
    }

    @Override
    public void showDialogUnauthorized(String message) {
    }

    @Override
    public void hideDialog() {
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
    }
}

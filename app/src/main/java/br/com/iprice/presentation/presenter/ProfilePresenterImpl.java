package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.ProfileService;
import br.com.iprice.domain.ProfileServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.RecoverMyPostEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.activity.ProfileView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class ProfilePresenterImpl implements ProfilePresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(ProfileServiceImpl.class)
    ProfileService profileService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    private ProfileView profileView;

    @Override
    public void attachView(ProfileView profileView) {
        this.profileView = profileView;
        this.profileView.setupRecyclerView();
        this.profileView.setupToolbar();
    }

    @Override
    public void recoverMyPost() {
        userService.recoverMyPost();
    }

    @Override
    public void deleteProduct(ProductVO productVO) {
        profileView.showDialogLoading();
        profileService.deleteProduct(productVO);
    }

    @Override
    public void registerBus() {
        profileService.registerBus();
        requestErrorService.registerBus();
        userService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        profileService.unregisterBus();
        requestErrorService.unregisterBus();
        userService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRecoverMyPostEvent(RecoverMyPostEvent recoverMyPostEvent) {
        profileView.hideDialog();
        UserVO userVO = recoverMyPostEvent.getUserVO();

        if (userVO != null) {
            if (userVO.getPhotoUrl() != null) {
                profileView.showPhoto(userVO.getPhotoUrl());
            }

            if (userVO.getName() != null) {
                profileView.showName(userVO.getName());
            }

            if (userVO.getProductVOs() != null && !userVO.getProductVOs().isEmpty()) {
                profileView.insertAllProductsVO(userVO.getProductVOs());
                profileView.showRecyclerView();
            } else {
                profileView.showEmptyState();
                profileView.hideRecyclerView();
            }

            profileView.showPhoto(userVO.getPhotoUrl());
            profileView.showNumberPost(userVO.getPosts());
            profileView.showFlag(userVO.getFlag());
            profileView.showCategory(userVO.getCategory());
        }

        profileView.hideProgress();
        profileView.showContentData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        profileView.hideDialog();
        profileView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        profileView.hideDialog();
        profileView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        profileView.hideDialog();
        profileView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        profileView.hideDialog();
        profileView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        profileView.hideDialog();
        userService.logout();
        profileView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        profileView.hideDialog();
        profileView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        profileView.hideDialog();
        profileView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        profileView.hideDialog();
        profileView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

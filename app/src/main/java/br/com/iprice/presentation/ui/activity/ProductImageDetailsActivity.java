package br.com.iprice.presentation.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EActivity(R.layout.activity_product_image_details)
public class ProductImageDetailsActivity extends AppCompatActivity implements ProductImageDetailsView {
    public static final String EXTRA_URL_IMAGE = "extra_url_image";

    @Extra(EXTRA_URL_IMAGE)
    String url;

    @ViewById(R.id.activity_product_image_details_image_view_image)
    AppCompatImageView appCompatImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void afterViews() {
        Picasso.with(getBaseContext())
                .load(url)
                .placeholder(R.drawable.placeholder_image)
                .into(appCompatImageView);
    }


}

package br.com.iprice.presentation.ui.activity;

import android.graphics.Bitmap;

import java.io.File;

import br.com.iprice.presentation.ui.views.BaseActivityView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SellerDetailsView extends BaseActivityView {
    void showDialogValidate();

    void showOnePicture(Bitmap bitmap);

    void showOnePicture(File fileImageOne);

    void showTwoPicture(Bitmap bitmapImageTwo);

    void showTwoPicture(File fileImageTwo);

    void showThreePicture(Bitmap bitmapImageThree);

    void showThreePicture(File fileImageThree);

    void showFourPicture(Bitmap bitmapImageFour);

    void showFourPicture(File fileImageFour);

    void showFivePicture(Bitmap bitmapImageFive);

    void showFivePicture(File fileImageFive);

    void clearMessageError();

    void showInvalidName();

    void showInvalidPhone();

    void showInvalidPrice();

    void showInvalidMessage();

    void showImageInvalid();

    void showDialogLoading();

    void redirectToFeed();

    void showMessageAddProductSuccess();
}

package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import br.com.iprice.R;
import br.com.iprice.presentation.presenter.SplashPresenter;
import br.com.iprice.presentation.presenter.SplashPresenterImpl;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity implements SplashView {
    @Bean(SplashPresenterImpl.class)
    SplashPresenter splashPresenter;

    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        splashPresenter.registerBus();
        splashPresenter.attachView(this);
        splashPresenter.userIsValid();
    }

    @Override
    protected void onDestroy() {
        splashPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_try_again)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        splashPresenter.userIsValid();
                    }
                })
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(SplashActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    @UiThread(delay = 300)
    public void redirectToLogin() {
        LoginActivity_
                .intent(this)
                .start();
        finish();
    }

    @Override
    @UiThread(delay = 300)
    public void redirectToHome() {
        HomeActivity_
                .intent(this)
                .start();
        finish();
    }
}

package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.presentation.ui.viewholder.CommentsViewHolder;
import br.com.iprice.presentation.ui.viewholder.CommentsViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class CommentsAdapter extends BaseRecyclerViewAdapter<CommentVO, CommentsViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected CommentsViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return CommentsViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<CommentVO, CommentsViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }
}
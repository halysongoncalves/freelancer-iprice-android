package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.presenter.SearchPresenter;
import br.com.iprice.presentation.presenter.SearchPresenterImpl;
import br.com.iprice.presentation.ui.activity.LoginActivity_;
import br.com.iprice.presentation.ui.activity.ProductDetailsActivity_;
import br.com.iprice.presentation.ui.adapter.SearchAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static br.com.iprice.presentation.ui.fragment.FeedFragment.EXTRA_PRODUCT;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_search)
public class SearchFragment extends Fragment implements SearchView {
    @ViewById(R.id.fragment_search_recycler_view)
    RecyclerView recyclerView;

    @ViewById(R.id.fragment_search_progress)
    ProgressBar progressBar;

    @ViewById(R.id.fragment_search_text_view_empty)
    AppCompatTextView appCompatTextViewEmpty;

    @Bean(SearchPresenterImpl.class)
    SearchPresenter searchPresenter;

    @Bean
    SearchAdapter searchAdapter;

    private MaterialDialog materialDialog;
    private boolean blockSearch = false;

    @AfterViews
    void afterViews() {
        setHasOptionsMenu(true);
        searchPresenter.registerBus();
        searchPresenter.attachView(this);
    }

    @Override
    public void onDestroy() {
        searchPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        final MenuItem searchItem = menu.findItem(R.id.menu_search_search_view);
        final android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(SearchFragment.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void showEmptyState() {
        appCompatTextViewEmpty.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void insertAllProducts(List<ProductVO> productVOList) {
        searchAdapter.clear();
        searchAdapter.addAll(productVOList);
    }

    @Override
    public void hideEmptyState() {
        appCompatTextViewEmpty.setVisibility(GONE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void freeSearch() {
        blockSearch = false;
    }

    @Override
    public void showRecyclerView() {
        recyclerView.setVisibility(VISIBLE);
    }

    @Override
    public void setupRecyclerView() {
        searchAdapter.setListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(searchAdapter);
    }

    @Override
    public void hideRecyclerView() {
        recyclerView.setVisibility(GONE);
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (!blockSearch) {
            blockSearch = true;
            searchPresenter.searchItems(query);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        ProductDetailsActivity_
                .intent(this)
                .extra(EXTRA_PRODUCT, searchAdapter.get(position))
                .start();
    }
}
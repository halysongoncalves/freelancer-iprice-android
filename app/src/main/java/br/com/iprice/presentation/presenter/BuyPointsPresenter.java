package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.InvalidQuantityBuyPointsEvent;
import br.com.iprice.model.event.PointsUpdatedEvent;
import br.com.iprice.model.event.ValidQuantityBuyPointsEvent;
import br.com.iprice.presentation.ui.activity.BuyPointsView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface BuyPointsPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {
    void attachView(BuyPointsView buyPointsView, float valueInitial);

    void validateBuyPoints(String points);

    void updatePoints(String points);

    void onLoadInvalidQuantityBuyPointsEvent(InvalidQuantityBuyPointsEvent invalidQuantityBuyPointsEvent);

    void onLoadValidQuantityBuyPointsEvent(ValidQuantityBuyPointsEvent validQuantityBuyPointsEvent);

    void onLoadPointsUpdatedEvent(PointsUpdatedEvent pointsUpdatedEvent);
}

package br.com.iprice.presentation.ui.fragment;

import java.util.List;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.views.BaseView;
import br.com.iprice.presentation.ui.views.ViewWrapper;

public interface FeedView extends BaseView, ViewWrapper.OnItemClickListener {

    void setupRecyclerView();

    void hideProgress();

    void showRecyclerView();

    void showEmptyState();

    void hideRecyclerView();

    void hideEmptyState();

    void showProgress();

    void insertAllProducts(List<ProductVO> productVOList);

    void updateLike(int like, int position);

    void showInvalidLike();

    void showDialogAddWishList();

    void showSnackBarAddWishList();

    void updateProductAddWishList(int position);

    void updateProductRemoveWishList(int position);

    void showSnackBarRemoveWishList();

    void showDialogAddLike();
}

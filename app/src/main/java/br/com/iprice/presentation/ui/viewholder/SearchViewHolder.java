package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.views.ViewWrapper;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_search)
public class SearchViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<ProductVO> {
    private final Drawable drawableProduct;
    @ViewById(R.id.view_item_search_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.view_item_search_text_view_owner)
    AppCompatTextView appCompatTextViewOwner;

    @ViewById(R.id.view_item_search_text_view_distance)
    AppCompatTextView appCompatTextViewDistance;

    @ViewById(R.id.view_item_search_text_view_price)
    AppCompatTextView appCompatTextViewPrice;

    @ViewById(R.id.view_item_search_text_view_description)
    AppCompatTextView appCompatTextViewDescription;

    @ViewById(R.id.view_item_search_image_view_product)
    CircleImageView circleImageView;

    @ViewById(R.id.view_item_search_content_root)
    View viewContentRoot;

    public SearchViewHolder(Context context) {
        super(context);

        drawableProduct = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_image_micro);
    }

    @Override
    public void bind(ProductVO data, int itemPositionType) {
        if (data.getPictures() != null && data.getPictures().get(0) != null) {
            Picasso.with(getContext()).load(data.getPictures().get(0))
                    .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image_micro)
                    .into(circleImageView);
        }

        if (data.getName() != null) {
            appCompatTextViewName.setText(data.getName());
        }

        if (data.getUserName() != null) {
            appCompatTextViewOwner.setText(getContext().getString(R.string.view_item_user_text_view_owner, data.getUserName()));
        }

        if (data.getDescription() != null) {
            appCompatTextViewDescription.setText(data.getDescription());
        }

        if (data.getDistance() > 0) {
            appCompatTextViewDistance.setText(getContext().getString(R.string.fragment_search_info_text_view_distance, data.getDistance()));
            appCompatTextViewDistance.setVisibility(VISIBLE);
        }

        if (data.isSold()) {
            appCompatTextViewPrice.setText(getContext().getString(R.string.sold));
        } else if (data.getPrice() != null) {
            appCompatTextViewPrice.setText(getContext().getString(R.string.price, data.getPrice()));
        }
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
        viewContentRoot.setOnClickListener(listener);
        viewContentRoot.setTag(position);
    }
}

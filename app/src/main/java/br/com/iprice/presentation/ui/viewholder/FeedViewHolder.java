package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.adapter.ViewPagerAdapter;
import br.com.iprice.presentation.ui.views.ViewWrapper;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_feed)
public class FeedViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<ProductVO> {
    private final Drawable drawableUser, drawableProduct;
    private final ViewPagerAdapter viewPagerAdapter;
    @ViewById(R.id.view_item_user_text_view_name)
    AppCompatTextView appCompatTextViewName;
    @ViewById(R.id.view_item_user_text_view_owner)
    AppCompatTextView appCompatTextViewOwner;
    @ViewById(R.id.view_item_user_text_view_city)
    AppCompatTextView appCompatTextViewCity;
    @ViewById(R.id.view_item_fed_text_view_price)
    AppCompatTextView appCompatTextViewPrice;
    @ViewById(R.id.view_item_fed_text_view_description)
    AppCompatTextView appCompatTextViewDescription;
    @ViewById(R.id.view_item_fed_text_view_date)
    AppCompatTextView appCompatTextViewDate;
    @ViewById(R.id.view_item_fed_image_view_product)
    AppCompatImageView appCompatImageViewProduct;
    @ViewById(R.id.view_item_fed_image_view_wish_list)
    AppCompatImageView appCompatImageViewWishList;
    @ViewById(R.id.view_item_fed_text_view_message)
    AppCompatTextView appCompatTextViewMessage;
    @ViewById(R.id.view_item_fed_text_view_tag)
    AppCompatTextView appCompatTextViewTag;
    @ViewById(R.id.view_item_user_image_view_user)
    CircleImageView circleImageView;
    @ViewById(R.id.view_item_fed_content_root)
    View viewContentRoot;
    @ViewById(R.id.view_item_fed_content_view_pager)
    View viewContentViewPager;
    @ViewById(R.id.view_item_fed_infinite_indicator)
    ViewPager viewPager;
    @ViewById(R.id.view_item_fed_view_pager_indicator)
    CircleIndicator circleIndicator;

    public FeedViewHolder(Context context) {
        super(context);

        viewPagerAdapter = new ViewPagerAdapter(context);
        drawableUser = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_user_medium);
        drawableProduct = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_image);
    }

    @Override
    public void bind(ProductVO data, int itemPositionType) {
        if (data.getType() == ProductVO.PRODUCT_TYPE_CAROUSEL) {
            viewPagerAdapter.setPhotoUrl(data.getPictures());
            viewPager.setAdapter(viewPagerAdapter);
            circleIndicator.setViewPager(viewPager);

            viewContentRoot.setVisibility(GONE);
            viewContentViewPager.setVisibility(VISIBLE);
        } else {
            Picasso.with(getContext()).load(data.getUserPhotoUrl())
                    .resize(drawableUser.getIntrinsicWidth(), drawableUser.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_user_medium)
                    .into(circleImageView);

            Picasso.with(getContext()).load(data.getPhotoUrl())
                    .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(appCompatImageViewProduct);

            if (data.getName() != null) {
                appCompatTextViewName.setText(data.getName());
            }

            appCompatTextViewMessage.setText(String.valueOf(data.getCommentVOList() != null ? data.getCommentVOList().size() : 0));
            appCompatTextViewTag.setText(String.valueOf(data.getLikes()));

            if (data.getUserName() != null) {
                appCompatTextViewOwner.setText(getContext().getString(R.string.view_item_user_text_view_owner, data.getUserName()));
            }

            if (data.getCity() != null) {
                appCompatTextViewCity.setText(data.getCity());
                appCompatTextViewCity.setVisibility(VISIBLE);
            }

            if (data.getDescription() != null) {
                appCompatTextViewDescription.setText(data.getDescription());
            }

            if (data.getDate() != null) {
                appCompatTextViewDate.setText(data.getDate());
            }

            if (data.isSold()) {
                appCompatTextViewPrice.setText(getContext().getString(R.string.sold));
                appCompatTextViewPrice.setVisibility(VISIBLE);
            } else if (data.getPrice() != null && !data.getPrice().isEmpty()) {
                appCompatTextViewPrice.setText(getContext().getString(R.string.price, data.getPrice()));
                appCompatTextViewPrice.setVisibility(VISIBLE);
            }

            appCompatImageViewWishList.setImageResource(data.isWishlist() ? R.drawable.vector_wishlisted : R.drawable.vector_wish_list);
            viewContentViewPager.setVisibility(GONE);
            viewContentRoot.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
        viewContentRoot.setOnClickListener(listener);
        viewContentRoot.setTag(position);

        appCompatTextViewTag.setOnClickListener(listener);
        appCompatTextViewTag.setTag(position);

        appCompatTextViewMessage.setOnClickListener(listener);
        appCompatTextViewMessage.setTag(position);

        appCompatImageViewWishList.setOnClickListener(listener);
        appCompatImageViewWishList.setTag(position);
    }
}

package br.com.iprice.presentation.presenter;

import android.support.annotation.NonNull;

import com.facebook.AccessToken;

import java.io.InputStream;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.TermsUseEvent;
import br.com.iprice.model.event.UnexpectedErrorFacebookEvent;
import br.com.iprice.model.event.UserFieldsInvalidEvent;
import br.com.iprice.presentation.ui.activity.LoginView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface LoginPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {

    void attachView(LoginView loginView);

    void validateFields(@NonNull String email, @NonNull String password);

    void recoverTermsUse(InputStream inputStream);

    void recoverUserFacebook(AccessToken accessToken);

    void onLoadUserFieldsInvalidEvent(UserFieldsInvalidEvent userFieldsInvalidEvent);

    void onLoadTermsUseEvent(TermsUseEvent termsUseEvent);

    void onLoadDuplicateUserErrorEvent(DuplicateUserErrorEvent duplicateUserErrorEvent);

    void onLoadUnexpectedErrorFacebookEvent(UnexpectedErrorFacebookEvent unexpectedErrorFacebookEvent);
}

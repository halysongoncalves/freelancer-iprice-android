package br.com.iprice.presentation.ui.fragment;

import java.util.List;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.views.BaseView;
import br.com.iprice.presentation.ui.views.ViewWrapper;

public interface SearchView extends BaseView, android.support.v7.widget.SearchView.OnQueryTextListener, ViewWrapper.OnItemClickListener {

    void showEmptyState();

    void hideProgress();

    void insertAllProducts(List<ProductVO> productVOList);

    void hideEmptyState();

    void showProgress();

    void freeSearch();

    void showRecyclerView();

    void setupRecyclerView();

    void hideRecyclerView();
}

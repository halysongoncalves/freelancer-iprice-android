package br.com.iprice.presentation.ui.activity;

import br.com.iprice.presentation.ui.views.BaseActivityView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface BuyPointsView extends BaseActivityView {

    void showDialogValidateBuyPoints();

    void showDialogSave();

    void showMyPoints();

    void showValueTotal(float valueTotal);

    void showInvalidPoints();

    void redirectToPayPal(int buyPoints);

    void redirectToMyPoints(int point);
}

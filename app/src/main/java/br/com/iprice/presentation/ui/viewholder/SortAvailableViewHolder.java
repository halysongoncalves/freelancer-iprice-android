package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.presentation.ui.views.ViewWrapper;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_sort_available)
public class SortAvailableViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<SortVO> {
    @ViewById(R.id.view_item_sort_available_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.view_item_sort_available_text_view_description)
    AppCompatTextView appCompatTextViewDescription;

    @ViewById(R.id.view_item_sort_available_text_view_points_value)
    AppCompatTextView appCompatTextViewPoints;

    @ViewById(R.id.view_item_sort_available_button_participate)
    AppCompatButton appCompatButtonParticipate;


    public SortAvailableViewHolder(Context context) {
        super(context);
    }

    @Override
    public void bind(SortVO data, int itemPositionType) {
        if (data.getName() != null) {
            appCompatTextViewName.setText(data.getName());
        }

        if (data.getDescription() != null) {
            appCompatTextViewDescription.setText(data.getDescription());
        }

        appCompatTextViewPoints.setText(String.valueOf(data.getPoints()));
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
        appCompatButtonParticipate.setOnClickListener(listener);
        appCompatButtonParticipate.setTag(position);
    }
}

package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.SortVO;
import br.com.iprice.presentation.ui.viewholder.SortAvailableViewHolder;
import br.com.iprice.presentation.ui.viewholder.SortAvailableViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class SortAvailableAdapter extends BaseRecyclerViewAdapter<SortVO, SortAvailableViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected SortAvailableViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return SortAvailableViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<SortVO, SortAvailableViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }
}
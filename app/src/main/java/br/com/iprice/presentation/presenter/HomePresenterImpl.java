package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.presentation.ui.activity.HomeView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class HomePresenterImpl implements HomePresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    private HomeView homeView;

    @Override
    public void attachView(HomeView homeView) {
        this.homeView = homeView;
        this.homeView.setupToolbar();
        this.homeView.connectLocation();
        this.homeView.setupNavigationView(this.homeView.getNavigationView(), this.homeView.getDrawerLayout());
    }

    @Override
    public void logout() {
        userService.logout();
    }
}
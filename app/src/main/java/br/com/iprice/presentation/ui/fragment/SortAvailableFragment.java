package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.iprice.R;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.presentation.presenter.SortAvailablePresenter;
import br.com.iprice.presentation.presenter.SortAvailablePresenterImpl;
import br.com.iprice.presentation.ui.activity.LoginActivity_;
import br.com.iprice.presentation.ui.activity.SortActivity;
import br.com.iprice.presentation.ui.adapter.SortAvailableAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_sort_available)
public class SortAvailableFragment extends Fragment implements SortAvailableView {
    @ViewById(R.id.fragment_sort_available_recycler_view_feed)
    RecyclerView recyclerView;

    @ViewById(R.id.fragment_sort_available_progress)
    ProgressBar progressBar;

    @ViewById(R.id.fragment_sort_available_text_view_empty)
    AppCompatTextView appCompatTextViewEmptyState;

    @Bean
    SortAvailableAdapter sortAvailableAdapter;

    @Bean(SortAvailablePresenterImpl.class)
    SortAvailablePresenter sortAvailablePresenter;

    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        sortAvailablePresenter.registerBus();
        sortAvailablePresenter.attachView(this);
        sortAvailablePresenter.recoverAllAvailableSort();
    }

    @Override
    public void onDestroy() {
        sortAvailablePresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupRecyclerView() {
        sortAvailableAdapter.setListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(sortAvailableAdapter);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showEmptyState() {
        appCompatTextViewEmptyState.setVisibility(VISIBLE);
    }

    @Override
    public void hideEmptyState() {
        appCompatTextViewEmptyState.setVisibility(GONE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void showRecyclerView() {
        recyclerView.setVisibility(VISIBLE);
    }

    @Override
    public void hideRecyclerView() {
        recyclerView.setVisibility(GONE);
    }

    @Override
    public void insertAllAvailableSort(List<SortVO> sortVOList) {
        sortAvailableAdapter.addAll(sortVOList);
    }

    @Override
    public void showDialogParticipateSort() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .content(getString(R.string.dialog_title_participate_sort))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void updateAllAvailableSort(List<SortVO> sortVOList) {
        sortAvailableAdapter.clear();
        sortAvailableAdapter.addAll(sortVOList);
    }

    @Override
    public void showInsufficientPoints() {
        Snackbar.make(((SortActivity) getActivity()).getRootView(), getString(R.string.fragment_sort_available_snack_bar_insufficient_points), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(SortAvailableFragment.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void onItemClick(View view, int position) {
        sortAvailablePresenter.participateSort(sortAvailableAdapter.get(position));
    }
}

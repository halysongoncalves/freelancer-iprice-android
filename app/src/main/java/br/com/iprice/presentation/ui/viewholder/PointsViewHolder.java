package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.presentation.ui.views.ViewWrapper;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_points)
public class PointsViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<PointsVO> {
    @ViewById(R.id.view_item_points_image_view_type)
    AppCompatImageView appCompatImageView;

    @ViewById(R.id.view_item_points_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.view_item_points_text_view_points_value)
    AppCompatTextView appCompatTextViewPointsValue;

    @ViewById(R.id.view_item_points_content_root)
    View viewContentRoot;

    public PointsViewHolder(Context context) {
        super(context);
    }

    @Override
    public void bind(PointsVO data, int itemPositionType) {
        appCompatImageView.setBackgroundResource(data.getFlag());
        appCompatTextViewName.setText(data.getName());
        appCompatTextViewPointsValue.setText(String.valueOf(data.getPoint()));
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
        viewContentRoot.setOnClickListener(listener);
        viewContentRoot.setTag(position);
    }
}

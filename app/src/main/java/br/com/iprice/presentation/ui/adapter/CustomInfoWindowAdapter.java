package br.com.iprice.presentation.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import br.com.iprice.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private final View contentsView;
    private final AppCompatTextView appCompatTextViewName;
    private final CircleImageView circleImageView;
    private final Context context;
    private final Drawable drawableProduct;
    private Marker lastMarker = null;

    public CustomInfoWindowAdapter(Context context) {
        this.contentsView = LayoutInflater.from(context).inflate(R.layout.view_maps_windows, null);
        this.context = context;

        drawableProduct = ContextCompat.getDrawable(context, R.drawable.placeholder_image_micro);

        appCompatTextViewName = (AppCompatTextView) contentsView.findViewById(R.id.view_maps_windows_text_view_name);
        circleImageView = (CircleImageView) contentsView.findViewById(R.id.view_maps_windows_image_view_product);
    }

    @Override
    public View getInfoWindow(final Marker marker) {
        if (lastMarker == null || !lastMarker.getId().equals(marker.getId())) {
            lastMarker = marker;

            appCompatTextViewName.setText(marker.getTitle() != null ? marker.getTitle() : "");

            if (marker.getSnippet() != null) {
                Picasso.with(context).load(marker.getSnippet())
                        .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                        .centerCrop()
                        .placeholder(R.drawable.placeholder_image_micro)
                        .into(circleImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                if (marker.isInfoWindowShown()) {
                                    marker.showInfoWindow();
                                }
                            }

                            @Override
                            public void onError() {

                            }
                        });
            }
        }
        return contentsView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}

package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.views.ViewWrapper;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_wish_list)
public class WishListViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<ProductVO> {
    private final Drawable drawableProduct;
    @ViewById(R.id.view_item_wish_list_image_view_product)
    AppCompatImageView appCompatImageViewProduct;

    @ViewById(R.id.view_item_wish_list_text_view_price)
    AppCompatTextView appCompatTextViewPrice;

    @ViewById(R.id.view_item_wish_list_card_view)
    View viewContentRoot;

    public WishListViewHolder(Context context) {
        super(context);

        drawableProduct = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_image);
    }

    @Override
    public void bind(ProductVO data, int itemPositionType) {
        Picasso.with(getContext()).load(data.getPhotoUrl())
                .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_image)
                .into(appCompatImageViewProduct);

        if (data.getPrice() != null && !data.getPrice().isEmpty()) {
            appCompatTextViewPrice.setText(getContext().getString(R.string.price, data.getPrice()));
            appCompatTextViewPrice.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
        viewContentRoot.setOnClickListener(listener);
        viewContentRoot.setTag(position);
    }
}

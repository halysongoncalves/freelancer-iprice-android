package br.com.iprice.presentation.ui.fragment;

import com.google.android.gms.maps.OnMapReadyCallback;

import br.com.iprice.presentation.ui.views.BaseView;

public interface ProductLocationView extends BaseView, OnMapReadyCallback {

    void setupGoogleMaps();
}

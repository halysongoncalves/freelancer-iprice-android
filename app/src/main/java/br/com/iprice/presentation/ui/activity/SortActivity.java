package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.LinkedList;
import java.util.List;

import br.com.iprice.R;
import br.com.iprice.model.entities.SectionBeanVO;
import br.com.iprice.presentation.ui.adapter.TabLayoutAdapter;
import br.com.iprice.presentation.ui.fragment.SortAvailableFragment_;
import br.com.iprice.presentation.ui.fragment.SortParticipateFragment_;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EActivity(R.layout.activity_sort)
public class SortActivity extends AppCompatActivity implements SortView {
    @ViewById(R.id.view_toolbar_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.activity_sort_tab_layout)
    TabLayout tabLayout;

    @ViewById(R.id.activity_sort_viewpager)
    ViewPager viewPager;

    @ViewById(R.id.activity_sort_coordinator)
    CoordinatorLayout coordinatorLayout;

    @AfterViews
    void afterViews() {
        setupToolbar();
        setupViewPager(getViewPager(), recoverSectionsFragment());
        setupTabLayout(getTabLayout(), getViewPager());
    }

    @Override
    public void setupToolbar() {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.activity_sort_title);
        }
    }

    @Override
    public void setupViewPager(final ViewPager viewPager, final TabLayoutAdapter tabLayoutAdapter) {
        viewPager.setAdapter(tabLayoutAdapter);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    public void setupTabLayout(TabLayout tabLayout, ViewPager viewPager) {
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public TabLayoutAdapter recoverSectionsFragment() {
        List<SectionBeanVO> sectionBeanVOList = new LinkedList<>();
        sectionBeanVOList.add(new SectionBeanVO(getString(R.string.fragment_sort_available_title),
                SortAvailableFragment_.builder()
                        .build()));

        sectionBeanVOList.add(new SectionBeanVO(getString(R.string.fragment_sort_participate_title),
                SortParticipateFragment_.builder()
                        .build()));

        return new TabLayoutAdapter(getSupportFragmentManager(), sectionBeanVOList);
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewPager;
    }

    @Override
    public CoordinatorLayout getRootView() {
        return coordinatorLayout;
    }

    @OptionsItem(android.R.id.home)
    void clickMenuHome() {
        finish();
    }

    @Override
    public void showDialogTryAgain(String message) {
    }

    @Override
    public void showDialogConnection() {
    }

    @Override
    public void showDialogUnauthorized(String message) {
    }

    @Override
    public void hideDialog() {
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
    }
}

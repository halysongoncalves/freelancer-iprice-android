package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.SearchService;
import br.com.iprice.domain.SearchServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.event.SearchItemsEmptyEvent;
import br.com.iprice.model.event.SearchItemsFilteredEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.fragment.SearchView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class SearchPresenterImpl implements SearchPresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    @Bean(SearchServiceImpl.class)
    SearchService searchService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    private SearchView searchView;

    @Override
    public void attachView(SearchView searchView) {
        this.searchView = searchView;
        this.searchView.setupRecyclerView();
    }

    @Override
    public void registerBus() {
        searchService.registerBus();
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        searchService.unregisterBus();
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void searchItems(String query) {
        searchView.hideRecyclerView();
        searchView.hideEmptyState();
        searchView.showProgress();

        searchService.searchItems(query);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadSearchItemsFilteredEvent(SearchItemsFilteredEvent searchItemsFilteredEvent) {
        searchView.insertAllProducts(searchItemsFilteredEvent.getProductVOList());
        searchView.hideProgress();
        searchView.freeSearch();
        searchView.showRecyclerView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadSearchItemsEmptyEvent(SearchItemsEmptyEvent searchItemsEmptyEvent) {
        searchView.hideProgress();
        searchView.showEmptyState();
        searchView.freeSearch();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        searchView.hideDialog();
        searchView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        searchView.hideDialog();
        searchView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        searchView.hideDialog();
        searchView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        searchView.hideDialog();
        searchView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        searchView.hideDialog();
        userService.logout();
        searchView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        searchView.hideDialog();
        searchView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        searchView.hideDialog();
        searchView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        searchView.hideDialog();
        searchView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

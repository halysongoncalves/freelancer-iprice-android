package br.com.iprice.presentation.ui.fragment;

import android.graphics.Bitmap;

import java.io.File;

import br.com.iprice.presentation.ui.views.BaseView;

public interface SellerView extends BaseView {
    void showDialogLoading();

    void redirectToSellerDetails(File file);

    void redirectToSellerDetails(Bitmap bitmap);
}
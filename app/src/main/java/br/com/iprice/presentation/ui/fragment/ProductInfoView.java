package br.com.iprice.presentation.ui.fragment;

import java.util.List;

import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.presentation.ui.views.BaseView;
import br.com.iprice.presentation.ui.views.ViewWrapper;
import cn.lightsky.infiniteindicator.page.OnPageClickListener;
import cn.lightsky.infiniteindicator.page.Page;

public interface ProductInfoView extends BaseView, ViewWrapper.OnItemClickListener, OnPageClickListener {

    void setupInfiniteIndicator();

    void setupRecyclerView();

    void hideProgress();

    void showViewComments();

    void showEmptyState();

    void hideViewComment();

    void hideEmptyState();

    void showProgress();

    void insertAllComments(List<CommentVO> commentVOList);

    void insertComment(CommentVO commentVO);

    void insertAllPhotos(List<Page> pageList);

    void disableButtonSend();

    void enableButtonSend();

    void showProductName(String name);

    void showCity(String city);

    void showOwner(String owner);

    void showDescription(String description);

    void showPhone(String phone);

    void showUserPhoto(String productVO);

    void showDialogAddComment();

    void clearComment();
}

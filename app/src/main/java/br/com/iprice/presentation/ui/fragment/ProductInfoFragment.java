package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.EditorAction;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.iprice.R;
import br.com.iprice.common.PicassoLoader;
import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.presenter.ProductPresenter;
import br.com.iprice.presentation.presenter.ProductPresenterImpl;
import br.com.iprice.presentation.ui.activity.LoginActivity_;
import br.com.iprice.presentation.ui.activity.ProductImageDetailsActivity;
import br.com.iprice.presentation.ui.activity.ProductImageDetailsActivity_;
import br.com.iprice.presentation.ui.adapter.CommentsAdapter;
import cn.lightsky.infiniteindicator.InfiniteIndicator;
import cn.lightsky.infiniteindicator.page.Page;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_product_info)
public class ProductInfoFragment extends Fragment implements ProductInfoView {
    @ViewById(R.id.fragment_product_info_view_description)
    AppCompatTextView appCompatTextViewDescription;

    @ViewById(R.id.fragment_product_info_view_phone_value)
    AppCompatTextView appCompatTextViewPhone;

    @ViewById(R.id.fragment_product_info_text_view_empty)
    AppCompatTextView appCompatTextViewEmptyState;

    @ViewById(R.id.view_item_user_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.view_item_user_text_view_owner)
    AppCompatTextView appCompatTextViewOwner;

    @ViewById(R.id.view_item_user_text_view_city)
    AppCompatTextView appCompatTextViewCity;

    @ViewById(R.id.view_message_edit_text_message)
    AppCompatEditText appCompatEditTextMessage;

    @ViewById(R.id.view_message_button_send)
    AppCompatButton appCompatButtonSend;

    @ViewById(R.id.view_item_user_image_view_user)
    CircleImageView circleImageView;

    @ViewById(R.id.fragment_product_info_recycler_view_feed)
    RecyclerView recyclerView;

    @ViewById(R.id.fragment_product_info_progress)
    ProgressBar progressBar;

    @ViewById(R.id.fragment_product_info_infinite_view_pager)
    InfiniteIndicator infiniteIndicator;

    @ViewById(R.id.fragment_product_info_content_comment)
    View viewContentComment;

    @FragmentArg(FeedFragment.EXTRA_PRODUCT)
    @InstanceState
    ProductVO productVO;

    @Bean
    PicassoLoader picassoLoader;

    @Bean
    CommentsAdapter commentsAdapter;

    @Bean(ProductPresenterImpl.class)
    ProductPresenter productPresenter;

    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        productPresenter.registerBus();
        productPresenter.attachView(this);
        productPresenter.recoverProductDetails(productVO, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        infiniteIndicator.stop();
    }

    @Override
    public void onResume() {
        super.onResume();
        infiniteIndicator.start();
    }

    @Override
    public void onDestroy() {
        productPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupInfiniteIndicator() {
        infiniteIndicator.setImageLoader(picassoLoader);
        infiniteIndicator.setPosition(InfiniteIndicator.IndicatorPosition.Center_Bottom);
        infiniteIndicator.setInfinite(true);
        infiniteIndicator.setStopScrollWhenTouch(false);
    }

    @Override
    public void setupRecyclerView() {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(commentsAdapter);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showViewComments() {
        viewContentComment.setVisibility(VISIBLE);
    }

    @Override
    public void showEmptyState() {
        appCompatTextViewEmptyState.setVisibility(VISIBLE);
    }

    @Override
    public void hideViewComment() {
        viewContentComment.setVisibility(GONE);
    }

    @Override
    public void hideEmptyState() {
        appCompatTextViewEmptyState.setVisibility(GONE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void insertAllComments(List<CommentVO> commentVOList) {
        commentsAdapter.addAll(commentVOList);
        infiniteIndicator.start();
    }

    @Override
    public void insertComment(CommentVO commentVO) {
        commentsAdapter.add(commentVO);
    }

    @Override
    public void insertAllPhotos(List<Page> pageList) {
        infiniteIndicator.addPages(pageList);
    }

    @AfterTextChange(R.id.view_message_edit_text_message)
    void afterTextChangeMessage(final Editable editable) {
        if (editable.toString().isEmpty()) {
            disableButtonSend();
            return;
        }

        enableButtonSend();
    }

    @EditorAction(R.id.view_message_edit_text_message)
    void actionEditTextPrice(int actionId) {
        switch (actionId) {
            case EditorInfo.IME_ACTION_SEND:
                productPresenter.addComment(productVO.getId(), appCompatEditTextMessage.getText().toString());

                break;

            default:
                break;
        }
    }

    @Override
    public void disableButtonSend() {
        appCompatButtonSend.setAlpha(0.5f);
        appCompatButtonSend.setClickable(false);
        appCompatButtonSend.setEnabled(false);
    }

    @Override
    public void enableButtonSend() {
        appCompatButtonSend.setAlpha(1f);
        appCompatButtonSend.setClickable(true);
        appCompatButtonSend.setEnabled(true);
    }

    @Click(R.id.view_message_button_send)
    void onClickSendMComment() {
        productPresenter.addComment(productVO.getId(), appCompatEditTextMessage.getText().toString());
    }

    @Override
    public void showProductName(String name) {
        appCompatTextViewName.setText(name);
    }

    @Override
    public void showCity(String city) {
        appCompatTextViewCity.setText(city);
        appCompatTextViewCity.setVisibility(VISIBLE);
    }

    @Override
    public void showOwner(String owner) {
        appCompatTextViewOwner.setText(getString(R.string.view_item_user_text_view_owner, owner));
    }

    @Override
    public void showDescription(String description) {
        appCompatTextViewDescription.setText(description);
    }

    @Override
    public void showPhone(String phone) {
        appCompatTextViewPhone.setText(phone);
    }

    @Override
    public void showUserPhoto(String photoUrl) {
        final Drawable drawableUser = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_user_medium);

        Picasso.with(getContext()).load(photoUrl)
                .resize(drawableUser.getIntrinsicWidth(), drawableUser.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_user_medium)
                .into(circleImageView);
    }

    @Override
    public void showDialogAddComment() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .content(getString(R.string.dialog_title_save))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void clearComment() {
        appCompatEditTextMessage.getText().clear();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(ProductInfoFragment.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void onItemClick(View view, int position) {
    }

    @Override
    public void onPageClick(int position, Page page) {
        ProductImageDetailsActivity_
                .intent(this)
                .extra(ProductImageDetailsActivity.EXTRA_URL_IMAGE, page.data != null ? page.data : "")
                .start();
    }
}

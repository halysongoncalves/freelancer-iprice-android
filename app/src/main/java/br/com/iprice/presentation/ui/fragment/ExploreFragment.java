package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.R;
import br.com.iprice.domain.LocationClient;
import br.com.iprice.domain.LocationClientImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.presenter.ExplorePresenter;
import br.com.iprice.presentation.presenter.ExplorePresenterImpl;
import br.com.iprice.presentation.ui.activity.LoginActivity_;
import br.com.iprice.presentation.ui.activity.ProductDetailsActivity_;
import br.com.iprice.presentation.ui.adapter.CustomInfoWindowAdapter;

import static android.view.View.VISIBLE;
import static br.com.iprice.presentation.ui.fragment.FeedFragment.EXTRA_PRODUCT;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_explore)
public class ExploreFragment extends Fragment implements ExploreView {
    private static final int ZOOM = 8;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @ViewById(R.id.fragment_explore_progress)
    ProgressBar progressBar;
    @ViewById(R.id.fragment_explore_text_view_empty)
    AppCompatTextView appCompatTextViewEmpty;
    @ViewById(R.id.fragment_explore_scrollview)
    View viewContentMaps;
    @Bean(ExplorePresenterImpl.class)
    ExplorePresenter explorePresenter;
    @Bean(LocationClientImpl.class)
    LocationClient locationClient;
    @InstanceState
    ArrayList<ProductVO> productVOArrayList;
    private MaterialDialog materialDialog;
    private GoogleMap googleMap;
    private CustomInfoWindowAdapter customInfoWindowAdapter;

    @AfterViews
    void afterViews() {
        explorePresenter.registerBus();
        explorePresenter.attachView(this);
        explorePresenter.exploreAllProduct(locationClient.getLatitude(), locationClient.getLongitude(), productVOArrayList);
    }

    @Override
    public void onDestroy() {
        explorePresenter.unregisterBus();
        super.onDestroy();
    }


    @Override
    public void setupGoogleMaps() {
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_explore_support_map)).getMapAsync(this);
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(ExploreFragment.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void showEmptyState() {
        appCompatTextViewEmpty.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMaps() {
        viewContentMaps.setVisibility(VISIBLE);
    }

    @Override
    public void insertAllProducts(List<ProductVO> productVOList) {
        this.productVOArrayList = (ArrayList<ProductVO>) productVOList;

        for (ProductVO productVO : productVOList) {
            double[] location = productVO.getLocation();
            LatLng latLng = new LatLng(location[0], location[1]);

            googleMap.addMarker(new MarkerOptions()
                    .title(productVO.getName() != null ? productVO.getName() : "")
                    .snippet(productVO.getPhotoUrl() != null ? productVO.getPhotoUrl() : "")
                    .position(latLng));
        }

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(locationClient.getLatitude(), locationClient.getLongitude()), ZOOM));
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (customInfoWindowAdapter == null) {
            customInfoWindowAdapter = new CustomInfoWindowAdapter(getContext());
        }

        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.setInfoWindowAdapter(customInfoWindowAdapter);
        this.googleMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        ProductVO productVOSelected = recoverProductSelected(marker);

        if (productVOSelected != null) {
            ProductDetailsActivity_
                    .intent(this)
                    .extra(EXTRA_PRODUCT, productVOSelected)
                    .start();
        }
    }

    private ProductVO recoverProductSelected(Marker marker) {
        ProductVO productVOSelected = null;
        for (ProductVO productVO : productVOArrayList) {
            if (productVO.getName() != null && !productVO.getName().isEmpty() && productVO.getName().equals(marker.getTitle())) {
                productVOSelected = productVO;
            }
        }

        return productVOSelected;
    }
}
package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.presentation.ui.viewholder.PointsViewHolder;
import br.com.iprice.presentation.ui.viewholder.PointsViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class PointsAdapter extends BaseRecyclerViewAdapter<PointsVO, PointsViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected PointsViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return PointsViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<PointsVO, PointsViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }
}
package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.viewholder.WishListViewHolder;
import br.com.iprice.presentation.ui.viewholder.WishListViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class WishListAdapter extends BaseRecyclerViewAdapter<ProductVO, WishListViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected WishListViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return WishListViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ProductVO, WishListViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }
}
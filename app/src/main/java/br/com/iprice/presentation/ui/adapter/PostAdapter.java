package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.viewholder.PostViewHolder;
import br.com.iprice.presentation.ui.viewholder.PostViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class PostAdapter extends BaseRecyclerViewAdapter<ProductVO, PostViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected PostViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return PostViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ProductVO, PostViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }
}
package br.com.iprice.presentation.presenter;

import java.util.ArrayList;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.RecoverAllProductsExploreEmptyEvent;
import br.com.iprice.model.event.RecoverAllProductsExploreEvent;
import br.com.iprice.presentation.ui.fragment.ExploreView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ExplorePresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {

    void attachView(ExploreView exploreView);

    void exploreAllProduct(double latitude, double longitude, ArrayList<ProductVO> productVOArrayList);

    void onLoadRecoverAllProductsExploreEvent(RecoverAllProductsExploreEvent recoverAllProductsExploreEvent);

    void onLoadRecoverAllProductsExploreEmptyEvent(RecoverAllProductsExploreEmptyEvent recoverAllProductsExploreEmptyEvent);
}

package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.AddCommentEvent;
import br.com.iprice.model.event.BuilderPagesEvent;
import br.com.iprice.presentation.ui.fragment.ProductInfoView;
import br.com.iprice.repository.http.HttpFailure;
import cn.lightsky.infiniteindicator.page.OnPageClickListener;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ProductPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {

    void attachView(ProductInfoView productInfoView);

    void recoverProductDetails(ProductVO productVO, OnPageClickListener onPageClickListener);

    void addComment(String productId, String comment);

    void onLoadAddCommentEvent(AddCommentEvent addCommentEvent);

    void onLoadBuilderPagesEvent(BuilderPagesEvent builderPagesEvent);
}

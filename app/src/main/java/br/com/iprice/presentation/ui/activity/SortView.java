package br.com.iprice.presentation.ui.activity;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import br.com.iprice.presentation.ui.adapter.TabLayoutAdapter;
import br.com.iprice.presentation.ui.views.BaseActivityView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SortView extends BaseActivityView {
    void setupViewPager(ViewPager viewPager, TabLayoutAdapter tabLayoutAdapter);

    void setupTabLayout(TabLayout tabLayout, ViewPager viewPager);

    TabLayoutAdapter recoverSectionsFragment();

    TabLayout getTabLayout();

    ViewPager getViewPager();

    CoordinatorLayout getRootView();


}

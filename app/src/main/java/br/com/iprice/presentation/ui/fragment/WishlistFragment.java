package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.presenter.WishListPresenter;
import br.com.iprice.presentation.presenter.WishListPresenterImpl;
import br.com.iprice.presentation.ui.activity.LoginActivity_;
import br.com.iprice.presentation.ui.activity.ProductDetailsActivity_;
import br.com.iprice.presentation.ui.adapter.WishListAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static br.com.iprice.presentation.ui.fragment.FeedFragment.EXTRA_PRODUCT;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_wish_list)
public class WishlistFragment extends Fragment implements WishListView {
    @ViewById(R.id.fragment_wish_list_recycler_view_feed)
    RecyclerView recyclerView;

    @ViewById(R.id.fragment_wish_list_progress)
    ProgressBar progressBar;

    @ViewById(R.id.fragment_wish_list_text_view_empty)
    AppCompatTextView appCompatTextViewEmptyState;

    @ViewById(R.id.fragment_wish_list_content_root)
    View viewContentRoot;

    @Bean(WishListPresenterImpl.class)
    WishListPresenter wishListPresenter;

    @Bean
    WishListAdapter wishListAdapter;

    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        wishListPresenter.registerBus();
        wishListPresenter.attachView(this);
        wishListPresenter.recoverAllWishList();
    }

    @Override
    public void onDestroy() {
        wishListPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupRecyclerView() {
        wishListAdapter.setListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(wishListAdapter);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showRecyclerView() {
        recyclerView.setVisibility(VISIBLE);
    }

    @Override
    public void showEmptyState() {
        appCompatTextViewEmptyState.setVisibility(VISIBLE);
    }

    @Override
    public void hideRecyclerView() {
        recyclerView.setVisibility(GONE);
    }

    @Override
    public void hideEmptyState() {
        appCompatTextViewEmptyState.setVisibility(GONE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void insertAllProduct(List<ProductVO> productVOList) {
        wishListAdapter.addAll(productVOList);
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_try_again)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        wishListPresenter.recoverAllWishList();
                    }
                })
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(WishlistFragment.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void onItemClick(View view, int position) {
        ProductDetailsActivity_
                .intent(this)
                .extra(EXTRA_PRODUCT, wishListAdapter.get(position))
                .start();
    }
}

package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.views.ViewWrapper;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_post)
public class PostViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<ProductVO> {
    private final Drawable drawableUser, drawableProduct;
    @ViewById(R.id.view_item_user_image_view_user)
    CircleImageView circleImageView;

    @ViewById(R.id.view_item_post_image_view_product)
    AppCompatImageView appCompatImageViewProduct;

    @ViewById(R.id.view_item_user_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.view_item_user_text_view_owner)
    AppCompatTextView appCompatTextViewOwner;

    @ViewById(R.id.view_item_post_text_view_price)
    AppCompatTextView appCompatTextViewPrice;

    @ViewById(R.id.view_item_post_text_view_delete)
    AppCompatTextView appCompatTextViewDelete;

    @ViewById(R.id.view_item_post_text_view_description)
    AppCompatTextView appCompatTextViewDescription;

    @ViewById(R.id.view_item_post_content_root)
    View viewContentRoot;

    public PostViewHolder(Context context) {
        super(context);

        drawableUser = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_user_medium);
        drawableProduct = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_image);
    }

    @Override
    public void bind(ProductVO data, int itemPositionType) {
        Picasso.with(getContext()).load(data.getUserPhotoUrl())
                .resize(drawableUser.getIntrinsicWidth(), drawableUser.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_user_medium)
                .into(circleImageView);

        Picasso.with(getContext()).load(data.getPhotoUrl())
                .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_image)
                .into(appCompatImageViewProduct);

        if (data.getName() != null) {
            appCompatTextViewName.setText(data.getName());
        }

        if (data.getUserName() != null) {
            appCompatTextViewOwner.setText(getContext().getString(R.string.view_item_user_text_view_owner, data.getUserName()));
        }

        if (data.getDescription() != null) {
            appCompatTextViewDescription.setText(data.getDescription());
        }

        if (data.isSold()) {
            appCompatTextViewPrice.setText(getContext().getString(R.string.sold));
            appCompatTextViewPrice.setVisibility(VISIBLE);
        } else if (data.getPrice() != null && !data.getPrice().isEmpty()) {
            appCompatTextViewPrice.setText(getContext().getString(R.string.price, data.getPrice()));
            appCompatTextViewPrice.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
        viewContentRoot.setOnClickListener(listener);
        viewContentRoot.setTag(position);

        appCompatTextViewDelete.setOnClickListener(listener);
        appCompatTextViewDelete.setTag(position);
    }
}

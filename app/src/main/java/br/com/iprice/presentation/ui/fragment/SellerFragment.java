package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.File;

import br.com.iprice.R;
import br.com.iprice.presentation.presenter.SellerPresenter;
import br.com.iprice.presentation.presenter.SellerPresenterImpl;
import br.com.iprice.presentation.ui.activity.HomeActivity;
import br.com.iprice.presentation.ui.activity.SellerDetailsActivity_;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_buy)
public class SellerFragment extends Fragment implements SellerView {
    public static final String EXTRA_TAKE_PICTURE = "extra_take_picture";
    public static final String EXTRA_PHOTO_GALLERY = "extra_photo_gallery";
    public static final String DATA = "data";
    private static final int REQUEST_CAMERA_TAKE_PICTURE = 5431;
    private static final int REQUEST_CAMERA_FROM_GALLERY = 4342;
    private static final int REQUEST_CODE_SELLER_DETAILS = 5422;
    @ViewById(R.id.fragment_seller_scrollview)
    View viewContentRoot;
    @Bean(SellerPresenterImpl.class)
    SellerPresenter sellerPresenter;
    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        sellerPresenter.attachView(this);
    }

    @Override
    public void onStart() {
        sellerPresenter.registerBus();
        super.onStart();
    }

    @Override
    public void onStop() {
        sellerPresenter.unregisterBus();
        super.onStop();
    }

    @Click(R.id.fragment_seller_button_take_picture)
    void clickTakePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA_TAKE_PICTURE);
    }

    @Click(R.id.fragment_seller_button_chose_library_photo)
    void clickFab() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        }
        startActivityForResult(Intent.createChooser(intent, getString(R.string.fragment_seller_title_chooser)), REQUEST_CAMERA_FROM_GALLERY);
    }

    @OnActivityResult(REQUEST_CAMERA_FROM_GALLERY)
    void onResultChoiceImageFromGallery(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerPresenter.selectedImageFromGallery(data.getData());
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CAMERA_TAKE_PICTURE)
    void onResultTakePicture(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerPresenter.builderTakePicture((Bitmap) data.getExtras().get(DATA));
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_SELLER_DETAILS)
    void onResultSellerDetails(int resultCode) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                ((HomeActivity) getActivity()).redirectToFeed();
                break;

            default:
                break;
        }
    }


    @Override
    public void showDialogLoading() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .content(getString(R.string.dialog_title_loading_data))
                .progress(true, 0)
                .show();
    }

    @Override
    public void redirectToSellerDetails(File file) {
        SellerDetailsActivity_
                .intent(this)
                .extra(EXTRA_PHOTO_GALLERY, file)
                .startForResult(REQUEST_CODE_SELLER_DETAILS);
    }

    @Override
    public void redirectToSellerDetails(Bitmap bitmap) {
        SellerDetailsActivity_
                .intent(this)
                .extra(EXTRA_TAKE_PICTURE, bitmap)
                .startForResult(REQUEST_CODE_SELLER_DETAILS);
    }

    @Override
    public void showDialogTryAgain(String message) {
    }

    @Override
    public void showDialogConnection() {
    }

    @Override
    public void showDialogUnauthorized(String message) {
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
            materialDialog = null;
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }
}

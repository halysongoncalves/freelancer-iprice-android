package br.com.iprice.presentation.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.iprice.R;


/**
 * Created by halysongoncalves on 08/11/15.
 */
public class ViewPagerAdapter extends PagerAdapter {
    private final Context context;
    private final LayoutInflater mLayoutInflater;
    private final Drawable drawableProduct;
    private List<String> photoUrl;

    public ViewPagerAdapter(Context context) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        drawableProduct = ContextCompat.getDrawable(context, R.drawable.placeholder_image);
    }

    public void setPhotoUrl(List<String> photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public int getCount() {
        return photoUrl.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View itemView = mLayoutInflater.inflate(R.layout.view_item_carousel, container, false);

        final AppCompatImageView appCompatImageViewProduct = (AppCompatImageView) itemView.findViewById(R.id.view_item_carousel_image_view_product);

        Picasso.with(context)
                .load(photoUrl.get(position))
                .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_image)
                .into(appCompatImageViewProduct);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}

package br.com.iprice.presentation.presenter;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.File;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.AddNewProductEvent;
import br.com.iprice.model.event.BuilderTakePictureFiveEvent;
import br.com.iprice.model.event.BuilderTakePictureFourEvent;
import br.com.iprice.model.event.BuilderTakePictureOneEvent;
import br.com.iprice.model.event.BuilderTakePictureTWoEvent;
import br.com.iprice.model.event.BuilderTakePictureThreeEvent;
import br.com.iprice.model.event.PhotoFiveChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoFourChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoOneChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoThreeChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoTwoChoiceFromGalleryEvent;
import br.com.iprice.model.event.PointsUpdatedEvent;
import br.com.iprice.model.event.SellerProductInvalidEvent;
import br.com.iprice.presentation.ui.activity.SellerDetailsView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SellerDetailsPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {
    void attachView(SellerDetailsView sellerDetailsView, Bitmap bitmap, File fileImageOne);

    void validateFields(File imageOne, Bitmap bitmapOne, File fileImageTwo, Bitmap bitmapTwo, File fileImageThree, Bitmap bitmapThree, File fileImageFour, Bitmap bitmapFour, File fileImageFive, Bitmap bitmapImageFive, String name, String message, String price, String phone);

    void selectedImageOneFromGallery(Uri data);

    void builderTakePictureOne(Bitmap bitmap);

    void selectedImageTwoFromGallery(Uri data);

    void builderTakePictureTwo(Bitmap bitmap);

    void selectedImageThreeFromGallery(Uri data);

    void builderTakePictureThree(Bitmap bitmap);

    void selectedImageFourFromGallery(Uri data);

    void builderTakePictureFour(Bitmap bitmap);

    void selectedImageFiveFromGallery(Uri data);

    void builderTakePictureFive(Bitmap bitmap);

    void onLoadSellerProductInvalidEvent(SellerProductInvalidEvent sellerProductInvalidEvent);

    void onLoadBuilderTakePictureOneEvent(BuilderTakePictureOneEvent builderTakePictureOneEvent);

    void onLoadPhotoOneChoiceFromGalleryEvent(PhotoOneChoiceFromGalleryEvent photoOneChoiceFromGalleryEvent);

    void onLoadBuilderTakePictureTwoEvent(BuilderTakePictureTWoEvent builderTakePictureTWoEvent);

    void onLoadPhotoTwoChoiceFromGalleryEvent(PhotoTwoChoiceFromGalleryEvent photoTwoChoiceFromGalleryEvent);

    void onLoadBuilderTakePictureThreeEvent(BuilderTakePictureThreeEvent builderTakePictureThreeEvent);

    void onLoadPhotoThreeChoiceFromGalleryEvent(PhotoThreeChoiceFromGalleryEvent photoThreeChoiceFromGalleryEvent);

    void onLoadBuilderTakePictureFourEvent(BuilderTakePictureFourEvent builderTakePictureFourEvent);

    void onLoadPhotoFourChoiceFromGalleryEvent(PhotoFourChoiceFromGalleryEvent photoFourChoiceFromGalleryEvent);

    void onLoadBuilderTakePictureFiveEvent(BuilderTakePictureFiveEvent builderTakePictureFiveEvent);

    void onLoadPhotoFiveChoiceFromGalleryEvent(PhotoFiveChoiceFromGalleryEvent photoFiveChoiceFromGalleryEvent);

    void onLoadAddNewProductEvent(AddNewProductEvent addNewProductEvent);

    void onLoadPointsUpdatedEvent(PointsUpdatedEvent pointsUpdatedEvent);
}

package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.SortAvailableService;
import br.com.iprice.domain.SortAvailableServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.model.event.AllAvailableSortEmptyEvent;
import br.com.iprice.model.event.AllAvailableSortEvent;
import br.com.iprice.model.event.InsufficientPointsEvent;
import br.com.iprice.model.event.RefreshAvailableSortEvent;
import br.com.iprice.model.event.http.BadRequestSortAvailableEvent;
import br.com.iprice.model.event.http.ConvertErrorSortAvaiableEvent;
import br.com.iprice.model.event.http.ForbiddenSortAvailableEvent;
import br.com.iprice.model.event.http.GenericErrorSortAvailableEvent;
import br.com.iprice.model.event.http.NetworkErrorSortAvailableEvent;
import br.com.iprice.model.event.http.NotFoundSortAvailableEvent;
import br.com.iprice.model.event.http.ServerErrorSortAvailableEvent;
import br.com.iprice.model.event.http.UnauthorizedSortAvailableEvent;
import br.com.iprice.presentation.ui.fragment.SortAvailableView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class SortAvailablePresenterImpl implements SortAvailablePresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    @Bean(SortAvailableServiceImpl.class)
    SortAvailableService sortAvailableService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    private SortAvailableView sortAvailableView;

    @Override
    public void attachView(SortAvailableView sortAvailableView) {
        this.sortAvailableView = sortAvailableView;
        this.sortAvailableView.setupRecyclerView();
    }

    @Override
    public void registerBus() {
        sortAvailableService.registerBus();
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        sortAvailableService.unregisterBus();
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void recoverAllAvailableSort() {
        sortAvailableService.recoverAllAvailableSort();
    }

    @Override
    public void participateSort(SortVO sortVO) {
        sortAvailableView.showDialogParticipateSort();
        sortAvailableService.participateSort(sortVO);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadInsufficientPointsEvent(InsufficientPointsEvent insufficientPointsEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showInsufficientPoints();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRefreshAvailableSortEvent(RefreshAvailableSortEvent refreshAvailableSortEvent) {
        sortAvailableView.updateAllAvailableSort(refreshAvailableSortEvent.getSortVOList());
        sortAvailableView.hideDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAllAvailableSortEvent(AllAvailableSortEvent allAvailableSortEvent) {
        sortAvailableView.insertAllAvailableSort(allAvailableSortEvent.getSortVOList());
        sortAvailableView.hideProgress();
        sortAvailableView.showRecyclerView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAllAvailableSortEmptyEvent(AllAvailableSortEmptyEvent allAvailableSortEmptyEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.hideRecyclerView();
        sortAvailableView.hideProgress();
        sortAvailableView.showEmptyState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorSortAvailableEvent(NetworkErrorSortAvailableEvent networkErrorSortAvailableEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadConvertErrorSortAvaiableEvent(ConvertErrorSortAvaiableEvent convertErrorSortAvaiableEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorSortAvailableEvent(GenericErrorSortAvailableEvent genericErrorSortAvailableEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestSortAvailableEvent(BadRequestSortAvailableEvent badRequestSortAvailableEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showDialogTryAgain(badRequestSortAvailableEvent.getMessage() != null ? badRequestSortAvailableEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedSortAvailableEvent(UnauthorizedSortAvailableEvent unauthorizedSortAvailableEvent) {
        sortAvailableView.hideDialog();
        userService.logout();
        sortAvailableView.showDialogUnauthorized(unauthorizedSortAvailableEvent.getMessage() != null ? unauthorizedSortAvailableEvent.getMessage() : unauthorizedSortAvailableEvent.getMessage() != null ? unauthorizedSortAvailableEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenSortAvailableEvent(ForbiddenSortAvailableEvent forbiddenSortAvailableEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showDialogTryAgain(forbiddenSortAvailableEvent.getMessage() != null ? forbiddenSortAvailableEvent.getMessage() : forbiddenSortAvailableEvent.getMessage() != null ? forbiddenSortAvailableEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundSortAvailableEvent(NotFoundSortAvailableEvent notFoundSortAvailableEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showDialogTryAgain(notFoundSortAvailableEvent.getMessage() != null ? notFoundSortAvailableEvent.getMessage() : notFoundSortAvailableEvent.getMessage() != null ? notFoundSortAvailableEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorSortAvailableEvent(ServerErrorSortAvailableEvent serverErrorSortAvailableEvent) {
        sortAvailableView.hideDialog();
        sortAvailableView.showDialogTryAgain(serverErrorSortAvailableEvent.getMessage() != null ? serverErrorSortAvailableEvent.getMessage() : serverErrorSortAvailableEvent.getMessage() != null ? serverErrorSortAvailableEvent.getMessage() : null);
    }
}

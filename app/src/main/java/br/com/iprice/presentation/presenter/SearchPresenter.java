package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.SearchItemsEmptyEvent;
import br.com.iprice.model.event.SearchItemsFilteredEvent;
import br.com.iprice.presentation.ui.fragment.SearchView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SearchPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {

    void attachView(SearchView searchView);

    void searchItems(String query);

    void onLoadSearchItemsFilteredEvent(SearchItemsFilteredEvent searchItemsFilteredEvent);

    void onLoadSearchItemsEmptyEvent(SearchItemsEmptyEvent searchItemsEmptyEvent);
}

package br.com.iprice.presentation.presenter;

import android.support.annotation.NonNull;

import com.facebook.AccessToken;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.InputStream;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.TermsUseEvent;
import br.com.iprice.model.event.UnexpectedErrorFacebookEvent;
import br.com.iprice.model.event.UserFieldsInvalidEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.activity.LoginView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class LoginPresenterImpl implements LoginPresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;
    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;
    private LoginView loginView;

    @Override
    public void attachView(LoginView loginView) {
        this.loginView = loginView;
        this.loginView.setupFacebookSdk();
        this.loginView.connectLocation();
    }

    @Override
    public void validateFields(@NonNull String email, @NonNull String password) {
        loginView.clearMessageError();
        loginView.showDialogValidateUser();
        userService.validateFields(email, password);
    }

    @Override
    public void recoverTermsUse(InputStream inputStream) {
        loginView.showDialogLoading();
        userService.recoverTermsUse(inputStream);
    }

    @Override
    public void recoverUserFacebook(AccessToken accessToken) {
        loginView.showDialogValidateUser();
        userService.recoverUserFacebook(accessToken);
    }

    @Override
    public void registerBus() {
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUserFieldsInvalidEvent(UserFieldsInvalidEvent userFieldsInvalidEvent) {
        loginView.hideDialog();
        if (userFieldsInvalidEvent.isEmailInvalid()) {
            loginView.showInvalidEmail();
        }

        if (userFieldsInvalidEvent.isPasswordInvalid()) {
            loginView.showInvalidPassword();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnexpectedErrorFacebookEvent(UnexpectedErrorFacebookEvent unexpectedErrorFacebookEvent) {
        loginView.hideDialog();
        loginView.showDialogTryAgainFacebook();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadTermsUseEvent(TermsUseEvent termsUseEvent) {
        loginView.hideDialog();
        loginView.showTermsUse(termsUseEvent.getTermsUser());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadDuplicateUserErrorEvent(DuplicateUserErrorEvent duplicateUserErrorEvent) {
        loginView.hideDialog();
        loginView.redirectToHome();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        loginView.hideDialog();
        loginView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        loginView.hideDialog();
        loginView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        loginView.hideDialog();
        loginView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        loginView.hideDialog();
        loginView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        loginView.hideDialog();
        userService.logout();
        loginView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        loginView.hideDialog();
        loginView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        loginView.hideDialog();
        loginView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        loginView.hideDialog();
        loginView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

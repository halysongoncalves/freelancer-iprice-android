package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.SortVO;
import br.com.iprice.presentation.ui.viewholder.SortParticipateViewHolder;
import br.com.iprice.presentation.ui.viewholder.SortParticipateViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class SortParticipateAdapter extends BaseRecyclerViewAdapter<SortVO, SortParticipateViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected SortParticipateViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return SortParticipateViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<SortVO, SortParticipateViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }
}
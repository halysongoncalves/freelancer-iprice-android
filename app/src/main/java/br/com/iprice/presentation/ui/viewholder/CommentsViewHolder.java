package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.presentation.ui.views.ViewWrapper;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_comment)
public class CommentsViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<CommentVO> {
    private final Drawable drawableUser;
    @ViewById(R.id.view_item_comment_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.view_item_comment_text_view_comment)
    AppCompatTextView appCompatTextViewComment;

    @ViewById(R.id.view_item_comment_image_view_user)
    CircleImageView circleImageView;

    public CommentsViewHolder(Context context) {
        super(context);

        drawableUser = ContextCompat.getDrawable(getContext(), R.drawable.placeholder_user_medium);
    }

    @Override
    public void bind(CommentVO data, int itemPositionType) {
        if (data.getUserPhotoUrl() != null) {
            Picasso.with(getContext()).load(data.getUserPhotoUrl())
                    .resize(drawableUser.getIntrinsicWidth(), drawableUser.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_user_medium)
                    .into(circleImageView);
        }

        if (data.getUserName() != null) {
            appCompatTextViewName.setText(data.getUserName());
        }

        if (data.getCommentDescription() != null) {
            appCompatTextViewComment.setText(data.getCommentDescription());
        }
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
    }
}

package br.com.iprice.presentation.presenter;

import java.util.ArrayList;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.AddProductWishListEvent;
import br.com.iprice.model.event.LikedProductEvent;
import br.com.iprice.model.event.RecoverAllProductsEmptyEvent;
import br.com.iprice.model.event.RecoverAllProductsEvent;
import br.com.iprice.model.event.RemoveProductWishListEvent;
import br.com.iprice.model.event.UserAlreadyLikeEvent;
import br.com.iprice.presentation.ui.fragment.FeedView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface FeedPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {

    void attachView(FeedView feedView);

    void recoverAllFeed(ArrayList<ProductVO> productVOArrayList);

    void like(ProductVO productVO, int position);

    void wishList(ProductVO productVO, int position);

    void onLoadUserAlreadyLikeEvent(UserAlreadyLikeEvent userAlreadyLikeEvent);

    void onLoadLikedProductEvent(LikedProductEvent likedProductEvent);

    void onLoadRecoverAllProductsEvent(RecoverAllProductsEvent recoverAllProductsEvent);

    void onLoadRecoverAllUsersEmptyEvent(RecoverAllProductsEmptyEvent recoverAllProductsEmptyEvent);

    void onLoadAddProductWishListEvent(AddProductWishListEvent addProductWishListEvent);

    void onLoadRemoveProductWishListEvent(RemoveProductWishListEvent removeProductWishListEvent);
}

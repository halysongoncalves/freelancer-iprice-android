package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.RecoverMyPointsEvent;
import br.com.iprice.presentation.ui.activity.MyPointsView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface MyPointsPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {
    void attachView(MyPointsView myPointsView);

    void recoverMyPoints();

    void onLoadRecoverMyPointsEvent(RecoverMyPointsEvent recoverMyPointsEvent);

}

package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.SessionIsInvalidEvent;
import br.com.iprice.presentation.ui.activity.SplashView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SplashPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {
    void attachView(SplashView splashView);

    void userIsValid();

    void onLoadDuplicateUserErrorEvent(DuplicateUserErrorEvent duplicateUserErrorEvent);

    void onLoadSessionIsInvalidEvent(SessionIsInvalidEvent sessionIsInvalidEvent);
}

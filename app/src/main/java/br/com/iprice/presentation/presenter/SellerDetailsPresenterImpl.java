package br.com.iprice.presentation.presenter;

import android.graphics.Bitmap;
import android.net.Uri;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.MyPointsService;
import br.com.iprice.domain.MyPointsServiceImpl;
import br.com.iprice.domain.PhotoService;
import br.com.iprice.domain.PhotoServiceImpl;
import br.com.iprice.domain.ProductService;
import br.com.iprice.domain.ProductServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.event.AddNewProductEvent;
import br.com.iprice.model.event.BuilderTakePictureFiveEvent;
import br.com.iprice.model.event.BuilderTakePictureFourEvent;
import br.com.iprice.model.event.BuilderTakePictureOneEvent;
import br.com.iprice.model.event.BuilderTakePictureTWoEvent;
import br.com.iprice.model.event.BuilderTakePictureThreeEvent;
import br.com.iprice.model.event.PhotoFiveChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoFourChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoOneChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoThreeChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoTwoChoiceFromGalleryEvent;
import br.com.iprice.model.event.PointsUpdatedEvent;
import br.com.iprice.model.event.SellerProductInvalidEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.activity.SellerDetailsView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class SellerDetailsPresenterImpl implements SellerDetailsPresenter {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(PhotoServiceImpl.class)
    PhotoService photoService;

    @Bean(ProductServiceImpl.class)
    ProductService productService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    @Bean(MyPointsServiceImpl.class)
    MyPointsService myPointsService;

    private SellerDetailsView sellerDetailsView;

    @Override
    public void attachView(SellerDetailsView sellerDetailsView, Bitmap bitmap, File fileImageOne) {
        this.sellerDetailsView = sellerDetailsView;
        this.sellerDetailsView.setupToolbar();
        this.sellerDetailsView.showOnePicture(bitmap);
        this.sellerDetailsView.showOnePicture(fileImageOne);
    }

    @Override
    public void registerBus() {
        requestErrorService.registerBus();
        productService.registerBus();
        myPointsService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        requestErrorService.unregisterBus();
        productService.unregisterBus();
        myPointsService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void validateFields(File imageOne, Bitmap bitmapOne, File fileImageTwo, Bitmap bitmapTwo, File fileImageThree, Bitmap bitmapThree, File fileImageFour, Bitmap bitmapFour, File fileImageFive, Bitmap bitmapImageFive, String name, String message, String price, String phone) {
        sellerDetailsView.clearMessageError();
        sellerDetailsView.showDialogValidate();
        productService.validateFields(imageOne, bitmapOne, fileImageTwo, bitmapTwo, fileImageThree, bitmapThree, fileImageFour, bitmapFour, fileImageFive, bitmapImageFive, name, message, price, phone);
    }

    @Override
    public void selectedImageOneFromGallery(Uri data) {
        sellerDetailsView.showDialogLoading();
        photoService.selectedImageOneFromGallery(data);
    }

    @Override
    public void builderTakePictureOne(Bitmap bitmap) {
        sellerDetailsView.showDialogLoading();
        photoService.builderTakePictureOne(bitmap);
    }

    @Override
    public void selectedImageTwoFromGallery(Uri data) {
        sellerDetailsView.showDialogLoading();
        photoService.selectedImageTwoFromGallery(data);
    }

    @Override
    public void builderTakePictureTwo(Bitmap bitmap) {
        sellerDetailsView.showDialogLoading();
        photoService.builderTakePictureTwo(bitmap);
    }

    @Override
    public void selectedImageThreeFromGallery(Uri data) {
        sellerDetailsView.showDialogLoading();
        photoService.selectedImageThreeFromGallery(data);
    }

    @Override
    public void builderTakePictureThree(Bitmap bitmap) {
        sellerDetailsView.showDialogLoading();
        photoService.builderTakePictureThree(bitmap);
    }

    @Override
    public void selectedImageFourFromGallery(Uri data) {
        sellerDetailsView.showDialogLoading();
        photoService.selectedImageFourFromGallery(data);
    }

    @Override
    public void builderTakePictureFour(Bitmap bitmap) {
        sellerDetailsView.showDialogLoading();
        photoService.builderTakePictureFour(bitmap);
    }

    @Override
    public void selectedImageFiveFromGallery(Uri data) {
        sellerDetailsView.showDialogLoading();
        photoService.selectedImageFiveFromGallery(data);
    }

    @Override
    public void builderTakePictureFive(Bitmap bitmap) {
        sellerDetailsView.showDialogLoading();
        photoService.builderTakePictureFive(bitmap);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadSellerProductInvalidEvent(SellerProductInvalidEvent sellerProductInvalidEvent) {
        if (sellerProductInvalidEvent.isNameInvalid()) {
            sellerDetailsView.showInvalidName();
        }

        if (sellerProductInvalidEvent.isMessageInvalid()) {
            sellerDetailsView.showInvalidMessage();
        }

        if (sellerProductInvalidEvent.isPriceInvalid()) {
            sellerDetailsView.showInvalidPrice();
        }

        if (sellerProductInvalidEvent.isPhoneInvalid()) {
            sellerDetailsView.showInvalidPhone();
        }

        if (sellerProductInvalidEvent.isPictureInvalid()) {
            sellerDetailsView.showImageInvalid();
        }

        sellerDetailsView.hideDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBuilderTakePictureOneEvent(BuilderTakePictureOneEvent builderTakePictureOneEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showOnePicture(builderTakePictureOneEvent.getBitmap());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPhotoOneChoiceFromGalleryEvent(PhotoOneChoiceFromGalleryEvent photoOneChoiceFromGalleryEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showOnePicture(photoOneChoiceFromGalleryEvent.getFile());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBuilderTakePictureTwoEvent(BuilderTakePictureTWoEvent builderTakePictureTWoEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showTwoPicture(builderTakePictureTWoEvent.getBitmap());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPhotoTwoChoiceFromGalleryEvent(PhotoTwoChoiceFromGalleryEvent photoTwoChoiceFromGalleryEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showTwoPicture(photoTwoChoiceFromGalleryEvent.getFile());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBuilderTakePictureThreeEvent(BuilderTakePictureThreeEvent builderTakePictureThreeEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showThreePicture(builderTakePictureThreeEvent.getBitmap());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPhotoThreeChoiceFromGalleryEvent(PhotoThreeChoiceFromGalleryEvent photoThreeChoiceFromGalleryEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showThreePicture(photoThreeChoiceFromGalleryEvent.getFile());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBuilderTakePictureFourEvent(BuilderTakePictureFourEvent builderTakePictureFourEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showFourPicture(builderTakePictureFourEvent.getBitmap());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPhotoFourChoiceFromGalleryEvent(PhotoFourChoiceFromGalleryEvent photoFourChoiceFromGalleryEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showFourPicture(photoFourChoiceFromGalleryEvent.getFile());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBuilderTakePictureFiveEvent(BuilderTakePictureFiveEvent builderTakePictureFiveEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showFivePicture(builderTakePictureFiveEvent.getBitmap());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPhotoFiveChoiceFromGalleryEvent(PhotoFiveChoiceFromGalleryEvent photoFiveChoiceFromGalleryEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showFivePicture(photoFiveChoiceFromGalleryEvent.getFile());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAddNewProductEvent(AddNewProductEvent addNewProductEvent) {
        myPointsService.addNewPoints();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPointsUpdatedEvent(PointsUpdatedEvent pointsUpdatedEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showMessageAddProductSuccess();
        sellerDetailsView.redirectToFeed();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        sellerDetailsView.hideDialog();
        userService.logout();
        sellerDetailsView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        sellerDetailsView.hideDialog();
        sellerDetailsView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.SortParticipateService;
import br.com.iprice.domain.SortParticipateServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.event.AllParticipateSortEmptyEvent;
import br.com.iprice.model.event.AllParticipateSortEvent;
import br.com.iprice.model.event.http.BadRequestSortParticipateEvent;
import br.com.iprice.model.event.http.ConvertErrorSortParticipateEvent;
import br.com.iprice.model.event.http.ForbiddenSortParticipateEvent;
import br.com.iprice.model.event.http.GenericErrorSortParticipateEvent;
import br.com.iprice.model.event.http.NetworkErrorSortParticipateEvent;
import br.com.iprice.model.event.http.NotFoundSortParticipateEvent;
import br.com.iprice.model.event.http.ServerErrorSortParticipateEvent;
import br.com.iprice.model.event.http.UnauthorizedSortParticipateEvent;
import br.com.iprice.presentation.ui.fragment.SortParticipateView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class SortParticipatePresenterImpl implements SortParticipatePresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(SortParticipateServiceImpl.class)
    SortParticipateService sortParticipateService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    private SortParticipateView sortParticipateView;

    @Override
    public void attachView(SortParticipateView sortParticipateView) {
        this.sortParticipateView = sortParticipateView;
        this.sortParticipateView.setupRecyclerView();
    }

    @Override
    public void registerBus() {
        sortParticipateService.registerBus();
        userService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        sortParticipateService.unregisterBus();
        userService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void recoverAllSortParticipate() {
        sortParticipateService.recoverAllSortParticipate();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAllParticipateSortEvent(AllParticipateSortEvent allParticipateSortEvent) {
        sortParticipateView.insertAllParticipateSort(allParticipateSortEvent.getSortVOList());
        sortParticipateView.hideProgress();
        sortParticipateView.showRecyclerView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAllParticipateSortEmptyEvent(AllParticipateSortEmptyEvent allParticipateSortEmptyEvent) {
        sortParticipateView.hideProgress();
        sortParticipateView.showEmptyState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorSortParticipateEvent(NetworkErrorSortParticipateEvent networkErrorSortParticipateEvent) {
        sortParticipateView.hideDialog();
        sortParticipateView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadConvertErrorSortParticipateEvent(ConvertErrorSortParticipateEvent convertErrorSortParticipateEvent) {
        sortParticipateView.hideDialog();
        sortParticipateView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorSortParticipateEvent(GenericErrorSortParticipateEvent genericErrorSortParticipateEvent) {
        sortParticipateView.hideDialog();
        sortParticipateView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestSortParticipateEvent(BadRequestSortParticipateEvent badRequestSortParticipateEvent) {
        sortParticipateView.hideDialog();
        sortParticipateView.showDialogTryAgain(badRequestSortParticipateEvent.getMessage() != null ? badRequestSortParticipateEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedSortParticipateEvent(UnauthorizedSortParticipateEvent unauthorizedSortParticipateEvent) {
        sortParticipateView.hideDialog();
        userService.logout();
        sortParticipateView.showDialogUnauthorized(unauthorizedSortParticipateEvent.getMessage() != null ? unauthorizedSortParticipateEvent.getMessage() : unauthorizedSortParticipateEvent.getMessage() != null ? unauthorizedSortParticipateEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenSortParticipateEvent(ForbiddenSortParticipateEvent forbiddenSortParticipateEvent) {
        sortParticipateView.hideDialog();
        sortParticipateView.showDialogTryAgain(forbiddenSortParticipateEvent.getMessage() != null ? forbiddenSortParticipateEvent.getMessage() : forbiddenSortParticipateEvent.getMessage() != null ? forbiddenSortParticipateEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundSortParticipateEvent(NotFoundSortParticipateEvent notFoundSortParticipateEvent) {
        sortParticipateView.hideDialog();
        sortParticipateView.showDialogTryAgain(notFoundSortParticipateEvent.getMessage() != null ? notFoundSortParticipateEvent.getMessage() : notFoundSortParticipateEvent.getMessage() != null ? notFoundSortParticipateEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorSortParticipateEvent(ServerErrorSortParticipateEvent serverErrorSortParticipateEvent) {
        sortParticipateView.hideDialog();
        sortParticipateView.showDialogTryAgain(serverErrorSortParticipateEvent.getMessage() != null ? serverErrorSortParticipateEvent.getMessage() : serverErrorSortParticipateEvent.getMessage() != null ? serverErrorSortParticipateEvent.getMessage() : null);
    }
}

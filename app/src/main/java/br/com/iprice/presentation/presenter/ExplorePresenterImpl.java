package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.ExploreService;
import br.com.iprice.domain.ExploreServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.RecoverAllProductsExploreEmptyEvent;
import br.com.iprice.model.event.RecoverAllProductsExploreEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.fragment.ExploreView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class ExplorePresenterImpl implements ExplorePresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    @Bean(ExploreServiceImpl.class)
    ExploreService exploreService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    private ExploreView exploreView;

    @Override
    public void attachView(ExploreView exploreView) {
        this.exploreView = exploreView;
        this.exploreView.setupGoogleMaps();
    }

    @Override
    public void exploreAllProduct(double latitude, double longitude, ArrayList<ProductVO> productVOArrayList) {
        exploreService.exploreAllProduct(latitude, longitude, productVOArrayList);
    }

    @Override
    public void registerBus() {
        exploreService.registerBus();
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        exploreService.unregisterBus();
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRecoverAllProductsExploreEvent(RecoverAllProductsExploreEvent recoverAllProductsExploreEvent) {
        exploreView.insertAllProducts(recoverAllProductsExploreEvent.getProductVOList());
        exploreView.hideProgress();
        exploreView.showMaps();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRecoverAllProductsExploreEmptyEvent(RecoverAllProductsExploreEmptyEvent recoverAllProductsExploreEmptyEvent) {
        exploreView.hideProgress();
        exploreView.showEmptyState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        exploreView.hideDialog();
        exploreView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        exploreView.hideDialog();
        exploreView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        exploreView.hideDialog();
        exploreView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        exploreView.hideDialog();
        exploreView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        exploreView.hideDialog();
        userService.logout();
        exploreView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        exploreView.hideDialog();
        exploreView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        exploreView.hideDialog();
        exploreView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        exploreView.hideDialog();
        exploreView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

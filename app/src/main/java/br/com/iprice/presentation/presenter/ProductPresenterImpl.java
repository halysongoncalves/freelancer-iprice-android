package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.ProductService;
import br.com.iprice.domain.ProductServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.AddCommentEvent;
import br.com.iprice.model.event.BuilderPagesEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.fragment.ProductInfoView;
import cn.lightsky.infiniteindicator.page.OnPageClickListener;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class ProductPresenterImpl implements ProductPresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    @Bean(ProductServiceImpl.class)
    ProductService productService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    private ProductInfoView productInfoView;

    @Override
    public void attachView(ProductInfoView productInfoView) {
        this.productInfoView = productInfoView;
        this.productInfoView.setupRecyclerView();
        this.productInfoView.setupInfiniteIndicator();
    }

    @Override
    public void recoverProductDetails(ProductVO productVO, OnPageClickListener onPageClickListener) {
        productService.builderPages(productVO, onPageClickListener);

        if (productVO != null) {
            if (productVO.getUserPhotoUrl() != null && !productVO.getUserPhotoUrl().isEmpty()) {
                productInfoView.showUserPhoto(productVO.getUserPhotoUrl());
            }

            if (productVO.getName() != null && !productVO.getName().isEmpty()) {
                productInfoView.showProductName(productVO.getName());
            }

            if (productVO.getUserName() != null && !productVO.getUserName().isEmpty()) {
                productInfoView.showOwner(productVO.getUserName());
            }

            if (productVO.getCity() != null && !productVO.getCity().isEmpty()) {
                productInfoView.showCity(productVO.getCity());
            }

            if (productVO.getDescription() != null && !productVO.getDescription().isEmpty()) {
                productInfoView.showDescription(productVO.getDescription());
            }

            if (productVO.getPhone() != null && !productVO.getPhone().isEmpty()) {
                productInfoView.showPhone(productVO.getPhone());
            }

            if (productVO.getCommentVOList() != null && !productVO.getCommentVOList().isEmpty()) {
                productInfoView.insertAllComments(productVO.getCommentVOList());
                productInfoView.showViewComments();
            } else {
                productInfoView.showEmptyState();
            }
        }

        productInfoView.hideProgress();
    }

    @Override
    public void addComment(String productId, String comment) {
        productInfoView.showDialogAddComment();
        productService.addComment(productId, comment);
    }

    @Override
    public void registerBus() {
        productService.registerBus();
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        productService.unregisterBus();
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAddCommentEvent(AddCommentEvent addCommentEvent) {
        productInfoView.hideEmptyState();
        productInfoView.hideDialog();
        productInfoView.clearComment();
        productInfoView.insertComment(addCommentEvent.getCommentVO());
        productInfoView.disableButtonSend();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBuilderPagesEvent(BuilderPagesEvent builderPagesEvent) {
        productInfoView.insertAllPhotos(builderPagesEvent.getPageList());
        productInfoView.hideProgress();
        productInfoView.showViewComments();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        productInfoView.hideDialog();
        productInfoView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        productInfoView.hideDialog();
        productInfoView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        productInfoView.hideDialog();
        productInfoView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        productInfoView.hideDialog();
        productInfoView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        productInfoView.hideDialog();
        userService.logout();
        productInfoView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        productInfoView.hideDialog();
        productInfoView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        productInfoView.hideDialog();
        productInfoView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        productInfoView.hideDialog();
        productInfoView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

package br.com.iprice.presentation.ui.activity;

import java.io.File;

import br.com.iprice.domain.LocationClient;
import br.com.iprice.presentation.ui.views.BaseActivityView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface RegisterView extends BaseActivityView, LocationClient.ConnectionCallback {
    void clearMessageError();

    void showInvalidName();

    void showInvalidEmail();

    void showInvalidPassword();

    void showDialogValidateUser();

    void showDialogLoading();

    void showDialogLoadImage();

    void showImageSelectedFromGallery(File file);

    void redirectToHome();

    void connectLocation();

    void showDialogInvalidUser();
}

package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;

/**
 * Created by HalysonLima on 07/12/15.
 */
public interface SplashView extends DialogInterface.OnCancelListener {

    void showDialogTryAgain(String message);

    void showDialogConnection();

    void showDialogUnauthorized(String message);

    void hideDialog();

    void redirectToLogin();

    void redirectToHome();
}

package br.com.iprice.presentation.ui.viewholder;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.iprice.R;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.presentation.ui.views.ViewWrapper;


/**
 * Created by Halyson on 17/05/16.
 */
@EViewGroup(R.layout.view_item_sort_participate)
public class SortParticipateViewHolder extends LinearLayoutCompat implements ViewWrapper.Binder<SortVO> {
    @ViewById(R.id.view_item_sort_participate_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.view_item_sort_participate_text_view_description)
    AppCompatTextView appCompatTextViewDescription;

    @ViewById(R.id.view_item_sort_participate_text_view_points_value)
    AppCompatTextView appCompatTextViewPoints;

    public SortParticipateViewHolder(Context context) {
        super(context);
    }

    @Override
    public void bind(SortVO data, int itemPositionType) {
        if (data.getName() != null) {
            appCompatTextViewName.setText(data.getName());
        }

        if (data.getDescription() != null) {
            appCompatTextViewDescription.setText(data.getDescription());
        }

        appCompatTextViewPoints.setText(String.valueOf(data.getPoints()));
    }

    @Override
    public void setClickLister(OnClickListener listener, int position) {
    }
}

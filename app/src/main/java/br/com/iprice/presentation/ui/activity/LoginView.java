package br.com.iprice.presentation.ui.activity;

import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;

import br.com.iprice.domain.LocationClient;
import br.com.iprice.presentation.ui.views.BaseView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface LoginView extends BaseView, FacebookCallback<LoginResult>, LocationClient.ConnectionCallback {
    void connectLocation();

    void setupFacebookSdk();

    void clearMessageError();

    void showInvalidEmail();

    void showInvalidPassword();

    void showDialogValidateUser();

    void showDialogLoading();

    void showTermsUse(String termsUse);

    void redirectToHome();

    void showDialogTryAgainFacebook();
}

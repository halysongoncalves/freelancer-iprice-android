package br.com.iprice.presentation.ui.adapter;

import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.viewholder.SearchViewHolder;
import br.com.iprice.presentation.ui.viewholder.SearchViewHolder_;
import br.com.iprice.presentation.ui.views.ViewWrapper;

@EBean
public class SearchAdapter extends BaseRecyclerViewAdapter<ProductVO, SearchViewHolder, ViewWrapper.OnItemClickListener> {
    @Override
    protected SearchViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return SearchViewHolder_.build(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ProductVO, SearchViewHolder> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
    }
}
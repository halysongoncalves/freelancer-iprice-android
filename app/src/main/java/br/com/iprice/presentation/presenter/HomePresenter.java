package br.com.iprice.presentation.presenter;

import br.com.iprice.presentation.ui.activity.HomeView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface HomePresenter {

    void attachView(HomeView homeView);

    void logout();
}

package br.com.iprice.presentation.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.model.entities.SectionBeanVO;


/**
 * Created by halysongoncalves on 08/11/15.
 */
public class TabLayoutAdapter extends FragmentStatePagerAdapter {
    private List<SectionBeanVO> sectionBeanVOList = new ArrayList<>();

    public TabLayoutAdapter(FragmentManager fragmentManager, List<SectionBeanVO> sectionBeanVOList) {
        super(fragmentManager);
        this.sectionBeanVOList = sectionBeanVOList;
    }

    @Override
    public Fragment getItem(int position) {
        return sectionBeanVOList.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return sectionBeanVOList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return sectionBeanVOList.get(position).getTitle();
    }
}

package br.com.iprice.presentation.ui.fragment;

import java.util.List;

import br.com.iprice.model.entities.SortVO;
import br.com.iprice.presentation.ui.views.BaseView;
import br.com.iprice.presentation.ui.views.ViewWrapper;

public interface SortAvailableView extends BaseView, ViewWrapper.OnItemClickListener {

    void setupRecyclerView();

    void hideProgress();

    void showEmptyState();

    void hideEmptyState();

    void showProgress();

    void showRecyclerView();

    void hideRecyclerView();

    void insertAllAvailableSort(List<SortVO> sortVOList);

    void showDialogParticipateSort();

    void updateAllAvailableSort(List<SortVO> sortVOList);

    void showInsufficientPoints();

}

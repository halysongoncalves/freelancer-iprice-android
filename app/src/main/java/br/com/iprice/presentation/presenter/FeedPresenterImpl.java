package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.FeedService;
import br.com.iprice.domain.FeedServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.AddProductWishListEvent;
import br.com.iprice.model.event.LikedProductEvent;
import br.com.iprice.model.event.RecoverAllProductsEmptyEvent;
import br.com.iprice.model.event.RecoverAllProductsEvent;
import br.com.iprice.model.event.RemoveProductWishListEvent;
import br.com.iprice.model.event.UserAlreadyLikeEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.fragment.FeedView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class FeedPresenterImpl implements FeedPresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(FeedServiceImpl.class)
    FeedService feedService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    private FeedView feedView;

    @Override
    public void attachView(FeedView loginView) {
        this.feedView = loginView;
        this.feedView.setupRecyclerView();
    }

    @Override
    public void registerBus() {
        feedService.registerBus();
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        feedService.unregisterBus();
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void recoverAllFeed(ArrayList<ProductVO> productVOArrayList) {
        feedService.recoverAllFeed(productVOArrayList);
    }

    @Override
    public void like(ProductVO productVO, int position) {
        feedView.showDialogAddLike();
        feedService.like(productVO, position);
    }

    @Override
    public void wishList(ProductVO productVO, int position) {
        feedView.showDialogAddWishList();
        feedService.wishList(productVO, position);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRemoveProductWishListEvent(RemoveProductWishListEvent removeProductWishListEvent) {
        feedView.hideDialog();
        feedView.updateProductRemoveWishList(removeProductWishListEvent.getPosition());
        feedView.showSnackBarRemoveWishList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAddProductWishListEvent(AddProductWishListEvent addProductWishListEvent) {
        feedView.hideDialog();
        feedView.updateProductAddWishList(addProductWishListEvent.getPosition());
        feedView.showSnackBarAddWishList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUserAlreadyLikeEvent(UserAlreadyLikeEvent userAlreadyLikeEvent) {
        feedView.hideDialog();
        feedView.showInvalidLike();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadLikedProductEvent(LikedProductEvent likedProductEvent) {
        feedView.hideDialog();
        feedView.updateLike(likedProductEvent.getProductVO().getLikes(), likedProductEvent.getPosition());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRecoverAllProductsEvent(RecoverAllProductsEvent recoverAllProductsEvent) {
        feedView.hideProgress();
        feedView.insertAllProducts(recoverAllProductsEvent.getProductVOList());
        feedView.showRecyclerView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRecoverAllUsersEmptyEvent(RecoverAllProductsEmptyEvent recoverAllProductsEmptyEvent) {
        feedView.hideProgress();
        feedView.showEmptyState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        feedView.hideDialog();
        feedView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        feedView.hideDialog();
        feedView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        feedView.hideDialog();
        feedView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        feedView.hideDialog();
        feedView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        feedView.hideDialog();
        userService.logout();
        feedView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        feedView.hideDialog();
        feedView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        feedView.hideDialog();
        feedView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        feedView.hideDialog();
        feedView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;

import br.com.iprice.BuildConfig;
import br.com.iprice.R;
import br.com.iprice.presentation.presenter.BuyPointsPresenter;
import br.com.iprice.presentation.presenter.BuyPointsPresenterImpl;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EActivity(R.layout.activity_buy_points)
public class BuyPointsActivity extends AppCompatActivity implements BuyPointsView {
    static final String EXTRA_MY_POINTS = "extra_my_points";
    private static final int REQUEST_CODE_PAYMENT = 2342;
    private static final float VALUE_INITIAL = 0f;
    private static final String URL = "https://www.iprice.co";
    private static final String CURRENCY = "BRL";
    private static final String APP_NAME = "iPrice";
    private final static PayPalConfiguration payPalConfiguration = new PayPalConfiguration()
            .acceptCreditCards(true)
            .merchantName(APP_NAME)
            .clientId(BuildConfig.PAYPAL)
            .merchantPrivacyPolicyUri(Uri.parse(URL))
            .merchantUserAgreementUri(Uri.parse(URL))
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION);
    @ViewById(R.id.view_toolbar_toolbar)
    Toolbar toolbar;
    @ViewById(R.id.activity_buy_points_edit_text_points_value)
    AppCompatEditText appCompatEditTextPointsValue;
    @ViewById(R.id.activity_buy_points_text_view_my_points)
    AppCompatTextView appCompatTextViewMyPoints;
    @ViewById(R.id.activity_buy_points_text_view_total)
    AppCompatTextView appCompatTextViewTotal;
    @ViewById(R.id.activity_buy_points_content_main)
    View viewContentRoot;
    @Extra(EXTRA_MY_POINTS)
    @InstanceState
    int myPoints;
    @Bean(BuyPointsPresenterImpl.class)
    BuyPointsPresenter buyPointsPresenter;
    private MaterialDialog materialDialog;
    private Timer timer;

    @AfterViews
    void afterViews() {

        buyPointsPresenter.registerBus();
        buyPointsPresenter.attachView(this, VALUE_INITIAL);
    }

    @Override
    protected void onDestroy() {
        buyPointsPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.activity_buy_points_title);
        }
    }

    @OptionsItem(android.R.id.home)
    void clickMenuHome() {
        finish();
    }

    @OnActivityResult(REQUEST_CODE_PAYMENT)
    void onResultPayment(int resultCode, @OnActivityResult.Extra(PaymentActivity.EXTRA_RESULT_CONFIRMATION) PaymentConfirmation paymentConfirmation) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                if (paymentConfirmation != null) {
                    buyPointsPresenter.updatePoints(appCompatEditTextPointsValue.getText().toString());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void showDialogValidateBuyPoints() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_loading_data))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showDialogSave() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_save))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(BuyPointsActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void showMyPoints() {
        appCompatTextViewMyPoints.setText(getString(R.string.activity_buy_points_text_view_my_points, myPoints));
    }

    @Click(R.id.activity_buy_points_button_buy)
    void clickButtonBuy() {
        buyPointsPresenter.validateBuyPoints(appCompatEditTextPointsValue.getText().toString());
    }

    @Override
    @UiThread
    public void showValueTotal(float valueTotal) {
        appCompatTextViewTotal.setText(getString(R.string.activity_buy_points_text_view_total, valueTotal));
    }

    @Override
    public void showInvalidPoints() {
        Snackbar.make(viewContentRoot, R.string.activity_buy_points_invalid_quantity_points, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void redirectToPayPal(int buyPoints) {
        final BigDecimal bigDecimal = calculatePrice(buyPoints);
        final PayPalPayment payPalPayment = builderPayPalPayment(builderPayPalItem(bigDecimal));

        if (payPalPayment.isProcessable()) {
            startActivityForResult(new Intent(this, PaymentActivity.class)
                            .putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, payPalConfiguration)
                            .putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment),
                    REQUEST_CODE_PAYMENT);
            return;
        }

        Snackbar.make(viewContentRoot, R.string.activity_buy_points_invalid_payment_not_processable, Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public void redirectToMyPoints(int point) {
        Toast.makeText(this, R.string.activity_buy_points_toast_updated_points, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK, new Intent().putExtra(EXTRA_MY_POINTS, point));
        finish();
    }

    @AfterTextChange(R.id.activity_buy_points_edit_text_points_value)
    void afterTextChangeAddress(final Editable editable) {
        final String search = editable.toString();
        if (search.length() > 0) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    final int quantity = Integer.parseInt(editable.toString());
                    showValueTotal(quantity / 10);
                }
            }, 500);
        }
    }

    @TextChange(R.id.activity_buy_points_edit_text_points_value)
    void textChangeAddress() {
        if (timer != null) {
            timer.cancel();
        }
    }

    private PayPalPayment builderPayPalPayment(PayPalItem[] payPalItems) {
        PayPalPayment payPalPayment = new PayPalPayment(PayPalItem.getItemTotal(payPalItems), CURRENCY, APP_NAME, PayPalPayment.PAYMENT_INTENT_SALE);
        payPalPayment.items(payPalItems);

        return payPalPayment;
    }

    private PayPalItem[] builderPayPalItem(BigDecimal bigDecimal) {
        return new PayPalItem[]{new PayPalItem(APP_NAME, 1, bigDecimal, CURRENCY, "sku-0001")};
    }

    private BigDecimal calculatePrice(int buyPoints) {
        return new BigDecimal(buyPoints / 10);
    }
}

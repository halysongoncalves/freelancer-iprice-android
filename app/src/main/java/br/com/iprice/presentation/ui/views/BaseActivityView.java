package br.com.iprice.presentation.ui.views;

/**
 * Created by halysongoncalves on 07/02/16.
 */
public interface BaseActivityView extends BaseView {
    void setupToolbar();

}

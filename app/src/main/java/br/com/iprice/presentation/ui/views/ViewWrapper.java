package br.com.iprice.presentation.ui.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Halyson on 17/05/16.
 */
public class ViewWrapper<T, V extends View & ViewWrapper.Binder<T>> extends RecyclerView.ViewHolder {
    private V view;
    private OnItemClickListener listener;

    public ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }

    public OnItemClickListener getListener() {
        return listener;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface Binder<T> {
        void bind(T data, int itemPositionType);

        void setClickLister(View.OnClickListener listener, int position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
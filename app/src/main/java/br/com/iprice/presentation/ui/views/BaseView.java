package br.com.iprice.presentation.ui.views;

import android.content.DialogInterface;

/**
 * Created by halysongoncalves on 07/02/16.
 */
public interface BaseView extends DialogInterface.OnCancelListener {
    void showDialogTryAgain(String message);

    void showDialogConnection();

    void showDialogUnauthorized(String message);

    void hideDialog();
}

package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.MyPointsService;
import br.com.iprice.domain.MyPointsServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.RecoverMyPointsEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.activity.MyPointsView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class MyPointsPresenterImpl implements MyPointsPresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(MyPointsServiceImpl.class)
    MyPointsService myPointsService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    private MyPointsView myPointsView;

    @Override
    public void attachView(MyPointsView myPointsView) {
        this.myPointsView = myPointsView;
        this.myPointsView.setupRecyclerView();
        this.myPointsView.setupToolbar();
    }

    @Override
    public void registerBus() {
        requestErrorService.registerBus();
        myPointsService.registerBus();
        userService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        requestErrorService.unregisterBus();
        myPointsService.unregisterBus();
        userService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void recoverMyPoints() {
        myPointsService.recoverMyPoints();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRecoverMyPointsEvent(RecoverMyPointsEvent recoverMyPointsEvent) {
        final UserVO userVO = recoverMyPointsEvent.getUserVO();

        if (userVO != null) {
            if (userVO.getName() != null) {
                myPointsView.showName(userVO.getName());
                myPointsView.showNameLink(userVO.getName());
            }

            if (userVO.getCategory() != null) {
                myPointsView.showCategory(userVO.getCategory());
            }

            if (userVO.getPhotoUrl() != null) {
                myPointsView.showPhoto(userVO.getPhotoUrl());
            }

            myPointsView.showMyPoints(userVO.getPoints());
        }

        myPointsView.showRecordsPoints(recoverMyPointsEvent.getRecordsPoints());
        myPointsView.insertAllPointsVO(recoverMyPointsEvent.getPointsVOList());

        myPointsView.showContentData();
        myPointsView.hideProgress();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        myPointsView.hideDialog();
        myPointsView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        myPointsView.hideDialog();
        myPointsView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        myPointsView.hideDialog();
        myPointsView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        myPointsView.hideDialog();
        myPointsView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        myPointsView.hideDialog();
        userService.logout();
        myPointsView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        myPointsView.hideDialog();
        myPointsView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        myPointsView.hideDialog();
        myPointsView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        myPointsView.hideDialog();
        myPointsView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

package br.com.iprice.presentation.ui.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.adapter.TabLayoutAdapter;
import br.com.iprice.presentation.ui.views.BaseActivityView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ProductDetailsView extends BaseActivityView {
    void setupViewPager(ViewPager viewPager, TabLayoutAdapter tabLayoutAdapter);

    void setupTabLayout(TabLayout tabLayout, ViewPager viewPager);

    TabLayoutAdapter recoverSectionsFragment(ProductVO productVO);

    TabLayout getTabLayout();

    ViewPager getViewPager();
}

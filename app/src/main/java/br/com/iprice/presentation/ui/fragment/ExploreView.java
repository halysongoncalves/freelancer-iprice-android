package br.com.iprice.presentation.ui.fragment;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import java.util.List;

import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.ui.views.BaseView;

public interface ExploreView extends BaseView, OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    void setupGoogleMaps();

    void showEmptyState();

    void hideProgress();

    void showMaps();

    void insertAllProducts(List<ProductVO> productVOList);
}

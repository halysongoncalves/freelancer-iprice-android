package br.com.iprice.presentation.presenter;

import android.net.Uri;

import java.io.File;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.PhotoChoiceFromGalleryEvent;
import br.com.iprice.model.event.RegisterSuccessEvent;
import br.com.iprice.model.event.UserFieldsInvalidEvent;
import br.com.iprice.presentation.ui.activity.RegisterView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface RegisterPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {

    void attachView(RegisterView registerView);

    void validateFields(String name, String email, String password, File file);

    void selectedImageFromGallery(Uri data);

    void onLoadRegisterSuccessEvent(RegisterSuccessEvent registerSuccessEvent);

    void onLoadUserFieldsInvalidEvent(UserFieldsInvalidEvent userFieldsInvalidEvent);

    void onLoadDuplicateUserErrorEvent(DuplicateUserErrorEvent duplicateUserErrorEvent);

    void onLoadPhotoChoiceFromGalleryEvent(PhotoChoiceFromGalleryEvent photoChoiceFromGalleryEvent);

}

package br.com.iprice.presentation.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.R;
import br.com.iprice.domain.LocationClient;
import br.com.iprice.domain.LocationClientImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.presenter.FeedPresenter;
import br.com.iprice.presentation.presenter.FeedPresenterImpl;
import br.com.iprice.presentation.ui.activity.HomeActivity;
import br.com.iprice.presentation.ui.activity.LoginActivity_;
import br.com.iprice.presentation.ui.activity.ProductDetailsActivity_;
import br.com.iprice.presentation.ui.adapter.FeedAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EFragment(R.layout.fragment_feed)
public class FeedFragment extends Fragment implements FeedView {
    public static final String EXTRA_PRODUCT = "extra_product";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @ViewById(R.id.fragment_feed_recycler_view)
    RecyclerView recyclerView;
    @ViewById(R.id.fragment_feed_progress)
    ProgressBar progressBar;
    @ViewById(R.id.fragment_feed_text_view_empty)
    AppCompatTextView appCompatTextViewEmptyState;
    @ViewById(R.id.fragment_feed_content_root)
    View viewContentRoot;
    @Bean(FeedPresenterImpl.class)
    FeedPresenter feedPresenter;
    @Bean
    FeedAdapter feedAdapter;
    @InstanceState
    ArrayList<ProductVO> productVOArrayList;
    @Bean(LocationClientImpl.class)
    LocationClient locationClient;

    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        setHasOptionsMenu(true);
        feedPresenter.registerBus();
        feedPresenter.attachView(this);
        feedPresenter.recoverAllFeed(productVOArrayList);
    }

    @Override
    public void onDestroy() {
        feedPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupRecyclerView() {
        feedAdapter.setListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(feedAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_feed, menu);
        final MenuItem searchItem = menu.findItem(R.id.menu_feed_search);
        searchItem.setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_feed_search:
                ((HomeActivity) getActivity()).redirectToSearch();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showRecyclerView() {
        recyclerView.setVisibility(VISIBLE);
    }

    @Override
    public void showEmptyState() {
        appCompatTextViewEmptyState.setVisibility(VISIBLE);
    }

    @Override
    public void hideRecyclerView() {
        recyclerView.setVisibility(GONE);
    }

    @Override
    public void hideEmptyState() {
        appCompatTextViewEmptyState.setVisibility(GONE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void insertAllProducts(List<ProductVO> productVOList) {
        this.productVOArrayList = (ArrayList<ProductVO>) productVOList;
        feedAdapter.addAll(productVOList);
    }

    @Override
    public void updateLike(int like, int position) {
        feedAdapter.get(position).setLikes(like);
        feedAdapter.notifyDataSetChanged();
    }

    @Override
    public void showInvalidLike() {
        Snackbar.make((ViewGroup) viewContentRoot.getParent(), R.string.fragment_feed_snack_bar_invalid_like, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogAddWishList() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .content(getString(R.string.dialog_title_save))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showSnackBarAddWishList() {
        Snackbar.make(viewContentRoot, getString(R.string.fragment_feed_snack_bar_add_wish_list), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void updateProductAddWishList(int position) {
        feedAdapter.get(position).setWishlist(true);
        feedAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateProductRemoveWishList(int position) {
        feedAdapter.get(position).setWishlist(false);
        feedAdapter.notifyDataSetChanged();
    }


    @Override
    public void showSnackBarRemoveWishList() {
        Snackbar.make(viewContentRoot, getString(R.string.fragment_feed_snack_bar_remove_wish_list), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogAddLike() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .content(getString(R.string.dialog_title_save))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_try_again)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        feedPresenter.recoverAllFeed(productVOArrayList);
                    }
                })
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(FeedFragment.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void onItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.view_item_fed_image_view_wish_list:
                feedPresenter.wishList(feedAdapter.get(position), position);
                break;

            case R.id.view_item_fed_text_view_message:
                openProductDetails(position);
                break;

            case R.id.view_item_fed_content_root:
                openProductDetails(position);
                break;

            case R.id.view_item_fed_text_view_tag:
                feedPresenter.like(feedAdapter.get(position), position);
                break;

            default:
                break;
        }

    }

    private void openProductDetails(int position) {
        ProductDetailsActivity_
                .intent(this)
                .extra(EXTRA_PRODUCT, feedAdapter.get(position))
                .start();
    }
}

package br.com.iprice.presentation.presenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.domain.WishListService;
import br.com.iprice.domain.WishListServiceImpl;
import br.com.iprice.model.event.AllWishListEmptyEvent;
import br.com.iprice.model.event.RecoverAllWishListEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.fragment.WishListView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class WishListPresenterImpl implements WishListPresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(WishListServiceImpl.class)
    WishListService wishListService;

    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    private WishListView wishListView;

    @Override
    public void attachView(WishListView wishListView) {
        this.wishListView = wishListView;
        this.wishListView.setupRecyclerView();
    }

    @Override
    public void registerBus() {
        wishListService.registerBus();
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        wishListService.unregisterBus();
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Override
    public void recoverAllWishList() {
        wishListService.recoverAllWishList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRecoverAllWishListEvent(RecoverAllWishListEvent recoverAllWishListEvent) {
        wishListView.insertAllProduct(recoverAllWishListEvent.getProductVOList());
        wishListView.hideProgress();
        wishListView.showRecyclerView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadAllWishListEmptyEvent(AllWishListEmptyEvent allWishListEmptyEvent) {
        wishListView.hideProgress();
        wishListView.showEmptyState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        wishListView.hideDialog();
        wishListView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        wishListView.hideDialog();
        wishListView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        wishListView.hideDialog();
        wishListView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        wishListView.hideDialog();
        wishListView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        wishListView.hideDialog();
        userService.logout();
        wishListView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        wishListView.hideDialog();
        wishListView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        wishListView.hideDialog();
        wishListView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        wishListView.hideDialog();
        wishListView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

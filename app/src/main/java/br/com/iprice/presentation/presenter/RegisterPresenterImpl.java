package br.com.iprice.presentation.presenter;

import android.net.Uri;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.PhotoService;
import br.com.iprice.domain.PhotoServiceImpl;
import br.com.iprice.domain.RequestErrorService;
import br.com.iprice.domain.RequestErrorServiceImpl;
import br.com.iprice.domain.UserService;
import br.com.iprice.domain.UserServiceImpl;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.PhotoChoiceFromGalleryEvent;
import br.com.iprice.model.event.RegisterSuccessEvent;
import br.com.iprice.model.event.UserFieldsInvalidEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import br.com.iprice.presentation.ui.activity.RegisterView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class RegisterPresenterImpl implements RegisterPresenter {
    @Bean(UserServiceImpl.class)
    UserService userService;
    @Bean(RequestErrorServiceImpl.class)
    RequestErrorService requestErrorService;
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;
    @Bean(PhotoServiceImpl.class)
    PhotoService photoService;
    private RegisterView registerView;

    @Override
    public void attachView(RegisterView registerView) {
        this.registerView = registerView;
        this.registerView.setupToolbar();
        this.registerView.connectLocation();
    }

    @Override
    public void validateFields(String name, String email, String password, File file) {
        registerView.clearMessageError();
        registerView.showDialogValidateUser();
        userService.validateFields(name, email, password, file);
    }

    @Override
    public void selectedImageFromGallery(Uri data) {
        registerView.showDialogLoadImage();
        photoService.selectedImageFromGallery(data);
    }

    @Override
    public void registerBus() {
        userService.registerBus();
        requestErrorService.registerBus();
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        userService.unregisterBus();
        requestErrorService.unregisterBus();
        busProvider.getServiceBus().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUserFieldsInvalidEvent(UserFieldsInvalidEvent userFieldsInvalidEvent) {
        registerView.hideDialog();

        if (userFieldsInvalidEvent.isNameInvalid()) {
            registerView.showInvalidName();
        }

        if (userFieldsInvalidEvent.isEmailInvalid()) {
            registerView.showInvalidEmail();
        }

        if (userFieldsInvalidEvent.isPasswordInvalid()) {
            registerView.showInvalidPassword();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadRegisterSuccessEvent(RegisterSuccessEvent registerSuccessEvent) {
        registerView.hideDialog();
        registerView.redirectToHome();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadDuplicateUserErrorEvent(DuplicateUserErrorEvent duplicateUserErrorEvent) {
        registerView.hideDialog();
        registerView.showDialogInvalidUser();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPhotoChoiceFromGalleryEvent(PhotoChoiceFromGalleryEvent photoChoiceFromGalleryEvent) {
        registerView.hideDialog();
        registerView.showImageSelectedFromGallery(photoChoiceFromGalleryEvent.getFile());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent) {
        registerView.hideDialog();
        registerView.showDialogConnection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent) {
        registerView.hideDialog();
        registerView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent) {
        registerView.hideDialog();
        registerView.showDialogTryAgain(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBadRequestEvent(BadRequestEvent badRequestEvent) {
        registerView.hideDialog();
        registerView.showDialogTryAgain(badRequestEvent.getMessage() != null ? badRequestEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent) {
        registerView.hideDialog();
        userService.logout();
        registerView.showDialogUnauthorized(unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : unauthorizedEvent.getMessage() != null ? unauthorizedEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent) {
        registerView.hideDialog();
        registerView.showDialogTryAgain(forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : forbiddenEvent.getMessage() != null ? forbiddenEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadNotFoundEvent(NotFoundEvent notFoundEvent) {
        registerView.hideDialog();
        registerView.showDialogTryAgain(notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : notFoundEvent.getMessage() != null ? notFoundEvent.getMessage() : null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent) {
        registerView.hideDialog();
        registerView.showDialogTryAgain(serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : serverErrorEvent.getMessage() != null ? serverErrorEvent.getMessage() : null);
    }
}

package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.AllWishListEmptyEvent;
import br.com.iprice.model.event.RecoverAllWishListEvent;
import br.com.iprice.presentation.ui.fragment.WishListView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface WishListPresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {
    void attachView(WishListView wishListView);

    void recoverAllWishList();

    void onLoadRecoverAllWishListEvent(RecoverAllWishListEvent recoverAllWishListEvent);

    void onLoadAllWishListEmptyEvent(AllWishListEmptyEvent allWishListEmptyEvent);

}

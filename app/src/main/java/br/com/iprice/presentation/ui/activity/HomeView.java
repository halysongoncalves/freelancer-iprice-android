package br.com.iprice.presentation.ui.activity;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;

import br.com.iprice.domain.LocationClient;
import br.com.iprice.presentation.ui.views.BaseActivityView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface HomeView extends BaseActivityView, NavigationView.OnNavigationItemSelectedListener, LocationClient.ConnectionCallback {
    void setupNavigationView(NavigationView navigationView, DrawerLayout drawerLayout);

    void fragmentTransaction(Fragment fragment, int fragmentName);

    void connectLocation();

    void setTitleToolbar(String title);

    Toolbar getToolbar();

    NavigationView getNavigationView();

    DrawerLayout getDrawerLayout();

    void redirectToFeed();

    void redirectToSearch();

    void hideProgress();

    void showContent();
}

package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.RecoverMyPostEvent;
import br.com.iprice.presentation.ui.activity.ProfileView;
import br.com.iprice.repository.http.HttpFailure;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ProfilePresenter extends Bus, HttpFailure, HttpFailure.ServerError, HttpFailure.ClientError {
    void attachView(ProfileView profileView);

    void recoverMyPost();

    void deleteProduct(ProductVO productVO);

    void onLoadRecoverMyPostEvent(RecoverMyPostEvent recoverMyPostEvent);
}

package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.io.File;

import br.com.iprice.R;
import br.com.iprice.presentation.presenter.SellerDetailsPresenter;
import br.com.iprice.presentation.presenter.SellerDetailsPresenterImpl;
import de.hdodenhof.circleimageview.CircleImageView;

import static br.com.iprice.presentation.ui.fragment.SellerFragment.DATA;
import static br.com.iprice.presentation.ui.fragment.SellerFragment.EXTRA_PHOTO_GALLERY;
import static br.com.iprice.presentation.ui.fragment.SellerFragment.EXTRA_TAKE_PICTURE;

@EActivity(R.layout.activity_seller_details)
public class SellerDetailsActivity extends AppCompatActivity implements SellerDetailsView {
    private static final int OPEN_GALLERY = 1;

    private static final int REQUEST_CODE_IMAGE_ONE_GALLERY = 6435;
    private static final int REQUEST_CODE_IMAGE_ONE_TAKE_PICTURE = 6786;

    private static final int REQUEST_CODE_IMAGE_TWO_GALLERY = 5412;
    private static final int REQUEST_CODE_IMAGE_TWO_TAKE_PICTURE = 4321;

    private static final int REQUEST_CODE_IMAGE_THREE_GALLERY = 3412;
    private static final int REQUEST_CODE_IMAGE_THREE_TAKE_PICTURE = 4343;

    private static final int REQUEST_CODE_IMAGE_FOUR_GALLERY = 4562;
    private static final int REQUEST_CODE_IMAGE_FOUR_TAKE_PICTURE = 6752;

    private static final int REQUEST_CODE_IMAGE_FIVE_GALLERY = 3456;
    private static final int REQUEST_CODE_IMAGE_FIVE_TAKE_PICTURE = 5467;
    @ViewById(R.id.activity_seller_details_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.activity_seller_image_view_one)
    CircleImageView circleImageViewOne;

    @ViewById(R.id.activity_seller_image_view_two)
    CircleImageView circleImageViewTwo;

    @ViewById(R.id.activity_seller_image_view_three)
    CircleImageView circleImageViewThree;

    @ViewById(R.id.activity_seller_image_view_four)
    CircleImageView circleImageViewFour;

    @ViewById(R.id.activity_seller_image_view_five)
    CircleImageView circleImageViewFive;

    @ViewById(R.id.activity_seller_edit_text_name)
    AppCompatEditText appCompatEditTextName;

    @ViewById(R.id.activity_seller_text_input_name)
    TextInputLayout textInputLayoutName;

    @ViewById(R.id.activity_seller_edit_text_price)
    AppCompatEditText appCompatEditTextPrice;

    @ViewById(R.id.activity_seller_text_input_price)
    TextInputLayout textInputLayoutPrice;

    @ViewById(R.id.activity_seller_edit_text_phone)
    AppCompatEditText appCompatEditTextPhone;

    @ViewById(R.id.activity_seller_text_input_phone)
    TextInputLayout textInputLayoutPhone;

    @ViewById(R.id.activity_contact_edit_text_message)
    AppCompatEditText appCompatEditTextMessage;

    @ViewById(R.id.activity_seller_details_content_main)
    View viewContentRoot;

    @Bean(SellerDetailsPresenterImpl.class)
    SellerDetailsPresenter sellerDetailsPresenter;

    @Extra(EXTRA_TAKE_PICTURE)
    @InstanceState
    Bitmap bitmapOne;

    @InstanceState
    Bitmap bitmapTwo;

    @InstanceState
    Bitmap bitmapThree;

    @InstanceState
    Bitmap bitmapFour;

    @InstanceState
    Bitmap bitmapFive;

    @Extra(EXTRA_PHOTO_GALLERY)
    @InstanceState
    File fileImageOne;

    @InstanceState
    File fileImageTwo;

    @InstanceState
    File fileImageThree;

    @InstanceState
    File fileImageFour;

    @InstanceState
    File fileImageFive;

    private MaterialDialog materialDialog;
    private Drawable drawableProduct;

    @AfterViews
    void afterViews() {
        drawableProduct = ContextCompat.getDrawable(this, R.drawable.placeholder_image_small);
        sellerDetailsPresenter.registerBus();
        sellerDetailsPresenter.attachView(this, bitmapOne, fileImageOne);
    }

    @Override
    protected void onDestroy() {
        sellerDetailsPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupToolbar() {
        if (null != toolbar) {
            setSupportActionBar(toolbar);
        }
        if (null != getSupportActionBar()) {
            getSupportActionBar().setTitle(R.string.activity_seller_details_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_ONE_GALLERY)
    void onResultChoiceImageOneFromGallery(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.selectedImageOneFromGallery(data.getData());
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_ONE_TAKE_PICTURE)
    void onResultTakePictureOne(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.builderTakePictureOne((Bitmap) data.getExtras().get(DATA));
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_TWO_GALLERY)
    void onResultChoiceImageTwoFromGallery(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.selectedImageTwoFromGallery(data.getData());
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_TWO_TAKE_PICTURE)
    void onResultTakePictureTwo(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.builderTakePictureTwo((Bitmap) data.getExtras().get(DATA));
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_THREE_GALLERY)
    void onResultChoiceImageThreeFromGallery(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.selectedImageThreeFromGallery(data.getData());
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_THREE_TAKE_PICTURE)
    void onResultTakePictureThree(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.builderTakePictureThree((Bitmap) data.getExtras().get(DATA));
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_FOUR_GALLERY)
    void onResultChoiceImageFourFromGallery(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.selectedImageFourFromGallery(data.getData());
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_FOUR_TAKE_PICTURE)
    void onResultTakePictureFour(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.builderTakePictureFour((Bitmap) data.getExtras().get(DATA));
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_FIVE_GALLERY)
    void onResultChoiceImageFiveFromGallery(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.selectedImageFiveFromGallery(data.getData());
                break;

            default:
                break;
        }
    }

    @OnActivityResult(REQUEST_CODE_IMAGE_FIVE_TAKE_PICTURE)
    void onResultTakePictureFive(int resultCode, Intent data) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                sellerDetailsPresenter.builderTakePictureFive((Bitmap) data.getExtras().get(DATA));
                break;

            default:
                break;
        }
    }

    @OptionsItem(android.R.id.home)
    void clickMenuHome() {
        finish();
    }

    @Click(R.id.activity_seller_details_button_seller)
    void clickButtonSeller() {
        sellerDetailsPresenter.validateFields(fileImageOne, bitmapOne, fileImageTwo, bitmapTwo, fileImageThree, bitmapThree, fileImageFour, bitmapFour, fileImageFive, bitmapFive,
                appCompatEditTextName.getText().toString(), appCompatEditTextMessage.getText().toString()
                , appCompatEditTextPrice.getText().toString(), appCompatEditTextPhone.getText().toString());
    }

    @Click(R.id.activity_seller_image_view_one)
    void clickImageOne() {
        new MaterialDialog.Builder(this)
                .title(R.string.activity_seller_title_chooser)
                .items(R.array.activity_seller_array_options_dialog)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (which == OPEN_GALLERY) {
                            openGallery(REQUEST_CODE_IMAGE_ONE_GALLERY);
                            return false;
                        }
                        takePicture(REQUEST_CODE_IMAGE_ONE_TAKE_PICTURE);
                        return false;
                    }
                })
                .negativeText(R.string.dialog_button_ok)
                .show();
    }


    @Click(R.id.activity_seller_image_view_two)
    void clickImageTwo() {
        new MaterialDialog.Builder(this)
                .title(R.string.activity_seller_title_chooser)
                .items(R.array.activity_seller_array_options_dialog)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (which == OPEN_GALLERY) {
                            openGallery(REQUEST_CODE_IMAGE_TWO_GALLERY);
                            return false;
                        }
                        takePicture(REQUEST_CODE_IMAGE_TWO_TAKE_PICTURE);
                        return false;
                    }
                })
                .negativeText(R.string.dialog_button_ok)
                .show();
    }

    @Click(R.id.activity_seller_image_view_three)
    void clickImageThree() {
        new MaterialDialog.Builder(this)
                .title(R.string.activity_seller_title_chooser)
                .items(R.array.activity_seller_array_options_dialog)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (which == OPEN_GALLERY) {
                            openGallery(REQUEST_CODE_IMAGE_THREE_GALLERY);
                            return false;
                        }
                        takePicture(REQUEST_CODE_IMAGE_THREE_TAKE_PICTURE);
                        return false;
                    }
                })
                .negativeText(R.string.dialog_button_ok)
                .show();
    }

    @Click(R.id.activity_seller_image_view_four)
    void clickImageFour() {
        new MaterialDialog.Builder(this)
                .title(R.string.activity_seller_title_chooser)
                .items(R.array.activity_seller_array_options_dialog)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (which == OPEN_GALLERY) {
                            openGallery(REQUEST_CODE_IMAGE_FOUR_GALLERY);
                            return false;
                        }
                        takePicture(REQUEST_CODE_IMAGE_FOUR_TAKE_PICTURE);
                        return false;
                    }
                })
                .negativeText(R.string.dialog_button_ok)
                .show();
    }

    @Click(R.id.activity_seller_image_view_five)
    void clickImageFive() {
        new MaterialDialog.Builder(this)
                .title(R.string.activity_seller_title_chooser)
                .items(R.array.activity_seller_array_options_dialog)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (which == OPEN_GALLERY) {
                            openGallery(REQUEST_CODE_IMAGE_FIVE_GALLERY);
                            return false;
                        }
                        takePicture(REQUEST_CODE_IMAGE_FIVE_TAKE_PICTURE);
                        return false;
                    }
                })
                .negativeText(R.string.dialog_button_ok)
                .show();
    }

    @Override
    public void showDialogLoading() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_loading_data))
                .progress(true, 0)
                .show();
    }

    @Override
    public void redirectToFeed() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void showMessageAddProductSuccess() {
        Toast.makeText(this, getString(R.string.activity_seller_toast_add_product_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogValidate() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_validate_data))
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(SellerDetailsActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void showOnePicture(Bitmap bitmap) {
        if (bitmap != null) {
            this.bitmapOne = bitmap;
            circleImageViewOne.setImageBitmap(bitmap);
        }
    }

    @Override
    public void showOnePicture(File fileImageOne) {
        if (fileImageOne != null) {
            this.fileImageOne = fileImageOne;
            Picasso.with(getApplicationContext())
                    .load(fileImageOne)
                    .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(circleImageViewOne);
        }
    }

    @Override
    public void showTwoPicture(Bitmap bitmapImageTwo) {
        if (bitmapImageTwo != null) {
            this.bitmapTwo = bitmapImageTwo;
            circleImageViewTwo.setImageBitmap(bitmapImageTwo);
        }
    }

    @Override
    public void showTwoPicture(File fileImageTwo) {
        if (fileImageTwo != null) {
            this.fileImageTwo = fileImageTwo;
            Picasso.with(getApplicationContext())
                    .load(fileImageTwo)
                    .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(circleImageViewTwo);
        }
    }

    @Override
    public void showThreePicture(Bitmap bitmapImageThree) {
        if (bitmapImageThree != null) {
            circleImageViewThree.setImageBitmap(bitmapImageThree);
            this.bitmapThree = bitmapImageThree;
        }
    }

    @Override
    public void showThreePicture(File fileImageThree) {
        if (fileImageThree != null) {
            this.fileImageThree = fileImageThree;
            Picasso.with(getApplicationContext())
                    .load(fileImageThree)
                    .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(circleImageViewThree);
        }
    }

    @Override
    public void showFourPicture(Bitmap bitmapImageFour) {
        if (bitmapImageFour != null) {
            circleImageViewFour.setImageBitmap(bitmapImageFour);
            this.bitmapFour = bitmapImageFour;
        }
    }

    @Override
    public void showFourPicture(File fileImageFour) {
        if (fileImageFour != null) {
            this.fileImageFour = fileImageFour;
            Picasso.with(getApplicationContext())
                    .load(fileImageFour)
                    .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(circleImageViewFour);
        }
    }

    @Override
    public void showFivePicture(Bitmap bitmapImageFive) {
        if (bitmapImageFive != null) {
            circleImageViewFive.setImageBitmap(bitmapImageFive);
            this.bitmapFive = bitmapImageFive;
        }
    }

    @Override
    public void showFivePicture(File fileImageFive) {
        if (fileImageFive != null) {
            this.fileImageFive = fileImageFive;
            Picasso.with(getApplicationContext())
                    .load(fileImageFive)
                    .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(circleImageViewFive);
        }
    }

    @Override
    public void clearMessageError() {
        textInputLayoutName.setErrorEnabled(false);
        textInputLayoutName.setError(null);

        textInputLayoutPhone.setErrorEnabled(false);
        textInputLayoutPhone.setError(null);

        textInputLayoutPrice.setErrorEnabled(false);
        textInputLayoutPrice.setError(null);
    }

    @Override
    public void showInvalidName() {
        textInputLayoutName.setError(getString(R.string.activity_seller_edit_text_invalid_name));
    }

    @Override
    public void showInvalidPhone() {
        textInputLayoutPhone.setError(getString(R.string.activity_seller_edit_text_invalid_phone));
    }

    @Override
    public void showInvalidPrice() {
        textInputLayoutPrice.setError(getString(R.string.activity_seller_edit_text_invalid_price));
    }

    @Override
    public void showInvalidMessage() {
        Snackbar.make(viewContentRoot, R.string.activity_seller_edit_text_invalid_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showImageInvalid() {
        Snackbar.make(viewContentRoot, R.string.activity_seller_image_invalid, Snackbar.LENGTH_LONG).show();
    }

    private void openGallery(int requestCode) {
        Intent intent = new Intent()
                .setType("image/*")
                .setAction(Intent.ACTION_PICK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        }
        startActivityForResult(Intent.createChooser(intent, getString(R.string.fragment_seller_title_chooser)), requestCode);
    }

    private void takePicture(int requestCode) {
        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), requestCode);
    }
}


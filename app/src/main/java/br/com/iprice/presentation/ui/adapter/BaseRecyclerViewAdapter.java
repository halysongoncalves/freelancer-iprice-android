package br.com.iprice.presentation.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import br.com.iprice.presentation.ui.views.ViewWrapper;


public abstract class BaseRecyclerViewAdapter<T, V extends View & ViewWrapper.Binder<T>, L extends ViewWrapper.OnItemClickListener> extends RecyclerView.Adapter<ViewWrapper<T, V>> implements List<T>, View.OnClickListener {
    private final Object lock = new Object();
    private final List<T> items = new ArrayList<>();
    private L listener;

    protected abstract V onCreateItemView(ViewGroup parent, int viewType);

    @Override
    public final ViewWrapper<T, V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper<>(onCreateItemView(parent, viewType));
    }

    @Override
    public void onBindViewHolder(ViewWrapper<T, V> viewHolder, int position) {
        viewHolder.getView().bind(getItem(position), getItemPositionType(position));
        viewHolder.setListener(getListener());
        viewHolder.getView().setClickLister(this, position);
    }

    @Override
    public void onClick(View view) {
        if (getListener() != null && view.getTag() != null) {
            getListener().onItemClick(view, (int) view.getTag());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public void add(int location, T object) {
        synchronized (lock) {
            items.add(location, object);
            notifyItemInserted(location);
        }
    }

    @Override
    public boolean add(T object) {
        synchronized (lock) {
            int lastIndex = items.size();
            if (items.add(object)) {
                notifyItemInserted(lastIndex);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean addAll(int location, Collection<? extends T> collection) {
        synchronized (lock) {
            if (items.addAll(location, collection)) {
                notifyDataSetChanged();
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        synchronized (lock) {
            if (items.addAll(collection)) {
                notifyDataSetChanged();
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public synchronized void clear() {
        synchronized (lock) {
            int size = items.size();
            if (size > 0) {
                items.clear();
                notifyItemRangeRemoved(0, size);
            }
        }
    }

    @Override
    public boolean contains(Object object) {
        return items.contains(object);
    }

    @Override
    public boolean containsAll(@NonNull Collection<?> collection) {
        return items.containsAll(collection);
    }

    @Override
    public T get(int location) {
        return items.get(location);
    }

    @Override
    public int indexOf(Object object) {
        return items.indexOf(object);
    }

    @Override
    public boolean isEmpty() {
        return items.isEmpty();
    }

    public List<T> getList() {
        return items;
    }

    @NonNull
    @Override
    public Iterator<T> iterator() {
        return items.iterator();
    }

    @Override
    public int lastIndexOf(Object object) {
        return items.lastIndexOf(object);
    }

    @NonNull
    @Override
    public ListIterator<T> listIterator() {
        return items.listIterator();
    }

    @NonNull
    @Override
    public ListIterator<T> listIterator(int location) {
        return items.listIterator(location);
    }

    @Override
    public T remove(int location) {
        synchronized (lock) {
            T item = items.remove(location);
            notifyItemRemoved(location);
            return item;
        }
    }

    @Override
    public boolean remove(Object object) {
        synchronized (lock) {
            int index = indexOf(object);
            if (items.remove(object)) {
                notifyItemRemoved(index);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> collection) {
        boolean modified = false;

        Iterator<T> iterator = items.iterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            if (collection.contains(object)) {
                synchronized (lock) {
                    int index = indexOf(object);
                    iterator.remove();
                    notifyItemRemoved(index);
                }

                modified = true;
            }
        }

        return modified;
    }

    @Override
    public boolean retainAll(@NonNull Collection<?> collection) {
        boolean modified = false;

        Iterator<T> iterator = items.iterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            if (!collection.contains(object)) {
                synchronized (lock) {
                    int index = indexOf(object);
                    iterator.remove();
                    notifyItemRemoved(index);
                }

                modified = true;
            }
        }

        return modified;
    }

    @Override
    public T set(int location, T object) {
        synchronized (lock) {
            T origin = items.set(location, object);
            notifyItemChanged(location);
            return origin;
        }
    }

    @Override
    public int size() {
        return items.size();
    }

    @NonNull
    @Override
    public List<T> subList(int start, int end) {
        return items.subList(start, end);
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return items.toArray();
    }

    @NonNull
    @Override
    public <T> T[] toArray(@NonNull T[] array) {
        return items.toArray(array);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof List && items.equals(o);
    }

    public L getListener() {
        return listener;
    }

    public void setListener(L listener) {
        this.listener = listener;
    }

    private int getItemPositionType(int position) {
        if (position == 0)
            return 0;
        else if (position == (items.size() - 1))
            return 2;
        return 1;
    }

}
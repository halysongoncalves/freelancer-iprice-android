package br.com.iprice.presentation.presenter;

import android.graphics.Bitmap;
import android.net.Uri;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.domain.PhotoService;
import br.com.iprice.domain.PhotoServiceImpl;
import br.com.iprice.model.event.BuilderTakePictureEvent;
import br.com.iprice.model.event.PhotoChoiceFromGalleryEvent;
import br.com.iprice.presentation.ui.fragment.SellerView;

/**
 * Created by halysongoncalves on 26/07/16.
 */
@EBean
public class SellerPresenterImpl implements SellerPresenter {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(PhotoServiceImpl.class)
    PhotoService photoService;

    private SellerView sellerView;

    @Override
    public void attachView(SellerView sellerView) {
        this.sellerView = sellerView;
    }

    @Override
    public void selectedImageFromGallery(Uri data) {
        sellerView.showDialogLoading();
        photoService.selectedImageFromGallery(data);
    }

    @Override
    public void builderTakePicture(Bitmap bitmap) {
        sellerView.showDialogLoading();
        photoService.builderTakePicture(bitmap);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadPhotoChoiceFromGalleryEvent(PhotoChoiceFromGalleryEvent photoChoiceFromGalleryEvent) {
        sellerView.hideDialog();
        sellerView.redirectToSellerDetails(photoChoiceFromGalleryEvent.getFile());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadBuilderTakePictureEvent(BuilderTakePictureEvent builderTakePictureEvent) {
        sellerView.hideDialog();
        sellerView.redirectToSellerDetails(builderTakePictureEvent.getBitmap());
    }

    @Override
    public void registerBus() {
        busProvider.getServiceBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getServiceBus().unregister(this);
    }
}

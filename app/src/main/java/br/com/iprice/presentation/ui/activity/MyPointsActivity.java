package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.iprice.R;
import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.presentation.presenter.MyPointsPresenter;
import br.com.iprice.presentation.presenter.MyPointsPresenterImpl;
import br.com.iprice.presentation.ui.adapter.PointsAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static br.com.iprice.presentation.ui.activity.BuyPointsActivity.EXTRA_MY_POINTS;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EActivity(R.layout.activity_my_points)
public class MyPointsActivity extends AppCompatActivity implements MyPointsView {
    private static final int REQUEST_CODE_BUY_POINTS = 5421;
    @ViewById(R.id.view_toolbar_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.activity_my_points_text_view_name)
    AppCompatTextView appCompatTextViewName;

    @ViewById(R.id.activity_my_points_text_view_name_link)
    AppCompatTextView appCompatTextViewNameLink;

    @ViewById(R.id.activity_my_points_text_view_my_points)
    AppCompatTextView appCompatTextViewMyPoints;

    @ViewById(R.id.activity_my_points_text_view_records_points)
    AppCompatTextView appCompatTextViewRecordsPoints;

    @ViewById(R.id.activity_my_points_text_view_category)
    AppCompatTextView appCompatTextViewCategory;

    @ViewById(R.id.activity_my_point_recycler_view_types)
    RecyclerView recyclerView;

    @ViewById(R.id.activity_my_points_fab_user_photo)
    CircleImageView circleImageView;

    @ViewById(R.id.activity_my_points_progress)
    ProgressBar progressBar;

    @ViewById(R.id.activity_my_points_content_data)
    View viewContentData;

    @Bean(MyPointsPresenterImpl.class)
    MyPointsPresenter myPointsPresenter;

    @Bean
    PointsAdapter pointsAdapter;

    @InstanceState
    int myPoints;

    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        myPointsPresenter.registerBus();
        myPointsPresenter.attachView(this);
        myPointsPresenter.recoverMyPoints();
    }

    @Override
    protected void onDestroy() {
        myPointsPresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.activity_my_points_title);
        }
    }

    @Override
    public void setupRecyclerView() {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(pointsAdapter);
    }

    @OptionsItem(android.R.id.home)
    void clickMenuHome() {
        finish();
    }

    @Click(R.id.activity_my_point_floating_action_button)
    void clickFab() {
        BuyPointsActivity_
                .intent(this)
                .extra(EXTRA_MY_POINTS, myPoints)
                .startForResult(REQUEST_CODE_BUY_POINTS);
    }

    @OnActivityResult(REQUEST_CODE_BUY_POINTS)
    void onResultBuyPoints(int resultCode, @OnActivityResult.Extra(EXTRA_MY_POINTS) int points) {
        switch (resultCode) {
            case AppCompatActivity.RESULT_OK:
                showMyPoints(points);
                break;
            default:
                break;
        }
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(MyPointsActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void insertAllPointsVO(List<PointsVO> pointsVOList) {
        pointsAdapter.clear();
        pointsAdapter.addAll(pointsVOList);
    }

    @Override
    public void showName(String name) {
        appCompatTextViewName.setText(name);
    }

    @Override
    public void showNameLink(String name) {
        appCompatTextViewNameLink.setText(getString(R.string.activity_my_points_text_view_name_link, name));
    }

    @Override
    public void showCategory(String category) {
        appCompatTextViewCategory.setText(category);
    }

    @Override
    public void showPhoto(String photoUrl) {
        final Drawable drawableUser = ContextCompat.getDrawable(getApplicationContext(), R.drawable.placeholder_user_medium);

        Picasso.with(getApplicationContext()).load(photoUrl)
                .resize(drawableUser.getIntrinsicWidth(), drawableUser.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_user_medium)
                .into(circleImageView);
    }

    @Override
    public void showMyPoints(int points) {
        this.myPoints = points;
        appCompatTextViewMyPoints.setText(getString(R.string.activity_my_points_text_view_points, points));
    }

    @Override
    public void showRecordsPoints(int recordsPoints) {
        appCompatTextViewRecordsPoints.setText(getString(R.string.activity_my_points_text_view_points, recordsPoints));
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showContentData() {
        viewContentData.setVisibility(VISIBLE);
    }
}

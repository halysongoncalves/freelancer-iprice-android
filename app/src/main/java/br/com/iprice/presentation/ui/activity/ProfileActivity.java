package br.com.iprice.presentation.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.iprice.R;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.presentation.presenter.ProfilePresenter;
import br.com.iprice.presentation.presenter.ProfilePresenterImpl;
import br.com.iprice.presentation.ui.adapter.PostAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static br.com.iprice.presentation.ui.fragment.FeedFragment.EXTRA_PRODUCT;


/**
 * Created by HalysonLima on 07/12/15.
 */
@EActivity(R.layout.activity_profile)
public class ProfileActivity extends AppCompatActivity implements ProfileView {
    @ViewById(R.id.view_toolbar_toolbar)
    Toolbar toolbar;
    @ViewById(R.id.activity_profile_image_view_photo)
    CircleImageView circleImageView;
    @ViewById(R.id.activity_profile_text_view_name)
    AppCompatTextView appCompatTextViewName;
    @ViewById(R.id.activity_profile_text_view_post)
    AppCompatTextView appCompatTextViewPost;
    @ViewById(R.id.activity_profile_text_view_category)
    AppCompatTextView appCompatTextViewCategory;
    @ViewById(R.id.activity_profile_image_view_category)
    AppCompatImageView appCompatImageViewCategory;
    @ViewById(R.id.activity_profile_info_text_view_empty)
    AppCompatTextView appCompatTextViewEmptySate;
    @ViewById(R.id.activity_profile_recycler_view_post)
    RecyclerView recyclerView;
    @ViewById(R.id.activity_profile_progress)
    ProgressBar progressBar;
    @ViewById(R.id.activity_profile_content_data)
    View viewContentData;
    @Bean(ProfilePresenterImpl.class)
    ProfilePresenter profilePresenter;
    @Bean
    PostAdapter postAdapter;
    private boolean deleteProduct = false;
    private MaterialDialog materialDialog;

    @AfterViews
    void afterViews() {
        profilePresenter.registerBus();
        profilePresenter.attachView(this);
        profilePresenter.recoverMyPost();
    }

    @Override
    protected void onDestroy() {
        profilePresenter.unregisterBus();
        super.onDestroy();
    }

    @Override
    public void setupToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.activity_my_points_title);
        }
    }

    @Override
    public void setupRecyclerView() {
        postAdapter.setListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(postAdapter);
    }

    @OptionsItem(android.R.id.home)
    void clickMenuHome() {
        setResult(deleteProduct ? RESULT_OK : RESULT_CANCELED);
        finish();
    }

    @Override
    public void showDialogTryAgain(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_generic))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogConnection() {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(getString(R.string.dialog_error_message_connection))
                .positiveText(R.string.dialog_button_ok)
                .cancelListener(this)
                .show();
    }

    @Override
    public void showDialogUnauthorized(String message) {
        materialDialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(message != null ? message : getString(R.string.dialog_error_message_unauthorized))
                .positiveText(R.string.dialog_button_ok)
                .cancelable(false)
                .cancelListener(this)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        LoginActivity_.intent(ProfileActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
                    }
                })
                .show();
    }

    @Override
    public void hideDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        hideDialog();
    }

    @Override
    public void showPhoto(String photoUrl) {
        final Drawable drawableUser = ContextCompat.getDrawable(getApplicationContext(), R.drawable.placeholder_user_medium);

        Picasso.with(getApplicationContext()).load(photoUrl)
                .resize(drawableUser.getIntrinsicWidth(), drawableUser.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_user_medium)
                .into(circleImageView);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showContentData() {
        viewContentData.setVisibility(VISIBLE);
    }

    @Override
    public void showName(String name) {
        appCompatTextViewName.setText(name);
    }

    @Override
    public void showNumberPost(int post) {
        appCompatTextViewPost.setText(getString(R.string.activity_profile_text_view_post_value, post));
    }

    @Override
    public void showFlag(@DrawableRes int flag) {
        appCompatImageViewCategory.setBackgroundResource(flag);
    }

    @Override
    public void showCategory(String category) {
        appCompatTextViewCategory.setText(category);
    }

    @Override
    public void insertAllProductsVO(List<ProductVO> productVOList) {
        postAdapter.clear();
        postAdapter.addAll(productVOList);
    }

    @Override
    public void showRecyclerView() {
        recyclerView.setVisibility(VISIBLE);
    }

    @Override
    public void showEmptyState() {
        appCompatTextViewEmptySate.setVisibility(VISIBLE);
    }

    @Override
    public void showDialogLoading() {
        materialDialog = new MaterialDialog.Builder(this)
                .content(getString(R.string.dialog_title_loading_data))
                .progress(true, 0)
                .show();
    }

    @Override
    public void hideRecyclerView() {
        recyclerView.setVisibility(GONE);
    }

    @Override
    public void onItemClick(View view, int position) {
        switch (view.getId()) {
            case R.id.view_item_post_content_root:
                ProductDetailsActivity_
                        .intent(this)
                        .extra(EXTRA_PRODUCT, postAdapter.get(position))
                        .start();
                break;

            case R.id.view_item_post_text_view_delete:
                deleteProduct = true;
                profilePresenter.deleteProduct(postAdapter.get(position));
                break;

            default:
                break;
        }

    }
}

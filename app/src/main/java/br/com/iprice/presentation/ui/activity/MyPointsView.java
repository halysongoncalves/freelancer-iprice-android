package br.com.iprice.presentation.ui.activity;

import java.util.List;

import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.presentation.ui.views.BaseActivityView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface MyPointsView extends BaseActivityView {
    void setupRecyclerView();

    void insertAllPointsVO(List<PointsVO> pointsVOList);

    void showName(String name);

    void showNameLink(String name);

    void showCategory(String category);

    void showPhoto(String photoUrl);

    void showMyPoints(int points);

    void showRecordsPoints(int recordsPoints);

    void hideProgress();

    void showContentData();
}

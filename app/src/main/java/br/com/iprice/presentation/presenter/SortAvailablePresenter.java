package br.com.iprice.presentation.presenter;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.model.event.AllAvailableSortEmptyEvent;
import br.com.iprice.model.event.AllAvailableSortEvent;
import br.com.iprice.model.event.InsufficientPointsEvent;
import br.com.iprice.model.event.RefreshAvailableSortEvent;
import br.com.iprice.model.event.http.BadRequestSortAvailableEvent;
import br.com.iprice.model.event.http.ConvertErrorSortAvaiableEvent;
import br.com.iprice.model.event.http.ForbiddenSortAvailableEvent;
import br.com.iprice.model.event.http.GenericErrorSortAvailableEvent;
import br.com.iprice.model.event.http.NetworkErrorSortAvailableEvent;
import br.com.iprice.model.event.http.NotFoundSortAvailableEvent;
import br.com.iprice.model.event.http.ServerErrorSortAvailableEvent;
import br.com.iprice.model.event.http.UnauthorizedSortAvailableEvent;
import br.com.iprice.presentation.ui.fragment.SortAvailableView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SortAvailablePresenter extends Bus {
    void attachView(SortAvailableView sortAvailableView);

    void recoverAllAvailableSort();

    void participateSort(SortVO sortVO);

    void onLoadInsufficientPointsEvent(InsufficientPointsEvent insufficientPointsEvent);

    void onLoadRefreshAvailableSortEvent(RefreshAvailableSortEvent refreshAvailableSortEvent);

    void onLoadAllAvailableSortEvent(AllAvailableSortEvent allAvailableSortEvent);

    void onLoadAllAvailableSortEmptyEvent(AllAvailableSortEmptyEvent allAvailableSortEmptyEvent);

    void onLoadNetworkErrorSortAvailableEvent(NetworkErrorSortAvailableEvent networkErrorSortAvailableEvent);

    void onLoadConvertErrorSortAvaiableEvent(ConvertErrorSortAvaiableEvent convertErrorSortAvaiableEvent);

    void onLoadGenericErrorSortAvailableEvent(GenericErrorSortAvailableEvent genericErrorSortAvailableEvent);

    void onLoadBadRequestSortAvailableEvent(BadRequestSortAvailableEvent badRequestSortAvailableEvent);

    void onLoadUnauthorizedSortAvailableEvent(UnauthorizedSortAvailableEvent unauthorizedSortAvailableEvent);

    void onLoadForbiddenSortAvailableEvent(ForbiddenSortAvailableEvent forbiddenSortAvailableEvent);

    void onLoadNotFoundSortAvailableEvent(NotFoundSortAvailableEvent notFoundSortAvailableEvent);

    void onLoadServerErrorSortAvailableEvent(ServerErrorSortAvailableEvent serverErrorSortAvailableEvent);

}

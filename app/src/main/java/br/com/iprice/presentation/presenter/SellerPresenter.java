package br.com.iprice.presentation.presenter;

import android.graphics.Bitmap;
import android.net.Uri;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.BuilderTakePictureEvent;
import br.com.iprice.model.event.PhotoChoiceFromGalleryEvent;
import br.com.iprice.presentation.ui.fragment.SellerView;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SellerPresenter extends Bus {
    void attachView(SellerView sellerView);

    void selectedImageFromGallery(Uri data);

    void builderTakePicture(Bitmap bitmap);

    void onLoadPhotoChoiceFromGalleryEvent(PhotoChoiceFromGalleryEvent photoChoiceFromGalleryEvent);

    void onLoadBuilderTakePictureEvent(BuilderTakePictureEvent builderTakePictureEvent);
}

package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class FeedVO implements Parcelable {
    public static final Creator<FeedVO> CREATOR = new Creator<FeedVO>() {
        @Override
        public FeedVO createFromParcel(Parcel in) {
            return new FeedVO(in);
        }

        @Override
        public FeedVO[] newArray(int size) {
            return new FeedVO[size];
        }
    };
    @JsonField(name = "usuarios")
    private List<UserVO> userVOList;
    @JsonField(name = "carousel")
    private List<CarouselVO> carouselVOList;

    public FeedVO() {
    }

    public FeedVO(List<UserVO> userVOList, List<CarouselVO> carouselVOList) {
        this.userVOList = userVOList;
        this.carouselVOList = carouselVOList;
    }

    protected FeedVO(Parcel in) {
        userVOList = in.createTypedArrayList(UserVO.CREATOR);
        carouselVOList = in.createTypedArrayList(CarouselVO.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(userVOList);
        dest.writeTypedList(carouselVOList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<UserVO> getUserVOList() {
        return userVOList;
    }

    public void setUserVOList(List<UserVO> userVOList) {
        this.userVOList = userVOList;
    }

    public List<CarouselVO> getCarouselVOList() {
        return carouselVOList;
    }

    public void setCarouselVOList(List<CarouselVO> carouselVOList) {
        this.carouselVOList = carouselVOList;
    }
}

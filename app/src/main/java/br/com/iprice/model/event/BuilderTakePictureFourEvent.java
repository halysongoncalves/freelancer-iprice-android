package br.com.iprice.model.event;

import android.graphics.Bitmap;

public class BuilderTakePictureFourEvent {
    private final Bitmap bitmap;

    public BuilderTakePictureFourEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}

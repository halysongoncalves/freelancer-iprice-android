package br.com.iprice.model.event;

import br.com.iprice.model.entities.PointsVO;

public class PointsUpdatedEvent {
    private final PointsVO pointsVO;

    public PointsUpdatedEvent(PointsVO pointsVO) {
        this.pointsVO = pointsVO;
    }

    public PointsVO getPointsVO() {
        return pointsVO;
    }
}

package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.ProductVO;

/**
 * Created by halysongoncalves on 31/08/16.
 */
public class RecoverAllWishListEvent {
    private final List<ProductVO> productVOList;

    public RecoverAllWishListEvent(List<ProductVO> productVOList) {
        this.productVOList = productVOList;
    }

    public List<ProductVO> getProductVOList() {
        return productVOList;
    }
}

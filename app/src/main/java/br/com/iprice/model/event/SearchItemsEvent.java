package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.UserVO;

public class SearchItemsEvent {
    private final List<UserVO> userVOList;
    private final String query;

    public SearchItemsEvent(List<UserVO> userVOList, String query) {
        this.userVOList = userVOList;
        this.query = query;
    }

    public List<UserVO> getUserVOList() {
        return userVOList;
    }

    public String getQuery() {
        return query;
    }
}

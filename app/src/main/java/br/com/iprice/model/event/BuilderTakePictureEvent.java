package br.com.iprice.model.event;

import android.graphics.Bitmap;

/**
 * Created by halysongoncalves on 07/09/16.
 */
public class BuilderTakePictureEvent {
    private final Bitmap bitmap;

    public BuilderTakePictureEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}

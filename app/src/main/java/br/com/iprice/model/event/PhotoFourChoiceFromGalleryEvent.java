package br.com.iprice.model.event;

import java.io.File;

public class PhotoFourChoiceFromGalleryEvent {
    private final File file;

    public PhotoFourChoiceFromGalleryEvent(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

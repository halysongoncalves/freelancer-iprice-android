package br.com.iprice.model.event;

import android.graphics.Bitmap;

public class BuilderTakePictureFiveEvent {
    private final Bitmap bitmap;

    public BuilderTakePictureFiveEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}

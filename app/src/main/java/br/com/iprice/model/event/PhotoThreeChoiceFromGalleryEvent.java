package br.com.iprice.model.event;

import java.io.File;

public class PhotoThreeChoiceFromGalleryEvent {
    private final File file;

    public PhotoThreeChoiceFromGalleryEvent(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

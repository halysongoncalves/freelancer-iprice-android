package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by halysongoncalves on 30/07/16.
 */
@JsonObject
public class RegisterUserErrorVO implements Parcelable {
    public static final Creator<RegisterUserErrorVO> CREATOR = new Creator<RegisterUserErrorVO>() {
        @Override
        public RegisterUserErrorVO createFromParcel(Parcel in) {
            return new RegisterUserErrorVO(in);
        }

        @Override
        public RegisterUserErrorVO[] newArray(int size) {
            return new RegisterUserErrorVO[size];
        }
    };
    @JsonField(name = "code")
    private  int code;
    @JsonField(name = "op")
    private  UserVO userVO;

    public RegisterUserErrorVO() {
    }

    public RegisterUserErrorVO(int code, UserVO userVO) {
        this.code = code;
        this.userVO = userVO;
    }

    protected RegisterUserErrorVO(Parcel in) {
        code = in.readInt();
        userVO = in.readParcelable(UserVO.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeParcelable(userVO, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public UserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }
}

package br.com.iprice.model.event.http;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by Halyson on 23/05/16.
 */
@JsonObject
public class UnauthorizedEvent {
    @JsonField(name = "error")
    private String message;

    public UnauthorizedEvent() {
    }

    public UnauthorizedEvent(String detail) {
        this.message = detail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
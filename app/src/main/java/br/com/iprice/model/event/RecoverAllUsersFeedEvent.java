package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.FeedVO;

public class RecoverAllUsersFeedEvent {
    private final List<FeedVO> feedVOList;

    public RecoverAllUsersFeedEvent(List<FeedVO> feedVOList) {
        this.feedVOList = feedVOList;
    }

    public List<FeedVO> getFeedVOList() {
        return feedVOList;
    }
}

package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class DeviceVO implements Parcelable {
    public static final Creator<DeviceVO> CREATOR = new Creator<DeviceVO>() {
        @Override
        public DeviceVO createFromParcel(Parcel in) {
            return new DeviceVO(in);
        }

        @Override
        public DeviceVO[] newArray(int size) {
            return new DeviceVO[size];
        }
    };
    @JsonField(name = "pushId")
    private String pushId;

    public DeviceVO() {
    }

    public DeviceVO(String pushId) {
        this.pushId = pushId;
    }

    protected DeviceVO(Parcel in) {
        pushId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pushId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }
}

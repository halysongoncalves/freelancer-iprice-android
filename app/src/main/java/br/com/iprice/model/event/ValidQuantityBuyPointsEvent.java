package br.com.iprice.model.event;

public class ValidQuantityBuyPointsEvent {
    private final int buyPoints;

    public ValidQuantityBuyPointsEvent(int buyPoints) {
        this.buyPoints = buyPoints;
    }

    public int getBuyPoints() {
        return buyPoints;
    }
}

package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

/**
 * Created by felipe on 03/03/16.
 */
@JsonObject
public class WishlistVO implements Parcelable {
    public static final Creator<WishlistVO> CREATOR = new Creator<WishlistVO>() {
        @Override
        public WishlistVO createFromParcel(Parcel in) {
            return new WishlistVO(in);
        }

        @Override
        public WishlistVO[] newArray(int size) {
            return new WishlistVO[size];
        }
    };
    @JsonField(name = "_id")
    private String wishlistid;
    @JsonField(name = "idProduto")
    private String productId;
    @JsonField(name = "idUsuario")
    private String userId;
    @JsonField(name = "telefone")
    private String phone;
    @JsonField(name = "descricao")
    private String description;
    @JsonField(name = "nome")
    private String name;
    @JsonField(name = "preco")
    private String price;
    @JsonField(name = "data")
    private String date;
    @JsonField(name = "comentarios")
    private List<CommentVO> commentVOs;
    private String photoUrl;

    public WishlistVO() {
    }

    public WishlistVO(String wishlistid, String productId, String userId, String phone, String description, String name, String price, String date, List<CommentVO> commentVOs) {
        this.wishlistid = wishlistid;
        this.productId = productId;
        this.userId = userId;
        this.phone = phone;
        this.description = description;
        this.name = name;
        this.price = price;
        this.date = date;
        this.commentVOs = commentVOs;
    }

    protected WishlistVO(Parcel in) {
        wishlistid = in.readString();
        productId = in.readString();
        userId = in.readString();
        phone = in.readString();
        description = in.readString();
        name = in.readString();
        price = in.readString();
        date = in.readString();
        commentVOs = in.createTypedArrayList(CommentVO.CREATOR);
        photoUrl = in.readString();
    }

    public String getWishlistid() {
        return wishlistid;
    }

    public void setWishlistid(String wishlistid) {
        this.wishlistid = wishlistid;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<CommentVO> getCommentVOs() {
        return commentVOs;
    }

    public void setCommentVOs(List<CommentVO> commentVOs) {
        this.commentVOs = commentVOs;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(wishlistid);
        parcel.writeString(productId);
        parcel.writeString(userId);
        parcel.writeString(phone);
        parcel.writeString(description);
        parcel.writeString(name);
        parcel.writeString(price);
        parcel.writeString(date);
        parcel.writeTypedList(commentVOs);
        parcel.writeString(photoUrl);
    }
}

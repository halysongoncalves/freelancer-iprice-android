package br.com.iprice.model.event;

import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 04/09/16.
 */
public class RecoverMyUserEvent {
    private final UserVO userVO;

    public RecoverMyUserEvent(UserVO userVO) {
        this.userVO = userVO;
    }

    public UserVO getUserVO() {
        return userVO;
    }
}

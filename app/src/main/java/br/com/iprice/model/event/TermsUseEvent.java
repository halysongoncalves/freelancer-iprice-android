package br.com.iprice.model.event;

/**
 * Created by halysongoncalves on 28/07/16.
 */
public class TermsUseEvent {
    private final String termsUser;

    public TermsUseEvent(String termsUser) {
        this.termsUser = termsUser;
    }

    public String getTermsUser() {
        return termsUser;
    }
}

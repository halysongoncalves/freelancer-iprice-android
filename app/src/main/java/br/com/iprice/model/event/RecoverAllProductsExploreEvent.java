package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.ProductVO;

public class RecoverAllProductsExploreEvent {
    private final List<ProductVO> productVOList;

    public RecoverAllProductsExploreEvent(List<ProductVO> productVOList) {
        this.productVOList = productVOList;
    }

    public List<ProductVO> getProductVOList() {
        return productVOList;
    }
}

package br.com.iprice.model.event;

import org.json.JSONObject;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public class RecoverUserFacebookEvent {
    private final JSONObject jsonObject;
    private final String token;

    public RecoverUserFacebookEvent(JSONObject jsonObject, String token) {
        this.jsonObject = jsonObject;
        this.token = token;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public String getToken() {
        return token;
    }
}

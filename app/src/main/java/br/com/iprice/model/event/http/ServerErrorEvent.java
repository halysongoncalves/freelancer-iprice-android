package br.com.iprice.model.event.http;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by Halyson on 23/05/16.
 */
@JsonObject
public class ServerErrorEvent {
    @JsonField(name = "error")
    private String message;

    public ServerErrorEvent() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
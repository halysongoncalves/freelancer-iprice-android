package br.com.iprice.model.event;

/**
 * Created by Halyson on 18/05/16.
 */
public class UserFieldsInvalidEvent {
    private boolean nameInvalid;
    private boolean emailInvalid;
    private boolean passwordInvalid;

    public UserFieldsInvalidEvent() {
    }

    public boolean isNameInvalid() {
        return nameInvalid;
    }

    public void setNameInvalid(boolean nameInvalid) {
        this.nameInvalid = nameInvalid;
    }

    public boolean isEmailInvalid() {
        return emailInvalid;
    }

    public void setEmailInvalid(boolean emailInvalid) {
        this.emailInvalid = emailInvalid;
    }

    public boolean isPasswordInvalid() {
        return passwordInvalid;
    }

    public void setPasswordInvalid(boolean passwordInvalid) {
        this.passwordInvalid = passwordInvalid;
    }

}

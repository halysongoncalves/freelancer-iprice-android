package br.com.iprice.model.event;

/**
 * Created by halysongoncalves on 31/08/16.
 */
public class AddProductWishListEvent {
    private final int position;

    public AddProductWishListEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}

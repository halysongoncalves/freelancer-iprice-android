package br.com.iprice.model.event;

import java.io.File;

public class PhotoOneChoiceFromGalleryEvent {
    private final File file;

    public PhotoOneChoiceFromGalleryEvent(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.UserVO;

public class RecoverAllUsersExploreEvent {
    private final List<UserVO> userVOList;

    public RecoverAllUsersExploreEvent(List<UserVO> userVOList) {
        this.userVOList = userVOList;
    }

    public List<UserVO> getUserVOList() {
        return userVOList;
    }
}

package br.com.iprice.model.event;

import java.io.File;

/**
 * Created by halysongoncalves on 21/01/16.
 */
public class PhotoChoiceFromGalleryEvent {
    private  final File file;

    public PhotoChoiceFromGalleryEvent(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

package br.com.iprice.model.event;

import android.graphics.Bitmap;

public class BuilderTakePictureTWoEvent {
    private final Bitmap bitmap;

    public BuilderTakePictureTWoEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}

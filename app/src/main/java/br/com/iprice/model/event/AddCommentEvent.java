package br.com.iprice.model.event;

import br.com.iprice.model.entities.CommentVO;

public class AddCommentEvent {
    private final CommentVO commentVO;

    public AddCommentEvent(CommentVO commentVO) {
        this.commentVO = commentVO;
    }

    public CommentVO getCommentVO() {
        return commentVO;
    }
}

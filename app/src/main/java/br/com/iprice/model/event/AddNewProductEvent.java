package br.com.iprice.model.event;

import br.com.iprice.model.entities.ProductVO;

public class AddNewProductEvent {
    private final ProductVO productVO;

    public AddNewProductEvent(ProductVO productVO) {
        this.productVO = productVO;
    }

    public ProductVO getProductVO() {
        return productVO;
    }
}

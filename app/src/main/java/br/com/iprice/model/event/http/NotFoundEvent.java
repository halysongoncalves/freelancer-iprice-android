package br.com.iprice.model.event.http;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by Halyson on 23/05/16.
 */
@JsonObject
public class NotFoundEvent {
    @JsonField(name = "error")
    private String message;

    public NotFoundEvent() {
    }

    public NotFoundEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
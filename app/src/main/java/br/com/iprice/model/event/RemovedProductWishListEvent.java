package br.com.iprice.model.event;

public class RemovedProductWishListEvent {
    private final String productId;

    public RemovedProductWishListEvent(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }
}

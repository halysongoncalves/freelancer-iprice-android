package br.com.iprice.model.event;

import br.com.iprice.model.entities.UserVO;

public class RecoverMyPostEvent {
    private final UserVO userVO;

    public RecoverMyPostEvent(UserVO userVO) {
        this.userVO = userVO;
    }

    public UserVO getUserVO() {
        return userVO;
    }
}

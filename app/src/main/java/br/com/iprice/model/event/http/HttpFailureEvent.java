package br.com.iprice.model.event.http;

import retrofit2.Response;

/**
 * Created by halysongoncalves on 27/07/16.
 */

public class HttpFailureEvent<T> {
    private final Response<T> response;

    public HttpFailureEvent(Response<T> response) {
        this.response = response;
    }

    public Response<T> getResponse() {
        return response;
    }
}

package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.UserVO;

public class RecoverAllUsersWishListEvent {
    private final List<UserVO> userVOList;

    public RecoverAllUsersWishListEvent(List<UserVO> userVOList) {
        this.userVOList = userVOList;
    }

    public List<UserVO> getUserVOList() {
        return userVOList;
    }
}

package br.com.iprice.model.event;

public class SellerProductInvalidEvent {
    private boolean pictureInvalid;
    private boolean nameInvalid;
    private boolean messageInvalid;
    private boolean priceInvalid;
    private boolean phoneInvalid;

    public boolean isNameInvalid() {
        return nameInvalid;
    }

    public void setNameInvalid(boolean nameInvalid) {
        this.nameInvalid = nameInvalid;
    }

    public boolean isMessageInvalid() {
        return messageInvalid;
    }

    public void setMessageInvalid(boolean messageInvalid) {
        this.messageInvalid = messageInvalid;
    }

    public boolean isPriceInvalid() {
        return priceInvalid;
    }

    public void setPriceInvalid(boolean priceInvalid) {
        this.priceInvalid = priceInvalid;
    }

    public boolean isPhoneInvalid() {
        return phoneInvalid;
    }

    public void setPhoneInvalid(boolean phoneInvalid) {
        this.phoneInvalid = phoneInvalid;
    }

    public boolean isPictureInvalid() {
        return pictureInvalid;
    }

    public void setPictureInvalid(boolean pictureInvalid) {
        this.pictureInvalid = pictureInvalid;
    }
}

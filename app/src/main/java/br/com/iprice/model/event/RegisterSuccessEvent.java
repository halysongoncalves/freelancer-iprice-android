package br.com.iprice.model.event;

import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 31/07/16.
 */
public class RegisterSuccessEvent {
    private final UserVO userVO;

    public RegisterSuccessEvent(UserVO userVO) {
        this.userVO = userVO;
    }

    public UserVO getUserVO() {
        return userVO;
    }
}

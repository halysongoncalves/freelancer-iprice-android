package br.com.iprice.model.event.http;

/**
 * Created by halysongoncalves on 27/07/16.
 */

public class InputOutputFailureEvent {
    private final Throwable throwable;

    public InputOutputFailureEvent(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}

package br.com.iprice.model.event;

import br.com.iprice.model.entities.ProductVO;

/**
 * Created by halysongoncalves on 25/08/16.
 */
public class LikedProductEvent {
    private final ProductVO productVO;
    private final int position;

    public LikedProductEvent(ProductVO productVO, int position) {
        this.productVO = productVO;
        this.position = position;
    }

    public ProductVO getProductVO() {
        return productVO;
    }

    public int getPosition() {
        return position;
    }
}

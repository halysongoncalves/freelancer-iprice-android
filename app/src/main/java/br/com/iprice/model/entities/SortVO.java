package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

/**
 * Created by halysongoncalves on 24/08/16.
 */

@JsonObject
public class SortVO implements Parcelable {
    public static final Creator<SortVO> CREATOR = new Creator<SortVO>() {
        @Override
        public SortVO createFromParcel(Parcel in) {
            return new SortVO(in);
        }

        @Override
        public SortVO[] newArray(int size) {
            return new SortVO[size];
        }
    };
    @JsonField(name = "_id")
    private String sortId;
    @JsonField(name = "idUsuario")
    private String userId;
    @JsonField(name = "nome")
    private String name;
    @JsonField(name = "descricao")
    private String description;
    @JsonField(name = "pontos")
    private int points;
    @JsonField(name = "idUsuarios")
    private List<String> userIds;

    public SortVO() {
    }

    public SortVO(String userId) {
        this.userId = userId;
    }

    public SortVO(String sortId, String userId, String name, String description, int points, List<String> userIds) {
        this.sortId = sortId;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.points = points;
        this.userIds = userIds;
    }

    protected SortVO(Parcel in) {
        sortId = in.readString();
        userId = in.readString();
        name = in.readString();
        description = in.readString();
        points = in.readInt();
        userIds = in.createStringArrayList();
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sortId);
        parcel.writeString(userId);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(points);
        parcel.writeStringList(userIds);
    }
}

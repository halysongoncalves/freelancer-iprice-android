package br.com.iprice.model.event;

import java.io.File;

public class PhotoFiveChoiceFromGalleryEvent {
    private final File file;

    public PhotoFiveChoiceFromGalleryEvent(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

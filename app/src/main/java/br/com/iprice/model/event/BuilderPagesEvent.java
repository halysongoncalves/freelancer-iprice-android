package br.com.iprice.model.event;

import java.util.List;

import cn.lightsky.infiniteindicator.page.Page;

public class BuilderPagesEvent {
    private final List<Page> pageList;

    public BuilderPagesEvent(List<Page> pageList) {
        this.pageList = pageList;
    }

    public List<Page> getPageList() {
        return pageList;
    }
}

package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

/**
 * Created by felipe on 03/03/16.
 */
@JsonObject
public class UserVO implements Parcelable {
    public static final String KEY = "UserVO";
    public static final Creator<UserVO> CREATOR = new Creator<UserVO>() {
        @Override
        public UserVO createFromParcel(Parcel in) {
            return new UserVO(in);
        }

        @Override
        public UserVO[] newArray(int size) {
            return new UserVO[size];
        }
    };
    @JsonField(name = "_id")
    private String userId;
    @JsonField(name = "nome")
    private String name;
    @JsonField(name = "email")
    private String email;
    @JsonField(name = "senha")
    private String password;
    @JsonField(name = "profileImage")
    private String picture;
    @JsonField(name = "pwd")
    private String pwd;
    @JsonField(name = "cidade")
    private String city;
    @JsonField(name = "pontos")
    private int points;
    @JsonField(name = "facebookToken")
    private String facebookToken;
    @JsonField(name = "localizacao")
    private double[] location;
    @JsonField(name = "produtos")
    private List<ProductVO> productVOs;
    @JsonField(name = "wishlist")
    private List<WishlistVO> wishlists;
    private String photoUrl;
    private String category;
    private int posts;
    @DrawableRes
    private int flag;

    public UserVO() {
    }

    public UserVO(List<ProductVO> productVOs) {
        this.productVOs = productVOs;
    }

    public UserVO(String name, String email, String password, String facebookToken, String picture, String city, int points, double[] location) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.city = city;
        this.picture = picture;
        this.facebookToken = facebookToken;
        this.location = location;
        this.points = points;
    }

    public UserVO(String name, String email, String password, String facebookToken, String picture, String city, double[] location) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.city = city;
        this.picture = picture;
        this.facebookToken = facebookToken;
        this.location = location;
    }

    public UserVO(String email, String pwd) {
        this.email = email;
        this.pwd = pwd;
    }

    public UserVO(String name, String email, String password, double[] location) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.location = location;
    }

    protected UserVO(Parcel in) {
        userId = in.readString();
        name = in.readString();
        email = in.readString();
        password = in.readString();
        picture = in.readString();
        pwd = in.readString();
        city = in.readString();
        points = in.readInt();
        facebookToken = in.readString();
        location = in.createDoubleArray();
        productVOs = in.createTypedArrayList(ProductVO.CREATOR);
        wishlists = in.createTypedArrayList(WishlistVO.CREATOR);
        photoUrl = in.readString();
        category = in.readString();
        posts = in.readInt();
        flag = in.readInt();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    public List<ProductVO> getProductVOs() {
        return productVOs;
    }

    public void setProductVOs(List<ProductVO> productVOs) {
        this.productVOs = productVOs;
    }

    public List<WishlistVO> getWishlists() {
        return wishlists;
    }

    public void setWishlists(List<WishlistVO> wishlists) {
        this.wishlists = wishlists;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPosts() {
        return posts;
    }

    public void setPosts(int posts) {
        this.posts = posts;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userId);
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeString(password);
        parcel.writeString(picture);
        parcel.writeString(pwd);
        parcel.writeString(city);
        parcel.writeInt(points);
        parcel.writeString(facebookToken);
        parcel.writeDoubleArray(location);
        parcel.writeTypedList(productVOs);
        parcel.writeTypedList(wishlists);
        parcel.writeString(photoUrl);
        parcel.writeString(category);
        parcel.writeInt(posts);
        parcel.writeInt(flag);
    }
}

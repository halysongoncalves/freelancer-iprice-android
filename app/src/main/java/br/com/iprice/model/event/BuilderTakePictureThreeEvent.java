package br.com.iprice.model.event;

import android.graphics.Bitmap;

public class BuilderTakePictureThreeEvent {
    private final Bitmap bitmap;

    public BuilderTakePictureThreeEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}

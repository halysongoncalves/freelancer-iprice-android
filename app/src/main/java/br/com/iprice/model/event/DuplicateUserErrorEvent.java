package br.com.iprice.model.event;

import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 30/07/16.
 */
public class DuplicateUserErrorEvent {
    private final UserVO userVO;

    public DuplicateUserErrorEvent(UserVO userVO) {
        this.userVO = userVO;
    }

    public UserVO getUserVO() {
        return userVO;
    }
}

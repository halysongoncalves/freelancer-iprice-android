package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.SortVO;

/**
 * Created by halysongoncalves on 24/08/16.
 */
public class AllParticipateSortEvent {
    private final List<SortVO> sortVOList;

    public AllParticipateSortEvent(List<SortVO> sortVOList) {
        this.sortVOList = sortVOList;
    }

    public List<SortVO> getSortVOList() {
        return sortVOList;
    }
}

package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by felipe on 03/03/16.
 */
@JsonObject
public class CommentVO implements Parcelable{
    public static final Creator<CommentVO> CREATOR = new Creator<CommentVO>() {
        @Override
        public CommentVO createFromParcel(Parcel in) {
            return new CommentVO(in);
        }

        @Override
        public CommentVO[] newArray(int size) {
            return new CommentVO[size];
        }
    };
    @JsonField(name = "_id")
    private String id;
    @JsonField(name = "idUsuario")
    private String userId;
    @JsonField(name = "nomeUsuario")
    private String userName;
    @JsonField(name = "comentario")
    private String commentDescription;
    @JsonField(name = "data")
    private String date;
    private String userPhotoUrl;

    public CommentVO() {
    }

    public CommentVO(String userId, String userName, String commentDescription) {
        this.userId = userId;
        this.userName = userName;
        this.commentDescription = commentDescription;
    }

    protected CommentVO(Parcel in) {
        id = in.readString();
        userId = in.readString();
        userName = in.readString();
        commentDescription = in.readString();
        date = in.readString();
        userPhotoUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(userId);
        dest.writeString(userName);
        dest.writeString(commentDescription);
        dest.writeString(date);
        dest.writeString(userPhotoUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommentDescription() {
        return commentDescription;
    }

    public void setCommentDescription(String commentDescription) {
        this.commentDescription = commentDescription;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }
}

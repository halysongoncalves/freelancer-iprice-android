package br.com.iprice.model.event;

public class RemovedProductEvent {
    private final String userId;
    private final String productId;

    public RemovedProductEvent(String userId, String productId) {
        this.userId = userId;
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public String getProductId() {
        return productId;
    }
}

package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

/**
 * Created by halysongoncalves on 31/12/15.
 */
public class SectionBeanVO implements Parcelable {
    public static final Creator<SectionBeanVO> CREATOR = new Creator<SectionBeanVO>() {
        @Override
        public SectionBeanVO createFromParcel(Parcel in) {
            return new SectionBeanVO(in);
        }

        @Override
        public SectionBeanVO[] newArray(int size) {
            return new SectionBeanVO[size];
        }
    };
    private String title;
    private int iconSelected;
    private int iconUnselected;
    private Fragment fragment;

    public SectionBeanVO(String title, Fragment fragment) {
        this.title = title;
        this.fragment = fragment;
    }

    public SectionBeanVO(String title, int iconSelected, int iconUnselected, Fragment fragment) {
        this.title = title;
        this.iconSelected = iconSelected;
        this.iconUnselected = iconUnselected;
        this.fragment = fragment;
    }

    protected SectionBeanVO(Parcel in) {
        title = in.readString();
        iconSelected = in.readInt();
        iconUnselected = in.readInt();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconSelected() {
        return iconSelected;
    }

    public void setIconSelected(int iconSelected) {
        this.iconSelected = iconSelected;
    }

    public int getIconUnselected() {
        return iconUnselected;
    }

    public void setIconUnselected(int iconUnselected) {
        this.iconUnselected = iconUnselected;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(iconSelected);
        dest.writeInt(iconUnselected);
    }
}

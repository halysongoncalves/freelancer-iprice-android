package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class CarouselVO implements Parcelable {
    public static final Creator<CarouselVO> CREATOR = new Creator<CarouselVO>() {
        @Override
        public CarouselVO createFromParcel(Parcel in) {
            return new CarouselVO(in);
        }

        @Override
        public CarouselVO[] newArray(int size) {
            return new CarouselVO[size];
        }
    };
    @JsonField(name = "imageUrl")
    private String photoUrl;

    public CarouselVO() {
    }

    protected CarouselVO(Parcel in) {
        photoUrl = in.readString();
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photoUrl);
    }
}

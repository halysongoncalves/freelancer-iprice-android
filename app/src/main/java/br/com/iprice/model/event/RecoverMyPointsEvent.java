package br.com.iprice.model.event;

import java.util.List;

import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 04/09/16.
 */
public class RecoverMyPointsEvent {
    private final UserVO userVO;
    private final List<PointsVO> pointsVOList;
    private final int recordsPoints;

    public RecoverMyPointsEvent(UserVO userVO, List<PointsVO> pointsVOList, int recordsPoints) {
        this.userVO = userVO;
        this.pointsVOList = pointsVOList;
        this.recordsPoints = recordsPoints;
    }

    public UserVO getUserVO() {
        return userVO;
    }

    public List<PointsVO> getPointsVOList() {
        return pointsVOList;
    }

    public int getRecordsPoints() {
        return recordsPoints;
    }
}

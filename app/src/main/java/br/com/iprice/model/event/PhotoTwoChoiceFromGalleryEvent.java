package br.com.iprice.model.event;

import java.io.File;

public class PhotoTwoChoiceFromGalleryEvent {
    private final File file;

    public PhotoTwoChoiceFromGalleryEvent(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

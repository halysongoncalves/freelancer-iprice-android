package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by halysongoncalves on 04/09/16.
 */

@JsonObject
public class PointsVO implements Parcelable {
    public static final String BEGINNER = "Iniciante";
    public static final String RESEARCHER = "Pesquisador";
    public static final String INTERMEDIATE = "Intermediário";
    public static final String TAG = "Taggeador";
    public static final String SUPER_POST = "Superpostador";
    public static final String EXPERT = "Expert";

    public static final int POINTS_50 = 50;
    public static final int POINTS_100 = 100;
    public static final int POINTS_200 = 200;
    public static final int POINTS_300 = 300;
    public static final int POINTS_500 = 500;
    public static final int POINTS_700 = 700;
    public static final Creator<PointsVO> CREATOR = new Creator<PointsVO>() {
        @Override
        public PointsVO createFromParcel(Parcel in) {
            return new PointsVO(in);
        }

        @Override
        public PointsVO[] newArray(int size) {
            return new PointsVO[size];
        }
    };
    private String name;
    private int flag;
    @JsonField(name = "pontos")
    private int point;


    public PointsVO() {
    }

    public PointsVO(int point) {
        this.point = point;
    }

    public PointsVO(@NAME String name, @DrawableRes int flag, @POINTS int point) {
        this.name = name;
        this.flag = flag;
        this.point = point;
    }

    protected PointsVO(Parcel in) {
        name = in.readString();
        flag = in.readInt();
        point = in.readInt();
    }

    @NAME
    public String getName() {
        return name;
    }

    public void setName(@NAME String name) {
        this.name = name;
    }

    @DrawableRes
    public int getFlag() {
        return flag;
    }

    public void setFlag(@DrawableRes int flag) {
        this.flag = flag;
    }

    @POINTS
    public int getPoint() {
        return point;
    }

    public void setPoint(@POINTS int point) {
        this.point = point;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(flag);
        parcel.writeInt(point);
    }

    @StringDef({BEGINNER, RESEARCHER, INTERMEDIATE, TAG, SUPER_POST, EXPERT})
    public @interface NAME {
    }

    @IntDef({POINTS_50, POINTS_100, POINTS_200, POINTS_300, POINTS_500, POINTS_700})
    public @interface POINTS {
    }
}

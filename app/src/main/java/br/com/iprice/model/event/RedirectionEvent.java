package br.com.iprice.model.event;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by Halyson on 23/05/16.
 */
@JsonObject
public class RedirectionEvent {
    @JsonField(name = "error_description")
    private String description;

    public RedirectionEvent() {
    }

    public RedirectionEvent(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
package br.com.iprice.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

/**
 * Created by felipe on 03/03/16.
 */
@JsonObject
public class ProductVO implements Parcelable {
    public static final String KEY = "ProductVO";
    public static final int PRODUCT_TYPE_iTEM = 0;
    public static final int PRODUCT_TYPE_CAROUSEL = 1;
    public static final Creator<ProductVO> CREATOR = new Creator<ProductVO>() {
        @Override
        public ProductVO createFromParcel(Parcel in) {
            return new ProductVO(in);
        }

        @Override
        public ProductVO[] newArray(int size) {
            return new ProductVO[size];
        }
    };
    @JsonField(name = "_id")
    private String id;
    @JsonField(name = "idProduto")
    private String productId;
    @JsonField(name = "idUsuario")
    private String userId;
    @JsonField(name = "nome")
    private String name;
    @JsonField(name = "descricao")
    private String description;
    @JsonField(name = "preco")
    private String price;
    @JsonField(name = "vendido")
    private boolean sold;
    @JsonField(name = "data")
    private String date;
    @JsonField(name = "likesCount")
    private int likes;
    @JsonField(name = "telefone")
    private String phone;
    @JsonField(name = "comentarios")
    private List<CommentVO> commentVOList;
    @JsonField(name = "fotos")
    private List<String> pictures;
    private double[] location;
    private String city;
    private String userName;
    private String photoUrl;
    private String userPhotoUrl;
    private boolean wishlist;
    private double distance;
    private int type;

    public ProductVO() {
    }

    public ProductVO(String userId, String productId, String description, String price, String name, String phone, List<CommentVO> commentVOList) {
        this.userId = userId;
        this.productId = productId;
        this.description = description;
        this.price = price;
        this.name = name;
        this.phone = phone;
        this.commentVOList = commentVOList;
    }

    public ProductVO(String name, String description, String price, List<String> pictures, List<CommentVO> commentVOList, String phone, String date, int likes, boolean sold) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.pictures = pictures;
        this.commentVOList = commentVOList;
        this.date = date;
        this.phone = phone;
        this.likes = likes;
        this.sold = sold;
    }

    public ProductVO(List<String> pictures, @ProductType int type) {
        this.pictures = pictures;
        this.type = type;
    }

    protected ProductVO(Parcel in) {
        id = in.readString();
        productId = in.readString();
        userId = in.readString();
        name = in.readString();
        description = in.readString();
        price = in.readString();
        sold = in.readByte() != 0;
        date = in.readString();
        likes = in.readInt();
        phone = in.readString();
        commentVOList = in.createTypedArrayList(CommentVO.CREATOR);
        pictures = in.createStringArrayList();
        location = in.createDoubleArray();
        city = in.readString();
        userName = in.readString();
        photoUrl = in.readString();
        userPhotoUrl = in.readString();
        wishlist = in.readByte() != 0;
        distance = in.readDouble();
        type = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(productId);
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeByte((byte) (sold ? 1 : 0));
        dest.writeString(date);
        dest.writeInt(likes);
        dest.writeString(phone);
        dest.writeTypedList(commentVOList);
        dest.writeStringList(pictures);
        dest.writeDoubleArray(location);
        dest.writeString(city);
        dest.writeString(userName);
        dest.writeString(photoUrl);
        dest.writeString(userPhotoUrl);
        dest.writeByte((byte) (wishlist ? 1 : 0));
        dest.writeDouble(distance);
        dest.writeInt(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<CommentVO> getCommentVOList() {
        return commentVOList;
    }

    public void setCommentVOList(List<CommentVO> commentVOList) {
        this.commentVOList = commentVOList;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    public boolean isWishlist() {
        return wishlist;
    }

    public void setWishlist(boolean wishlist) {
        this.wishlist = wishlist;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @IntDef({PRODUCT_TYPE_CAROUSEL, PRODUCT_TYPE_iTEM})
    public @interface ProductType {
    }
}

package br.com.iprice.model.event.http;

import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by Halyson on 23/05/16.
 */
@JsonObject
public class BadRequestSortAvailableEvent extends BadRequestEvent {
}
package br.com.iprice.model.event;

import android.graphics.Bitmap;

public class BuilderTakePictureOneEvent {
    private final Bitmap bitmap;

    public BuilderTakePictureOneEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}

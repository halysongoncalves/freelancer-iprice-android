package br.com.iprice;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.onesignal.OneSignal;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;
import com.orhanobut.hawk.LogLevel;
import com.splunk.mint.Mint;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EApplication;

import br.com.iprice.common.okhttp.OkHttpImpl_;

/**
 * Created by halysongoncalves on 20/06/16.
 */
@EApplication
public class Iprice extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        initializeMint(this);
        initializeFacebook(this);
        initializeHawk(this);
        initializePicasso(this);
        initializeOneSignal(this);
    }

    private void initializeFacebook(Context context) {
        FacebookSdk.sdkInitialize(context);
    }

    private void initializeMint(Context context) {
        Mint.initAndStartSession(context, BuildConfig.MINT);
    }

    private void initializeHawk(Context context) {
        Hawk.init(context)
                .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
                .setStorage(HawkBuilder.newSharedPrefStorage(this))
                .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
                .build();
    }


    private void initializePicasso(Context context) {
        Picasso.setSingletonInstance(new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(OkHttpImpl_.getInstance_(context).getInstance()))
                .build());
    }

    private void initializeOneSignal(Context context) {
        OneSignal
                .startInit(context)
                .autoPromptLocation(true)
                .init();
    }
}

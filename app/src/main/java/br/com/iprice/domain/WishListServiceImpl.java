package br.com.iprice.domain;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.entities.WishlistVO;
import br.com.iprice.model.event.AllWishListEmptyEvent;
import br.com.iprice.model.event.RecoverAllUsersWishListEvent;
import br.com.iprice.model.event.RecoverAllWishListEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.UserRepositoryHttp;
import br.com.iprice.repository.http.UserRepositoryHttpImpl;
import br.com.iprice.repository.http.WishListRepositoryHttp;
import br.com.iprice.repository.http.WishListRepositoryHttpImpl;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class WishListServiceImpl implements WishListService {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(WishListRepositoryHttpImpl.class)
    WishListRepositoryHttp wishListRepositoryHttp;

    @Bean(UserRepositoryHttpImpl.class)
    UserRepositoryHttp userRepositoryHttp;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void recoverAllWishList() {
        wishListRepositoryHttp.recoverAllWishList();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRecoverAllUsersWishListEvent(RecoverAllUsersWishListEvent recoverAllUsersWishListEvent) {
        final List<WishlistVO> wishlist = recoverAllWishList(recoverAllUsersWishListEvent);

        if (!wishlist.isEmpty()) {
            busProvider.getServiceBus().post(new RecoverAllWishListEvent(formatWishList(wishlist, recoverAllUsersWishListEvent.getUserVOList())));
            return;
        }

        busProvider.getServiceBus().post(new AllWishListEmptyEvent());
    }

    private List<WishlistVO> recoverAllWishList(RecoverAllUsersWishListEvent recoverAllUsersWishListEvent) {
        List<WishlistVO> wishlist = new ArrayList<>();
        UserVO userVO = userRepositoryDisk.loadUser();

        if ((recoverAllUsersWishListEvent.getUserVOList() != null) && (!recoverAllUsersWishListEvent.getUserVOList().isEmpty())) {
            List<UserVO> userVOList = recoverAllUsersWishListEvent.getUserVOList();
            for (int count = 0; count < userVOList.size(); count++) {
                if (userVOList.get(count).getUserId().equals(userVO.getUserId()) && userVO.getWishlists() != null) {
                    wishlist = userVOList.get(count).getWishlists();
                }
            }
        }
        return wishlist;
    }

    private List<ProductVO> formatWishList(List<WishlistVO> wishlist, List<UserVO> userVOList) {
        final List<ProductVO> productVOListFormatted = userService.formatListProduct(userVOList);
        List<ProductVO> productVOList = new ArrayList<>();

        for (WishlistVO wishlistVO : wishlist) {
            for (ProductVO productVO : productVOListFormatted) {
                if (wishlistVO.getProductId() != null && wishlistVO.getProductId().equals(productVO.getId())) {
                    productVOList.add(productVO);
                }
            }
        }

        return productVOList;
    }
}

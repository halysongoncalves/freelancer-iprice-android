package br.com.iprice.domain;

import android.graphics.Bitmap;

import java.io.File;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.AddCommentEvent;
import br.com.iprice.model.event.AddNewProductEvent;
import br.com.iprice.model.event.RecoverAllUsersExploreEvent;
import cn.lightsky.infiniteindicator.page.OnPageClickListener;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ProductService extends Bus {

    void builderPages(ProductVO productVO, OnPageClickListener onPageClickListener);

    void addComment(String productId, String comment);

    void validateFields(File imageOne, Bitmap bitmapOne, File fileImageTwo, Bitmap bitmapTwo, File fileImageThree, Bitmap bitmapThree, File fileImageFour, Bitmap bitmapFour, File fileImageFive, Bitmap bitmapImageFive, String name, String message, String price, String phone);

    void onLoadAddCommentEvent(AddCommentEvent addCommentEvent);

    void onLoadRecoverAllUsersExploreEvent(RecoverAllUsersExploreEvent recoverAllUsersExploreEvent);

    void onLoadAddNewProductEvent(AddNewProductEvent addNewProductEvent);
}

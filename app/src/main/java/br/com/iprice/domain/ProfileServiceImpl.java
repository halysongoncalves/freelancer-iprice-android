package br.com.iprice.domain;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.RecoverMyPostEvent;
import br.com.iprice.model.event.RemovedProductEvent;
import br.com.iprice.model.event.RemovedProductWishListEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.ProductRepositoryHttp;
import br.com.iprice.repository.http.ProductRepositoryHttpImpl;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class ProfileServiceImpl implements ProfileService {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(ProductRepositoryHttpImpl.class)
    ProductRepositoryHttp productRepositoryHttp;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void deleteProduct(ProductVO productVO) {
        productRepositoryHttp.deleteProduct(userRepositoryDisk.loadUser().getUserId(), productVO.getId());
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRemovedProductEvent(RemovedProductEvent removedProductEvent) {
        productRepositoryHttp.deleteProductWishList(removedProductEvent.getUserId(), removedProductEvent.getProductId());
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRemovedProductWishListEvent(RemovedProductWishListEvent removedProductWishListEvent) {
        UserVO userVO = userRepositoryDisk.loadUser();
        final List<ProductVO> productVOList = removeProduct(userVO, removedProductWishListEvent.getProductId());

        userVO.setPosts(productVOList.size());
        userVO.setProductVOs(productVOList);
        userRepositoryDisk.saveUser(userVO);

        busProvider.getServiceBus().post(new RecoverMyPostEvent(userVO));
    }

    private List<ProductVO> removeProduct(UserVO userVO, String productId) {
        List<ProductVO> productVOList = new ArrayList<>();
        if (userVO.getProductVOs() != null) {
            for (ProductVO productVO : userVO.getProductVOs()) {
                if (!productVO.getId().equals(productId)) {
                    productVOList.add(productVO);
                }
            }
        }

        return productVOList;
    }

}

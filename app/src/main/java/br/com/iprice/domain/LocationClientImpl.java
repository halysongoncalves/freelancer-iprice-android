package br.com.iprice.domain;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.splunk.mint.Mint;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

/**
 * Created by halysongoncalves on 24/06/16.
 */
@EBean(scope = EBean.Scope.Singleton)
public class LocationClientImpl implements LocationClient {
    private static final long REPEAT_TIME = 1000 * 10;
    private static final long FATTEST_REPEAT_TIME = 1000 * 5;
    @RootContext
    Context context;
    @Bean(LocationClientImpl.class)
    LocationClient locationClient;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private ConnectionCallback connectionCallback;
    private LocationSettingsRequest locationSettingsRequest;
    private Location currentLocation;
    private boolean automaticallyDisconnect = true;

    @AfterInject
    void afterInject() {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(REPEAT_TIME)
                .setFastestInterval(FATTEST_REPEAT_TIME);

        locationSettingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest).build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, locationSettingsRequest)
                .setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient = null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (currentLocation == null) {
            currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            if (currentLocation == null) {
                if (connectionCallback != null) {
                    connectionCallback.onConnectionFailed();
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (automaticallyDisconnect) {
            disconnect();
        }

        if (location != null) {
            this.currentLocation = location;
            final double latitude = location.getLatitude();
            final double longitude = location.getLongitude();

            if (connectionCallback != null) {
                connectionCallback.onConnected(latitude, longitude);
            }
        }
    }

    @Override
    public boolean googleApiIsConnected() {
        return googleApiClient.isConnected();
    }

    @Override
    public boolean googleApiIsConnecting() {
        return googleApiClient.isConnecting();
    }

    @Override
    public boolean googleApiIsAvailable() {
        return ConnectionResult.SUCCESS == GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
    }

    @Override
    public void automaticallyDisconnect(boolean status) {
        this.automaticallyDisconnect = status;
    }

    @Override
    public void connect() {
        if (googleApiClient != null && locationRequest != null && (!googleApiIsConnected() || !googleApiIsConnecting())) {
            googleApiClient.registerConnectionCallbacks(this);
            googleApiClient.registerConnectionFailedListener(this);
            googleApiClient.connect();
        }
    }

    @Override
    public void startUpdateLocation() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public float calculateDistance(double latitude, double longitude) {
        Location locationCurrent = new Location("");
        locationCurrent.setLatitude(currentLocation.getLatitude());
        locationCurrent.setLongitude(currentLocation.getLongitude());

        Location locationDest = new Location("");
        locationDest.setLatitude(latitude);
        locationDest.setLongitude(longitude);

        return BigDecimal.valueOf((locationCurrent.distanceTo(locationDest) / 1000)).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
    }

    @Override
    public double getLatitude() {
        return currentLocation != null ? currentLocation.getLatitude() : 0;
    }

    @Override
    public double getLongitude() {
        return currentLocation != null ? currentLocation.getLongitude() : 0;
    }

    @Override
    public void disconnect() {
        if ((googleApiClient != null) && (googleApiIsConnected())) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.unregisterConnectionCallbacks(this);
            googleApiClient.unregisterConnectionFailedListener(this);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void setConnectionCallback(ConnectionCallback connectionCallback) {
        this.connectionCallback = connectionCallback;
    }

    @Override
    public LocationSettingsRequest getLocationSettingsRequest() {
        return locationSettingsRequest;
    }

    @Override
    public Location getLastLocation() {
        return currentLocation;
    }

    @Override
    public Address getAddress() {
        try {
            List<Address> addressList = new Geocoder(context, new Locale("pt", "BR")).getFromLocation(getLatitude(), getLongitude(), 1);
            if (addressList != null && !addressList.isEmpty()) {
                if (addressList.size() > 0) {
                    return addressList.get(0);
                }
            }
        } catch (Exception exception) {
            Mint.logException(exception);
        }

        return null;
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        if (connectionCallback != null) {
            connectionCallback.onResult(locationSettingsResult.getStatus());
        }
    }
}

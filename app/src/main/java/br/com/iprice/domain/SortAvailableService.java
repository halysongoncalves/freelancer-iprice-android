package br.com.iprice.domain;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.model.event.AddParticipateSortEvent;
import br.com.iprice.model.event.AllAvailableSortEvent;
import br.com.iprice.model.event.RefreshAvailableSortEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SortAvailableService extends Bus {
    void recoverAllAvailableSort();

    void participateSort(SortVO sortVO);

    void onLoadAddParticipateSortEvent(AddParticipateSortEvent addParticipateSortEvent);

    void onLoadRefreshAvailableSortEvent(RefreshAvailableSortEvent refreshAvailableSortEvent);

    void onLoadAllAvailableSortEvent(AllAvailableSortEvent allAvailableSortEvent);

    void onLoadHttpFailureEvent(HttpFailureEvent httpFailureEvent);

    void onLoadInputOutputFailureEvent(InputOutputFailureEvent inputOutputFailureEvent);
}

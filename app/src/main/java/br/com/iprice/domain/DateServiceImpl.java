package br.com.iprice.domain;

import org.androidannotations.annotations.EBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class DateServiceImpl implements DateService {
    private static final String DD_MM_YYYY = "dd/MM/yyyy HH:mm";

    @Override
    public String recoverCurrentDate() {
        return new SimpleDateFormat(DD_MM_YYYY, Locale.getDefault()).format(new Date());
    }

}

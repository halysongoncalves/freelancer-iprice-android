package br.com.iprice.domain;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.AllParticipateSortEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SortParticipateService extends Bus {

    void recoverAllSortParticipate();

    void onLoadAllParticipateSortEvent(AllParticipateSortEvent allParticipateSortEvent);

    void onLoadHttpFailureEvent(HttpFailureEvent httpFailureEvent);

    void onLoadInputOutputFailureEvent(InputOutputFailureEvent inputOutputFailureEvent);
}

package br.com.iprice.domain;

import android.util.Log;

import com.bluelinelabs.logansquare.LoganSquare;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.AddParticipateSortEvent;
import br.com.iprice.model.event.AllAvailableSortEmptyEvent;
import br.com.iprice.model.event.AllAvailableSortEvent;
import br.com.iprice.model.event.InsufficientPointsEvent;
import br.com.iprice.model.event.RefreshAvailableSortEvent;
import br.com.iprice.model.event.http.BadRequestSortAvailableEvent;
import br.com.iprice.model.event.http.ConvertErrorSortAvaiableEvent;
import br.com.iprice.model.event.http.ForbiddenSortAvailableEvent;
import br.com.iprice.model.event.http.GenericErrorSortAvailableEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.model.event.http.NetworkErrorSortAvailableEvent;
import br.com.iprice.model.event.http.NotFoundSortAvailableEvent;
import br.com.iprice.model.event.http.ServerErrorSortAvailableEvent;
import br.com.iprice.model.event.http.ServerErrorSortParticipateEvent;
import br.com.iprice.model.event.http.UnauthorizedSortAvailableEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.SortRepositoryHttp;
import br.com.iprice.repository.http.SortRepositoryHttpImpl;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class SortAvailableServiceImpl implements SortAvailableService {
    private static final String TAG = SortAvailableServiceImpl.class.getSimpleName();
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(SortRepositoryHttpImpl.class)
    SortRepositoryHttp sortRepositoryHttp;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void recoverAllAvailableSort() {
        sortRepositoryHttp.recoverAllAvailableSort();
    }

    @Override
    @Background
    public void participateSort(SortVO sortVO) {
        UserVO userVO = userRepositoryDisk.loadUser();
        if (userVO.getPoints() < sortVO.getPoints()) {
            busProvider.getServiceBus().post(new InsufficientPointsEvent());
            return;
        }

        sortRepositoryHttp.participateSort(sortVO.getSortId(), new SortVO(userVO.getUserId()));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadAddParticipateSortEvent(AddParticipateSortEvent addParticipateSortEvent) {
        sortRepositoryHttp.refreshAvailableSort();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRefreshAvailableSortEvent(RefreshAvailableSortEvent refreshAvailableSortEvent) {
        UserVO userVO = userRepositoryDisk.loadUser();

        if (refreshAvailableSortEvent.getSortVOList() != null && !refreshAvailableSortEvent.getSortVOList().isEmpty()) {
            List<SortVO> sortVOListFiltered = builderListSortFiltered(refreshAvailableSortEvent.getSortVOList(), userVO);

            busProvider.getServiceBus().post(!sortVOListFiltered.isEmpty() ?
                    new RefreshAvailableSortEvent(sortVOListFiltered)
                    : new AllAvailableSortEmptyEvent());
            return;
        }

        busProvider.getServiceBus().post(new AllAvailableSortEmptyEvent());
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadAllAvailableSortEvent(AllAvailableSortEvent allAvailableSortEvent) {
        UserVO userVO = userRepositoryDisk.loadUser();

        if (allAvailableSortEvent.getSortVOList() != null && !allAvailableSortEvent.getSortVOList().isEmpty()) {
            List<SortVO> sortVOListFiltered = builderListSortFiltered(allAvailableSortEvent.getSortVOList(), userVO);
            busProvider.getServiceBus().post(new AllAvailableSortEvent(sortVOListFiltered));
            return;
        }

        busProvider.getServiceBus().post(new AllAvailableSortEmptyEvent());
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadHttpFailureEvent(HttpFailureEvent httpFailureEvent) {
        Response response = httpFailureEvent.getResponse();
        int statusCode = response.code();

        try {
            String error = response.errorBody().string();

            if (statusCode >= RequestErrorService.Kind.HTTP_500 && statusCode <= RequestErrorService.Kind.HTTP_599) {
                busProvider.getServiceBus().post(LoganSquare.parse(error, ServerErrorSortAvailableEvent.class));
                return;
            }

            switch (statusCode) {
                case RequestErrorService.Kind.HTTP_400:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, BadRequestSortAvailableEvent.class));

                    break;

                case RequestErrorService.Kind.HTTP_401:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, UnauthorizedSortAvailableEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_403:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, ForbiddenSortAvailableEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_404:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, NotFoundSortAvailableEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_408:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortAvailableEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_409:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortAvailableEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_412:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortAvailableEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_500:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, ServerErrorSortParticipateEvent.class));
                    break;

                default:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortAvailableEvent.class));
                    break;
            }
        } catch (IOException iOException) {
            Log.e(TAG, iOException.getMessage());
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadInputOutputFailureEvent(InputOutputFailureEvent inputOutputFailureEvent) {
        if (inputOutputFailureEvent.getThrowable() != null) {

            Throwable throwable = inputOutputFailureEvent.getThrowable();
            if (throwable instanceof IOException) {
                busProvider.getServiceBus().post(new NetworkErrorSortAvailableEvent());
                return;
            }

            busProvider.getServiceBus().post(new ConvertErrorSortAvaiableEvent());
        }
    }


    private List<SortVO> builderListSortFiltered(List<SortVO> sortVOList, UserVO userVO) {
        List<SortVO> sortVOListFiltered = new ArrayList<>();
        for (SortVO sortVO : sortVOList) {
            if (sortVO.getUserIds().isEmpty()) {
                sortVOListFiltered.add(sortVO);
                continue;
            }

            if (!userAlreadyNotSort(userVO.getUserId(), sortVO.getUserIds())) {
                sortVOListFiltered.add(sortVO);
            }
        }

        return sortVOListFiltered;
    }

    private boolean userAlreadyNotSort(String userId, List<String> userIdList) {
        boolean userAlreadySort = false;
        for (String userIdSorted : userIdList) {
            if (userId.equals(userIdSorted)) {
                userAlreadySort = true;
            }
        }

        return userAlreadySort;
    }
}

package br.com.iprice.domain;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface DateService {
    String recoverCurrentDate();
}

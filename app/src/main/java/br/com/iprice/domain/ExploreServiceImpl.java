package br.com.iprice.domain;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.RecoverAllProductsEvent;
import br.com.iprice.model.event.RecoverAllProductsExploreEmptyEvent;
import br.com.iprice.model.event.RecoverAllProductsExploreEvent;
import br.com.iprice.model.event.RecoverAllUsersExploreEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.ProductRepositoryHttp;
import br.com.iprice.repository.http.ProductRepositoryHttpImpl;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class ExploreServiceImpl implements ExploreService {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(ProductRepositoryHttpImpl.class)
    ProductRepositoryHttp productRepositoryHttp;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void exploreAllProduct(double latitude, double longitude, ArrayList<ProductVO> productVOArrayList) {
        if (productVOArrayList == null) {
            productRepositoryHttp.exploreAllProduct(userRepositoryDisk.loadUser().getUserId(), latitude, longitude);
            return;
        }
        busProvider.getServiceBus().post(new RecoverAllProductsEvent(productVOArrayList));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRecoverAllUsersExploreEvent(RecoverAllUsersExploreEvent recoverAllUsersExploreEvent) {
        final List<ProductVO> productVOList = userService.formatListProduct(recoverUsersWithoutMyUser(recoverAllUsersExploreEvent.getUserVOList()));
        if (!productVOList.isEmpty()) {
            busProvider.getServiceBus().post(new RecoverAllProductsExploreEvent(productVOList));
            return;
        }
        busProvider.getServiceBus().post(new RecoverAllProductsExploreEmptyEvent());
    }

    private List<UserVO> recoverUsersWithoutMyUser(List<UserVO> userVOList) {
        final List<UserVO> userVOListFormatted = new ArrayList<>();
        final UserVO myUserVO = userRepositoryDisk.loadUser();

        for (UserVO userVO : userVOList) {
            if (userVO.getUserId() != null && !userVO.getUserId().equals(myUserVO.getUserId())) {
                userVOListFormatted.add(userVO);
            }
        }

        return userVOListFormatted;

    }

}

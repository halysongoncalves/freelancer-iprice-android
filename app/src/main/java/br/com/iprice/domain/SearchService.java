package br.com.iprice.domain;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.SearchItemsEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface SearchService extends Bus {

    void searchItems(String query);

    void onLoadSearchItemsEvent(SearchItemsEvent searchItemsEvent);

}

package br.com.iprice.domain;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.CarouselVO;
import br.com.iprice.model.entities.FeedVO;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.AddProductWishListEvent;
import br.com.iprice.model.event.LikedProductEvent;
import br.com.iprice.model.event.RecoverAllProductsEmptyEvent;
import br.com.iprice.model.event.RecoverAllProductsEvent;
import br.com.iprice.model.event.RecoverAllUsersFeedEvent;
import br.com.iprice.model.event.RemoveProductWishListEvent;
import br.com.iprice.model.event.UserAlreadyLikeEvent;
import br.com.iprice.repository.disk.ProductRepositoryDisk;
import br.com.iprice.repository.disk.ProductRepositoryDiskImpl;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.ProductRepositoryHttp;
import br.com.iprice.repository.http.ProductRepositoryHttpImpl;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class FeedServiceImpl implements FeedService {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(ProductRepositoryHttpImpl.class)
    ProductRepositoryHttp productRepositoryHttp;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(ProductRepositoryDiskImpl.class)
    ProductRepositoryDisk productRepositoryDisk;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }


    @Override
    @Background
    public void recoverAllFeed(ArrayList<ProductVO> productVOArrayList) {
        if (productVOArrayList == null) {
            productRepositoryHttp.recoverAllFeed();
            return;
        }
        busProvider.getServiceBus().post(new RecoverAllProductsEvent(productVOArrayList));
    }

    @Override
    @Background
    public void like(ProductVO productVO, int position) {
        List<String> likeIdsList = productRepositoryDisk.loadProductsId();
        if (likeIdsList != null && !likeIdsList.isEmpty() && userAlreadyLikeProduct(likeIdsList, productVO)) {
            busProvider.getServiceBus().post(new UserAlreadyLikeEvent());
            return;
        }

        productRepositoryHttp.like(productVO, position);
    }

    @Override
    @Background
    public void wishList(ProductVO productVO, int position) {
        if (!productVO.isWishlist()) {
            productRepositoryHttp.addWishList(userRepositoryDisk.loadUser().getUserId(), new ProductVO(userRepositoryDisk.loadUser().getUserId(), productVO.getId(),
                    productVO.getDescription(), productVO.getPrice(), productVO.getName(), productVO.getPhone(), productVO.getCommentVOList()), position);
            return;
        }
        productRepositoryHttp.removeWishList(userRepositoryDisk.loadUser().getUserId(), productVO.getId(), position);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadLikedProductEvent(LikedProductEvent likedProductEvent) {
        List<String> likeIdsList = productRepositoryDisk.loadProductsId();
        if (likeIdsList != null && !likeIdsList.isEmpty()) {
            likeIdsList.add(likedProductEvent.getProductVO().getId());
        } else {
            likeIdsList = new ArrayList<>();
            likeIdsList.add(likedProductEvent.getProductVO().getId());
        }

        likedProductEvent.getProductVO().setLikes(likedProductEvent.getProductVO().getLikes() + 1);
        productRepositoryDisk.saveProductsId(likeIdsList);
        busProvider.getServiceBus().post(likedProductEvent);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRecoverAllUsersFeedEvent(RecoverAllUsersFeedEvent recoverAllUsersFeedEvent) {
        final List<ProductVO> productVOListCarousel = convertCarouselToProduct(recoverAllUsersFeedEvent.getFeedVOList());
        final List<UserVO> userVOList = recoverListUser(recoverAllUsersFeedEvent.getFeedVOList());
        final List<ProductVO> productVOList = userService.formatListProduct(userVOList);

        Collections.sort(productVOList, new Comparator<ProductVO>() {
            @Override
            public int compare(ProductVO productOLeft, ProductVO productVORight) {
                return Double.compare(productOLeft.getDistance(), productVORight.getDistance());
            }
        });

        final List<ProductVO> productVOListWithCarousel = addCarousel(productVOListCarousel, productVOList);
        if (!productVOListWithCarousel.isEmpty()) {
            busProvider.getServiceBus().post(new RecoverAllProductsEvent(productVOListWithCarousel));
            return;
        }

        busProvider.getServiceBus().post(new RecoverAllProductsEmptyEvent());
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadAddProductWishListEvent(AddProductWishListEvent addProductWishListEvent) {
        busProvider.getServiceBus().post(addProductWishListEvent);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRemoveProductWishListEvent(RemoveProductWishListEvent removeProductWishListEvent) {
        busProvider.getServiceBus().post(removeProductWishListEvent);
    }

    private boolean userAlreadyLikeProduct(List<String> likeIdsList, ProductVO productVO) {
        boolean userAlreadyLike = false;

        for (String productId : likeIdsList) {
            if (productId.equals(productVO.getId())) {
                userAlreadyLike = true;
            }
        }

        return userAlreadyLike;
    }

    private List<ProductVO> convertCarouselToProduct(List<FeedVO> feedVOList) {
        final List<ProductVO> productVOList = new ArrayList<>();

        for (FeedVO feedVO : feedVOList) {
            if (feedVO.getCarouselVOList() != null) {
                final List<String> photoUrl = new ArrayList<>();
                for (CarouselVO carouselVO : feedVO.getCarouselVOList()) {
                    photoUrl.add(carouselVO.getPhotoUrl());
                }

                productVOList.add(new ProductVO(photoUrl, ProductVO.PRODUCT_TYPE_CAROUSEL));
            }

        }

        return productVOList;
    }

    private List<UserVO> recoverListUser(List<FeedVO> feedVOList) {
        final List<UserVO> userVOList = new ArrayList<>();

        for (FeedVO feedVO : feedVOList) {
            if (feedVO.getUserVOList() != null) {
                userVOList.addAll(feedVO.getUserVOList());
            }

        }

        return userVOList;
    }

    private List<ProductVO> addCarousel(List<ProductVO> feedVOListCarousel, List<ProductVO> productVOListAll) {
        final List<ProductVO> productVOList = new ArrayList<>();

        int countItem = 0;

        for (int count = 0; count < productVOListAll.size(); count++) {
            if (count == 0) {
                productVOList.add(feedVOListCarousel.get(countItem));
                productVOList.add(productVOListAll.get(count));
            } else if (count % 10 == 0) {
                productVOList.add(feedVOListCarousel.get(countItem));
                countItem++;

                if (count + 1 == productVOListAll.size()) {
                    productVOList.add(productVOListAll.get(count));
                }
            } else {
                productVOList.add(productVOListAll.get(count));
            }
        }


        return productVOList;
    }
}

package br.com.iprice.domain;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;

import com.facebook.AccessToken;
import com.onesignal.OneSignal;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.iprice.BuildConfig;
import br.com.iprice.R;
import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.model.entities.DeviceVO;
import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.entities.WishlistVO;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.FailRecoverPostEvent;
import br.com.iprice.model.event.RecoverMyPostEvent;
import br.com.iprice.model.event.RecoverUserFacebookEvent;
import br.com.iprice.model.event.RegisterSuccessEvent;
import br.com.iprice.model.event.SessionIsInvalidEvent;
import br.com.iprice.model.event.TermsUseEvent;
import br.com.iprice.model.event.UnexpectedErrorFacebookEvent;
import br.com.iprice.model.event.UserFieldsInvalidEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.UserRepositoryHttp;
import br.com.iprice.repository.http.UserRepositoryHttpImpl;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class UserServiceImpl implements UserService {
    static final String SEPARATOR = "/";
    static final String PROFILE = "profile.png";
    static final String PRODUCT_ONE = "image0.png";
    private static final String PRODUCT_TWO = "image1.png";
    private static final String PRODUCT_THREE = "image2.png";
    private static final String PRODUCT_FOUR = "image3.png";
    private static final String PRODUCT_FIVE = "image4.png";
    private static final String TAG = UserServiceImpl.class.getSimpleName();
    private static final String URL_PICTURE = "url";
    private static final String DATA = "data";
    private static final String PICTURE = "picture";
    private static final String FIELDS_ID = "id";
    private static final String FIELDS_NAME = "name";
    private static final String FIELDS_EMAIL = "email";
    private static final String FIELDS_PICTURE = "picture.type(large)";
    private static final String FIELDS = FIELDS_ID + "," + FIELDS_NAME + "," + FIELDS_EMAIL + "," + FIELDS_PICTURE;
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(UserRepositoryHttpImpl.class)
    UserRepositoryHttp userRepositoryHttp;

    @Bean(LocationClientImpl.class)
    LocationClient locationClient;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void sessionIsValid() {
        UserVO userVO = userRepositoryDisk.loadUser();
        if (userVO != null) {
            userRepositoryHttp.login(new UserVO(userVO.getEmail(), userVO.getPassword()));
            return;
        }

        busProvider.getServiceBus().post(new SessionIsInvalidEvent());
    }

    @Override
    @Background
    public void validateFields(@NonNull String email, @NonNull String password) {
        final UserFieldsInvalidEvent userFieldsInvalidEvent = new UserFieldsInvalidEvent();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            userFieldsInvalidEvent.setEmailInvalid(true);
        }

        if (password.isEmpty() || password.length() <= 3) {
            userFieldsInvalidEvent.setPasswordInvalid(true);
        }

        if (userFieldsInvalidEvent.isEmailInvalid() || userFieldsInvalidEvent.isPasswordInvalid()) {
            busProvider.getServiceBus().post(userFieldsInvalidEvent);
            return;
        }

        userRepositoryHttp.login(new UserVO(email, password));
    }

    @Override
    @Background
    public void validateFields(@NonNull String name, @NonNull String email, @NonNull String password, File file) {
        final UserFieldsInvalidEvent userFieldsInvalidEvent = new UserFieldsInvalidEvent();

        if (name.isEmpty() || name.length() <= 3) {
            userFieldsInvalidEvent.setNameInvalid(true);
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            userFieldsInvalidEvent.setEmailInvalid(true);
        }

        if (password.isEmpty() || password.length() <= 3) {
            userFieldsInvalidEvent.setPasswordInvalid(true);
        }

        if (userFieldsInvalidEvent.isEmailInvalid() || userFieldsInvalidEvent.isPasswordInvalid()) {
            busProvider.getServiceBus().post(userFieldsInvalidEvent);
            return;
        }

        final Address address = locationClient.getAddress();

        userRepositoryHttp.register(new UserVO(name, email, password, "", file != null ? encodePicture(BitmapFactory.decodeFile(file.getAbsolutePath())) : "",
                address != null ? address.getLocality() : "", builderLocation(locationClient.getLongitude(), locationClient.getLatitude())));
    }

    @Override
    @Background
    public void recoverTermsUse(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);

            busProvider.getServiceBus().post(new TermsUseEvent(new String(bytes)));
        } catch (IOException iOException) {
            Log.e(iOException.getMessage(), UserServiceImpl.class.getSimpleName());
        }
    }

    @Override
    @Background
    public void recoverUserFacebook(AccessToken accessToken) {
        Bundle bundleParameters = new Bundle();
        bundleParameters.putString("fields", FIELDS);

        userRepositoryHttp.recoverUserFacebook(accessToken, bundleParameters);
    }

    @Override
    @Background
    public void logout() {
        userRepositoryDisk.clear();
    }

    @Override
    public List<ProductVO> formatListProduct(List<UserVO> userVOList) {
        final List<WishlistVO> wishlist = recoverMyWishList(userVOList);
        final List<ProductVO> productVOList = new ArrayList<>();

        if (userVOList != null && !userVOList.isEmpty()) {
            for (UserVO userVO : userVOList) {
                if (userVO.getUserId() != null) {
                    userVO.setPhotoUrl(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + PROFILE);
                }

                for (ProductVO productVO : userVO.getProductVOs()) {
                    if (productVO.getType() == ProductVO.PRODUCT_TYPE_iTEM) {
                        if (productVO.getCommentVOList() != null && !productVO.getCommentVOList().isEmpty()) {
                            for (CommentVO commentVO : productVO.getCommentVOList()) {
                                commentVO.setUserPhotoUrl(BuildConfig.HOST + commentVO.getUserId() + SEPARATOR + PROFILE);
                            }
                        }

                        for (WishlistVO wishlistVO : wishlist) {
                            if (productVO.getId() != null && wishlistVO.getProductId() != null && productVO.getId().equals(wishlistVO.getProductId())) {
                                productVO.setWishlist(true);
                            }
                        }


                        if (userVO.getLocation() != null) {
                            productVO.setLocation(userVO.getLocation());
                            final double location[] = transformMyLocation(userVO.getLocation(), userVO, userRepositoryDisk.loadUser());

                            productVO.setLocation(location);
                            productVO.setDistance(locationClient.calculateDistance(location[0], location[1]));
                        }

                        productVO.setCity(userVO.getCity());
                        productVO.setPhotoUrl(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_ONE);
                        productVO.setUserPhotoUrl(userVO.getPhotoUrl());
                        productVO.setUserName(userVO.getName());
                        productVO.setPictures(builderListPhotoProduct(userVO, productVO));
                        productVOList.add(productVO);
                    } else {
                        productVOList.add(productVO);
                    }
                }
            }
        }


        return productVOList;
    }

    @Override
    public UserVO recoverMyUser() {
        return userRepositoryDisk.loadUser();
    }


    @Override
    public void formatListProduct(UserVO userVO, List<ProductVO> productVOList) {
        for (ProductVO productVO : productVOList) {
            productVO.setPictures(builderListPhotoProduct(userVO, productVO));
            productVO.setLocation(userVO.getLocation());
            productVO.setCity(userVO.getCity());
            productVO.setPhotoUrl(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_ONE);
            productVO.setUserPhotoUrl(userVO.getPhotoUrl());
            productVO.setUserName(userVO.getName());
        }
    }

    @Override
    public String encodePicture(Bitmap bitmap) {
        String pictureBase64 = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);

            byte[] bytes = byteArrayOutputStream.toByteArray();
            pictureBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (Exception exception) {
            Log.e(TAG, "" + exception.getMessage());
        } catch (OutOfMemoryError outOfMemoryError) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);

            byte[] bytes = byteArrayOutputStream.toByteArray();
            pictureBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);

            Log.e(TAG, "" + outOfMemoryError.getMessage());
        }

        return pictureBase64;
    }

    @Override
    @Background
    public void recoverMyPost() {
        final UserVO userVO = userRepositoryDisk.loadUser();
        userRepositoryHttp.recoverMyPost(new UserVO(userVO.getEmail(), userVO.getPassword()));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRecoverUserFacebookEvent(RecoverUserFacebookEvent recoverUserFacebookEvent) {
        final JSONObject jsonObject = recoverUserFacebookEvent.getJsonObject();

        final String userId = jsonObject.optString(FIELDS_ID);
        final Bitmap pictureUser = getPicture((((jsonObject.optJSONObject(PICTURE)).optJSONObject(DATA)).optString(URL_PICTURE)));
        final String pictureUserEncoded = pictureUser != null ? encodePicture(pictureUser) : "";

        userRepositoryHttp.loginFacebook(new UserVO(jsonObject.optString(FIELDS_NAME), userId + "_" + jsonObject.optString(FIELDS_EMAIL),
                userId, recoverUserFacebookEvent.getToken(), pictureUserEncoded,
                locationClient.getAddress() != null ? locationClient.getAddress().getLocality() : "",
                0, builderLocation(locationClient.getLongitude(), locationClient.getLatitude())));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadLoginSuccessEvent(DuplicateUserErrorEvent duplicateUserErrorEvent) {
        formatUser(duplicateUserErrorEvent.getUserVO());
        userRepositoryDisk.saveUser(duplicateUserErrorEvent.getUserVO());

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null) {
                    userRepositoryHttp.registerDeviceUser(userRepositoryDisk.loadUser().getUserId(), new DeviceVO(userId));
                }
            }
        });

        busProvider.getServiceBus().post(duplicateUserErrorEvent);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRegisterSuccessEvent(RegisterSuccessEvent registerSuccessEvent) {
        userRepositoryDisk.saveUser(registerSuccessEvent.getUserVO());
        busProvider.getServiceBus().post(registerSuccessEvent);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadUnexpectedErrorFacebookEvent(UnexpectedErrorFacebookEvent unexpectedErrorFacebookEvent) {
        busProvider.getServiceBus().post(unexpectedErrorFacebookEvent);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRecoverMyPostEvent(RecoverMyPostEvent recoverMyPostEvent) {
        UserVO userVO = recoverMyPostEvent.getUserVO();
        formatUser(userVO);
        userRepositoryDisk.saveUser(userVO);

        busProvider.getServiceBus().post(recoverMyPostEvent);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadFailRecoverPostEvent(FailRecoverPostEvent failRecoverPostEvent) {
        busProvider.getServiceBus().post(new RecoverMyPostEvent(userRepositoryDisk.loadUser()));
    }

    private void formatUser(UserVO userVO) {
        if (userVO != null) {
            userVO.setPhotoUrl(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + PROFILE);

            final int points = userVO.getPoints();

            if (points >= 0 && points < PointsVO.POINTS_100) {
                userVO.setCategory(PointsVO.BEGINNER);
                userVO.setFlag(R.drawable.ic_beginner);
            } else if (points >= PointsVO.POINTS_100 && points < PointsVO.POINTS_200) {
                userVO.setCategory(PointsVO.RESEARCHER);
                userVO.setFlag(R.drawable.vector_search);
            } else if (points >= PointsVO.POINTS_200 && points < PointsVO.POINTS_300) {
                userVO.setCategory(PointsVO.INTERMEDIATE);
                userVO.setFlag(R.drawable.ic_intermediate);
            } else if (points >= PointsVO.POINTS_300 && points < PointsVO.POINTS_500) {
                userVO.setCategory(PointsVO.TAG);
                userVO.setFlag(R.drawable.ic_tag);
            } else if (points >= PointsVO.POINTS_500 && points < PointsVO.POINTS_700) {
                userVO.setCategory(PointsVO.SUPER_POST);
                userVO.setFlag(R.drawable.ic_trophy);
            } else {
                userVO.setCategory(PointsVO.EXPERT);
                userVO.setFlag(R.drawable.ic_trophy);
            }

            if (userVO.getProductVOs() != null) {
                formatListProduct(userVO, userVO.getProductVOs());
            }

            userVO.setLocation(builderLocation(locationClient.getLongitude(), locationClient.getLatitude()));
            userVO.setPosts(userVO.getProductVOs() != null ? userVO.getProductVOs().size() : 0);
        }
    }

    private double[] builderLocation(double latitude, double longitude) {
        double[] locationArray = new double[2];

        locationArray[0] = latitude;
        locationArray[1] = longitude;

        return locationArray;
    }

    private Bitmap getPicture(String url) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
        } catch (IOException iOException) {
            Log.e(TAG, iOException.getMessage());
        }

        return bitmap;
    }

    private List<String> builderListPhotoProduct(UserVO userVO, ProductVO productVO) {
        List<String> stringList = new ArrayList<>();
        stringList.add(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_ONE);
        stringList.add(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_TWO);
        stringList.add(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_THREE);
        stringList.add(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_FOUR);
        stringList.add(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_FIVE);

        return stringList;
    }

    private List<WishlistVO> recoverMyWishList(List<UserVO> userVOList) {
        String userId = userRepositoryDisk.loadUser().getUserId();
        List<WishlistVO> wishlist = new ArrayList<>();

        for (UserVO userVO : userVOList) {
            if (userVO.getUserId() != null && userVO.getUserId().equals(userId) && userVO.getWishlists() != null && !userVO.getWishlists().isEmpty()) {
                wishlist = userVO.getWishlists();
            }
        }

        return wishlist;
    }

    private double[] transformMyLocation(double[] location, UserVO userVO, UserVO myUserVO) {
        if (userVO.getUserId() != null && userVO.getUserId().equals(myUserVO.getUserId())) {
            return builderLocation(location[1], location[0]);
        }

        return builderLocation(location[0], location[1]);
    }
}

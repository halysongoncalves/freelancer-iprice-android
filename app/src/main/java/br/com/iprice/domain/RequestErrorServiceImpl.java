package br.com.iprice.domain;

import android.util.Log;

import com.bluelinelabs.logansquare.LoganSquare;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.RegisterUserErrorVO;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.RedirectionEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class RequestErrorServiceImpl implements RequestErrorService {
    private static final String TAG = RequestErrorServiceImpl.class.getSimpleName();
    private static final int BACKEND_ERROR_CODE_DUPLICATE = 11000;

    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadHttpFailureEvent(HttpFailureEvent httpFailureEvent) {
        Response response = httpFailureEvent.getResponse();
        int statusCode = response.code();

        try {
            String error = response.errorBody().string();

            if (statusCode >= Kind.HTTP_300 && statusCode <= Kind.HTTP_308) {
                busProvider.getServiceBus().post(LoganSquare.parse(error, RedirectionEvent.class));
                return;
            }

            if (statusCode >= Kind.HTTP_500 && statusCode <= Kind.HTTP_599) {
                RegisterUserErrorVO registerUserErrorVO = LoganSquare.parse(error, RegisterUserErrorVO.class);
                if (registerUserErrorVO != null && registerUserErrorVO.getCode() == BACKEND_ERROR_CODE_DUPLICATE) {
                    busProvider.getRepositoryBus().post(new DuplicateUserErrorEvent(registerUserErrorVO.getUserVO()));
                    return;
                }

                busProvider.getServiceBus().post(LoganSquare.parse(error, ServerErrorEvent.class));
                return;
            }

            switch (statusCode) {
                case Kind.HTTP_400:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, BadRequestEvent.class));

                    break;

                case Kind.HTTP_401:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, UnauthorizedEvent.class));
                    break;

                case Kind.HTTP_403:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, ForbiddenEvent.class));
                    break;

                case Kind.HTTP_404:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, NotFoundEvent.class));
                    break;

                case Kind.HTTP_408:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorEvent.class));
                    break;

                case Kind.HTTP_409:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorEvent.class));
                    break;

                case Kind.HTTP_412:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorEvent.class));
                    break;

                case Kind.HTTP_500:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, ServerErrorEvent.class));
                    break;

                default:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorEvent.class));
                    break;
            }
        } catch (IOException iOException) {
            Log.e(TAG, iOException.getMessage());
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadInputOutputFailureEvent(InputOutputFailureEvent inputOutputFailureEvent) {
        if (inputOutputFailureEvent.getThrowable() != null) {

            Throwable throwable = inputOutputFailureEvent.getThrowable();
            if (throwable instanceof IOException) {
                busProvider.getServiceBus().post(new NetworkErrorEvent());
                return;
            }

            busProvider.getServiceBus().post(new ConvertErrorEvent());
        }
    }
}

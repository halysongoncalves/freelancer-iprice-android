package br.com.iprice.domain;

import java.util.ArrayList;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.AddProductWishListEvent;
import br.com.iprice.model.event.LikedProductEvent;
import br.com.iprice.model.event.RecoverAllUsersFeedEvent;
import br.com.iprice.model.event.RemoveProductWishListEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface FeedService extends Bus {
    void recoverAllFeed(ArrayList<ProductVO> productVOArrayList);

    void like(ProductVO productVO, int position);

    void wishList(ProductVO productVO, int position);

    void onLoadRecoverAllUsersFeedEvent(RecoverAllUsersFeedEvent recoverAllUsersFeedEvent);

    void onLoadLikedProductEvent(LikedProductEvent likedProductEvent);

    void onLoadAddProductWishListEvent(AddProductWishListEvent addProductWishListEvent);

    void onLoadRemoveProductWishListEvent(RemoveProductWishListEvent removeProductWishListEvent);
}

package br.com.iprice.domain;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.PointsUpdatedEvent;
import br.com.iprice.repository.http.RecoverRecordsPointsEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface MyPointsService extends Bus {
    void recoverMyPoints();

    void validateBuyPoints(String points);

    void addNewPoints(String points);

    void addNewPoints();

    void onLoadRecoverRecordsPointsEvent(RecoverRecordsPointsEvent recoverRecordsPointsEvent);

    void onLoadPointsUpdatedEvent(PointsUpdatedEvent pointsUpdatedEvent);
}

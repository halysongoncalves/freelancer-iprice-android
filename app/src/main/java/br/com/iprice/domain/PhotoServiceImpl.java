package br.com.iprice.domain;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.ByteArrayOutputStream;
import java.io.File;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.event.BuilderTakePictureEvent;
import br.com.iprice.model.event.BuilderTakePictureFiveEvent;
import br.com.iprice.model.event.BuilderTakePictureFourEvent;
import br.com.iprice.model.event.BuilderTakePictureOneEvent;
import br.com.iprice.model.event.BuilderTakePictureTWoEvent;
import br.com.iprice.model.event.BuilderTakePictureThreeEvent;
import br.com.iprice.model.event.PhotoChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoFiveChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoFourChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoOneChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoThreeChoiceFromGalleryEvent;
import br.com.iprice.model.event.PhotoTwoChoiceFromGalleryEvent;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class PhotoServiceImpl implements PhotoService {
    private static final int COMPRESS = 80;
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @RootContext
    Context context;

    @Override
    @Background
    public void selectedImageFromGallery(Uri data) {
        final File file = recoverFile(data);
        busProvider.getServiceBus().post(new PhotoChoiceFromGalleryEvent(file));
    }

    @Override
    @Background
    public void builderTakePicture(Bitmap bitmap) {
        bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS, new ByteArrayOutputStream());
        busProvider.getServiceBus().post(new BuilderTakePictureEvent(bitmap));
    }

    @Override
    @Background
    public void builderTakePictureOne(Bitmap bitmap) {
        bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS, new ByteArrayOutputStream());
        busProvider.getServiceBus().post(new BuilderTakePictureOneEvent(bitmap));
    }

    @Override
    @Background
    public void selectedImageOneFromGallery(Uri data) {
        final File file = recoverFile(data);
        busProvider.getServiceBus().post(new PhotoOneChoiceFromGalleryEvent(file));
    }

    @Override
    @Background
    public void builderTakePictureTwo(Bitmap bitmap) {
        bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS, new ByteArrayOutputStream());
        busProvider.getServiceBus().post(new BuilderTakePictureTWoEvent(bitmap));
    }

    @Override
    @Background
    public void selectedImageTwoFromGallery(Uri data) {
        final File file = recoverFile(data);
        busProvider.getServiceBus().post(new PhotoTwoChoiceFromGalleryEvent(file));
    }

    @Override
    @Background
    public void builderTakePictureThree(Bitmap bitmap) {
        bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS, new ByteArrayOutputStream());
        busProvider.getServiceBus().post(new BuilderTakePictureThreeEvent(bitmap));
    }

    @Override
    @Background
    public void selectedImageThreeFromGallery(Uri data) {
        final File file = recoverFile(data);
        busProvider.getServiceBus().post(new PhotoThreeChoiceFromGalleryEvent(file));
    }

    @Override
    @Background
    public void builderTakePictureFour(Bitmap bitmap) {
        bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS, new ByteArrayOutputStream());
        busProvider.getServiceBus().post(new BuilderTakePictureFourEvent(bitmap));
    }

    @Override
    @Background
    public void selectedImageFourFromGallery(Uri data) {
        final File file = recoverFile(data);
        busProvider.getServiceBus().post(new PhotoFourChoiceFromGalleryEvent(file));
    }

    @Override
    @Background
    public void builderTakePictureFive(Bitmap bitmap) {
        bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS, new ByteArrayOutputStream());
        busProvider.getServiceBus().post(new BuilderTakePictureFiveEvent(bitmap));
    }

    @Override
    @Background
    public void selectedImageFiveFromGallery(Uri data) {
        final File file = recoverFile(data);
        busProvider.getServiceBus().post(new PhotoFiveChoiceFromGalleryEvent(file));
    }

    private File recoverFile(Uri data) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        String filePath = "";
        Cursor cursor = context.getContentResolver().query(data, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filePath = cursor.getString(columnIndex);
            cursor.close();

        }


        return new File(filePath);
    }
}

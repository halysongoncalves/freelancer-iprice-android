package br.com.iprice.domain;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.facebook.AccessToken;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.FailRecoverPostEvent;
import br.com.iprice.model.event.RecoverMyPostEvent;
import br.com.iprice.model.event.RecoverUserFacebookEvent;
import br.com.iprice.model.event.RegisterSuccessEvent;
import br.com.iprice.model.event.UnexpectedErrorFacebookEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface UserService extends Bus {
    void sessionIsValid();

    void validateFields(@NonNull String email, @NonNull String password);

    void validateFields(@NonNull String name, @NonNull String email, @NonNull String password, File file);

    void recoverTermsUse(InputStream inputStream);

    void recoverUserFacebook(AccessToken accessToken);

    void logout();

    List<ProductVO> formatListProduct(List<UserVO> userVOList);

    UserVO recoverMyUser();

    void recoverMyPost();

    void onLoadRecoverUserFacebookEvent(RecoverUserFacebookEvent recoverUserFacebookEvent);

    void onLoadLoginSuccessEvent(DuplicateUserErrorEvent duplicateUserErrorEvent);

    void onLoadRegisterSuccessEvent(RegisterSuccessEvent registerSuccessEvent);

    void onLoadUnexpectedErrorFacebookEvent(UnexpectedErrorFacebookEvent unexpectedErrorFacebookEvent);

    void onLoadRecoverMyPostEvent(RecoverMyPostEvent recoverMyPostEvent);

    void onLoadFailRecoverPostEvent(FailRecoverPostEvent failRecoverPostEvent);

    String encodePicture(Bitmap bitmap);

    void formatListProduct(UserVO userVO, List<ProductVO> productVOList);
}

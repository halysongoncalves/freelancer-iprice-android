package br.com.iprice.domain;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface PhotoService {
    void selectedImageFromGallery(Uri data);

    void builderTakePicture(Bitmap bitmap);

    void builderTakePictureOne(Bitmap bitmap);

    void selectedImageOneFromGallery(Uri data);

    void builderTakePictureTwo(Bitmap bitmap);

    void selectedImageTwoFromGallery(Uri data);

    void builderTakePictureThree(Bitmap bitmap);

    void selectedImageThreeFromGallery(Uri data);

    void builderTakePictureFour(Bitmap bitmap);

    void selectedImageFourFromGallery(Uri data);

    void builderTakePictureFive(Bitmap bitmap);

    void selectedImageFiveFromGallery(Uri data);
}
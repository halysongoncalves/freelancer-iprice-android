package br.com.iprice.domain;

import java.util.ArrayList;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.RecoverAllUsersExploreEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ExploreService extends Bus {

    void exploreAllProduct(double latitude, double longitude, ArrayList<ProductVO> productVOArrayList);

    void onLoadRecoverAllUsersExploreEvent(RecoverAllUsersExploreEvent recoverAllUsersExploreEvent);
}

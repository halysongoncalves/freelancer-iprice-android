package br.com.iprice.domain;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface RequestErrorService extends Bus {
    void onLoadHttpFailureEvent(HttpFailureEvent httpFailureEvent);

    void onLoadInputOutputFailureEvent(InputOutputFailureEvent inputOutputFailureEvent);

    interface Kind {
        int HTTP_300 = 300;

        int HTTP_308 = 308;

        int HTTP_400 = 400;

        int HTTP_401 = 401;

        int HTTP_403 = 403;

        int HTTP_404 = 404;

        int HTTP_408 = 408;

        int HTTP_409 = 409;

        int HTTP_412 = 412;

        int HTTP_500 = 500;

        int HTTP_599 = 599;
    }
}

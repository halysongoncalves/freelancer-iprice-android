package br.com.iprice.domain;

import android.location.Address;
import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface LocationClient extends GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult>, LocationListener {
    void automaticallyDisconnect(boolean status);

    void connect();

    void disconnect();

    void setConnectionCallback(ConnectionCallback connectionCallback);

    void startUpdateLocation();

    float calculateDistance(double latitude, double longitude);

    double getLatitude();

    double getLongitude();

    boolean googleApiIsAvailable();

    boolean googleApiIsConnected();

    boolean googleApiIsConnecting();

    LocationSettingsRequest getLocationSettingsRequest();

    Location getLastLocation();

    Address getAddress();

    interface ConnectionCallback {
        void onConnected(double latitude, double longitude);

        void onConnectionFailed();

        void onResult(Status status);
    }
}

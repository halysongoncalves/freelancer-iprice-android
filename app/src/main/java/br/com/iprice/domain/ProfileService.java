package br.com.iprice.domain;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.RemovedProductEvent;
import br.com.iprice.model.event.RemovedProductWishListEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface ProfileService extends Bus {

    void deleteProduct(ProductVO productVO);

    void onLoadRemovedProductEvent(RemovedProductEvent removedProductEvent);

    void onLoadRemovedProductWishListEvent(RemovedProductWishListEvent removedProductWishListEvent);

}

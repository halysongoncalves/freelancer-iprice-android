package br.com.iprice.domain;

import br.com.iprice.common.bus.Bus;
import br.com.iprice.model.event.RecoverAllUsersWishListEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

public interface WishListService extends Bus {

    void recoverAllWishList();

    void onLoadRecoverAllUsersWishListEvent(RecoverAllUsersWishListEvent recoverAllUsersWishListEvent);
}

package br.com.iprice.domain;

import android.util.Log;

import com.bluelinelabs.logansquare.LoganSquare;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.AllParticipateSortEmptyEvent;
import br.com.iprice.model.event.AllParticipateSortEvent;
import br.com.iprice.model.event.http.BadRequestSortParticipateEvent;
import br.com.iprice.model.event.http.ConvertErrorSortAvaiableEvent;
import br.com.iprice.model.event.http.ForbiddenSortParticipateEvent;
import br.com.iprice.model.event.http.GenericErrorSortParticipateEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.model.event.http.NetworkErrorSortParticipateEvent;
import br.com.iprice.model.event.http.NotFoundSortParticipateEvent;
import br.com.iprice.model.event.http.ServerErrorSortParticipateEvent;
import br.com.iprice.model.event.http.UnauthorizedSortParticipateEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.SortRepositoryHttp;
import br.com.iprice.repository.http.SortRepositoryHttpImpl;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class SortParticipateServiceImpl implements SortParticipateService {
    private static final String TAG = SortParticipateServiceImpl.class.getSimpleName();
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(SortRepositoryHttpImpl.class)
    SortRepositoryHttp sortRepositoryHttp;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void recoverAllSortParticipate() {
        sortRepositoryHttp.recoverAllSortParticipate();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadAllParticipateSortEvent(AllParticipateSortEvent allParticipateSortEvent) {
        UserVO userVO = userRepositoryDisk.loadUser();

        List<SortVO> sortVOList = new ArrayList<>();
        if (allParticipateSortEvent.getSortVOList() != null && !allParticipateSortEvent.getSortVOList().isEmpty()) {
            for (SortVO sortVO : allParticipateSortEvent.getSortVOList()) {
                if (userAlreadySort(userVO.getUserId(), sortVO.getUserIds())) {
                    sortVOList.add(sortVO);
                }
            }

            busProvider.getServiceBus().post(!sortVOList.isEmpty() ? new AllParticipateSortEvent(sortVOList) : new AllParticipateSortEmptyEvent());
            return;
        }

        busProvider.getServiceBus().post(new AllParticipateSortEmptyEvent());
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadHttpFailureEvent(HttpFailureEvent httpFailureEvent) {
        Response response = httpFailureEvent.getResponse();
        int statusCode = response.code();

        try {
            String error = response.errorBody().string();

            if (statusCode >= RequestErrorService.Kind.HTTP_500 && statusCode <= RequestErrorService.Kind.HTTP_599) {
                busProvider.getServiceBus().post(LoganSquare.parse(error, ServerErrorSortParticipateEvent.class));
                return;
            }

            switch (statusCode) {
                case RequestErrorService.Kind.HTTP_400:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, BadRequestSortParticipateEvent.class));

                    break;

                case RequestErrorService.Kind.HTTP_401:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, UnauthorizedSortParticipateEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_403:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, ForbiddenSortParticipateEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_404:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, NotFoundSortParticipateEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_408:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortParticipateEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_409:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortParticipateEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_412:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortParticipateEvent.class));
                    break;

                case RequestErrorService.Kind.HTTP_500:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, ServerErrorSortParticipateEvent.class));
                    break;

                default:
                    busProvider.getServiceBus().post(LoganSquare.parse(error, GenericErrorSortParticipateEvent.class));
                    break;
            }
        } catch (IOException iOException) {
            Log.e(TAG, iOException.getMessage());
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadInputOutputFailureEvent(InputOutputFailureEvent inputOutputFailureEvent) {
        if (inputOutputFailureEvent.getThrowable() != null) {

            Throwable throwable = inputOutputFailureEvent.getThrowable();
            if (throwable instanceof IOException) {
                busProvider.getServiceBus().post(new NetworkErrorSortParticipateEvent());
                return;
            }

            busProvider.getServiceBus().post(new ConvertErrorSortAvaiableEvent());
        }
    }


    private boolean userAlreadySort(String userId, List<String> userIdList) {
        boolean userAlreadySort = false;
        for (String userIdSorted : userIdList) {
            if (userId.equals(userIdSorted)) {
                userAlreadySort = true;
            }
        }

        return userAlreadySort;
    }
}

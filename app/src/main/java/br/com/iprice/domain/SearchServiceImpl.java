package br.com.iprice.domain;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.event.SearchItemsEmptyEvent;
import br.com.iprice.model.event.SearchItemsEvent;
import br.com.iprice.model.event.SearchItemsFilteredEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.ProductRepositoryHttp;
import br.com.iprice.repository.http.ProductRepositoryHttpImpl;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class SearchServiceImpl implements SearchService {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(ProductRepositoryHttpImpl.class)
    ProductRepositoryHttp productRepositoryHttp;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(UserServiceImpl.class)
    UserService userService;


    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void searchItems(String query) {
        productRepositoryHttp.searchItems(query);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadSearchItemsEvent(SearchItemsEvent searchItemsEvent) {
        if (searchItemsEvent.getUserVOList() != null && !searchItemsEvent.getUserVOList().isEmpty()) {
            final List<ProductVO> productVOList = filterListByName(userService.formatListProduct(searchItemsEvent.getUserVOList()), searchItemsEvent.getQuery());

            if (!productVOList.isEmpty()) {
                busProvider.getServiceBus().post(new SearchItemsFilteredEvent(productVOList));
                return;
            }

            busProvider.getServiceBus().post(new SearchItemsEmptyEvent());
            return;
        }

        busProvider.getServiceBus().post(new SearchItemsEmptyEvent());
    }


    private List<ProductVO> filterListByName(List<ProductVO> productVOList, String query) {
        List<ProductVO> productVOListFiltered = new ArrayList<>();

        for (ProductVO productVO : productVOList) {
            if (productVO.getName() != null && productVO.getName().toLowerCase().contains(query.toLowerCase())) {
                productVOListFiltered.add(productVO);
            }
        }

        return productVOListFiltered;
    }
}

package br.com.iprice.domain;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.iprice.BuildConfig;
import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.AddCommentEvent;
import br.com.iprice.model.event.AddNewProductEvent;
import br.com.iprice.model.event.BuilderPagesEvent;
import br.com.iprice.model.event.RecoverAllProductsEmptyEvent;
import br.com.iprice.model.event.RecoverAllProductsEvent;
import br.com.iprice.model.event.RecoverAllUsersExploreEvent;
import br.com.iprice.model.event.SellerProductInvalidEvent;
import br.com.iprice.repository.disk.ProductRepositoryDisk;
import br.com.iprice.repository.disk.ProductRepositoryDiskImpl;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.ProductRepositoryHttp;
import br.com.iprice.repository.http.ProductRepositoryHttpImpl;
import cn.lightsky.infiniteindicator.page.OnPageClickListener;
import cn.lightsky.infiniteindicator.page.Page;

import static br.com.iprice.domain.UserServiceImpl.PRODUCT_ONE;
import static br.com.iprice.domain.UserServiceImpl.PROFILE;
import static br.com.iprice.domain.UserServiceImpl.SEPARATOR;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class ProductServiceImpl implements ProductService {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(ProductRepositoryHttpImpl.class)
    ProductRepositoryHttp productRepositoryHttp;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(ProductRepositoryDiskImpl.class)
    ProductRepositoryDisk productRepositoryDisk;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(DateServiceImpl.class)
    DateService dateService;


    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void builderPages(ProductVO productVO, OnPageClickListener onPageClickListener) {
        List<Page> pageList = new ArrayList<>();
        if ((productVO.getPictures() != null) && (!productVO.getPictures().isEmpty())) {
            for (String photoUrl : productVO.getPictures()) {
                pageList.add(new Page(photoUrl, photoUrl, onPageClickListener));
            }
        }

        busProvider.getServiceBus().post(new BuilderPagesEvent(pageList));
    }

    @Override
    @Background
    public void addComment(String productId, String comment) {
        UserVO userVO = userRepositoryDisk.loadUser();
        productRepositoryHttp.addComment(productId, new CommentVO(userVO.getUserId(), userVO.getName(), comment));
    }

    @Override
    @Background
    public void validateFields(File imageOne, Bitmap bitmapOne, File fileImageTwo, Bitmap bitmapTwo, File fileImageThree, Bitmap bitmapThree, File fileImageFour, Bitmap bitmapFour, File fileImageFive, Bitmap bitmapFive, String name, String message, String price, String phone) {
        SellerProductInvalidEvent sellerProductInvalidEvent = new SellerProductInvalidEvent();

        if (imageOne == null && bitmapOne == null) {
            sellerProductInvalidEvent.setPictureInvalid(true);
        }

        if (name == null || name.isEmpty()) {
            sellerProductInvalidEvent.setNameInvalid(true);
        }

        if (message == null || message.isEmpty()) {
            sellerProductInvalidEvent.setMessageInvalid(true);
        }

        if (price == null || price.isEmpty()) {
            sellerProductInvalidEvent.setPriceInvalid(true);
        }

        if (phone == null || phone.isEmpty()) {
            sellerProductInvalidEvent.setPhoneInvalid(true);
        }

        if (sellerProductInvalidEvent.isPictureInvalid() || sellerProductInvalidEvent.isNameInvalid() || sellerProductInvalidEvent.isMessageInvalid()
                || sellerProductInvalidEvent.isPriceInvalid() || sellerProductInvalidEvent.isPhoneInvalid()) {
            busProvider.getServiceBus().post(sellerProductInvalidEvent);
        }

        productRepositoryHttp.addProduct(userRepositoryDisk.loadUser().getUserId(), new ProductVO(name, message, price, builderPictures(imageOne, bitmapOne, fileImageTwo, bitmapTwo, fileImageThree, bitmapThree, fileImageFour, bitmapFour, fileImageFive, bitmapFive)
                , new ArrayList<CommentVO>(), phone, dateService.recoverCurrentDate(), 0, false));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadAddCommentEvent(AddCommentEvent addCommentEvent) {
        UserVO userVO = userRepositoryDisk.loadUser();
        CommentVO commentVO = addCommentEvent.getCommentVO();
        commentVO.setUserPhotoUrl(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + PROFILE);
        busProvider.getServiceBus().post(addCommentEvent);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRecoverAllUsersExploreEvent(RecoverAllUsersExploreEvent recoverAllUsersExploreEvent) {
        final List<ProductVO> productVOList = userService.formatListProduct(recoverAllUsersExploreEvent.getUserVOList());
        if (!productVOList.isEmpty()) {
            busProvider.getServiceBus().post(new RecoverAllProductsEvent(productVOList));
            return;
        }
        busProvider.getServiceBus().post(new RecoverAllProductsEmptyEvent());
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadAddNewProductEvent(AddNewProductEvent addNewProductEvent) {
        final ProductVO productVO = addNewProductEvent.getProductVO();
        userRepositoryDisk.saveUser(formatUser(userRepositoryDisk.loadUser(), productVO));

        busProvider.getServiceBus().post(addNewProductEvent);
    }


    private UserVO formatUser(UserVO userVO, ProductVO productVO) {
        productVO.setUserName(userVO.getName());
        productVO.setPhotoUrl(BuildConfig.HOST + userVO.getUserId() + SEPARATOR + productVO.getId() + SEPARATOR + PRODUCT_ONE);

        if (userVO.getProductVOs() == null) {
            List<ProductVO> productVOList = new ArrayList<>();
            productVOList.add(productVO);
            userVO.setProductVOs(productVOList);
            return userVO;
        }

        List<ProductVO> productVOList = userVO.getProductVOs();
        productVOList.add(productVO);
        userVO.setProductVOs(productVOList);

        return userVO;
    }

    private List<String> builderPictures(File fileImageOne, Bitmap bitmapOne, File fileImageTwo, Bitmap bitmapTwo, File fileImageThree, Bitmap bitmapThree, File fileImageFour, Bitmap bitmapFour, File fileImageFive, Bitmap bitmapFive) {
        List<String> stringPictures = new ArrayList<>();

        stringPictures.add(fileImageOne != null ? userService.encodePicture(BitmapFactory.decodeFile(fileImageOne.getAbsolutePath())) : bitmapOne != null ? userService.encodePicture(bitmapOne) : "");
        stringPictures.add(fileImageTwo != null ? userService.encodePicture(BitmapFactory.decodeFile(fileImageTwo.getAbsolutePath())) : bitmapTwo != null ? userService.encodePicture(bitmapTwo) : "");
        stringPictures.add(fileImageThree != null ? userService.encodePicture(BitmapFactory.decodeFile(fileImageThree.getAbsolutePath())) : bitmapThree != null ? userService.encodePicture(bitmapThree) : "");
        stringPictures.add(fileImageFour != null ? userService.encodePicture(BitmapFactory.decodeFile(fileImageFour.getAbsolutePath())) : bitmapFour != null ? userService.encodePicture(bitmapFour) : "");
        stringPictures.add(fileImageFive != null ? userService.encodePicture(BitmapFactory.decodeFile(fileImageFive.getAbsolutePath())) : bitmapFive != null ? userService.encodePicture(bitmapFive) : "");

        return stringPictures;
    }
}

package br.com.iprice.domain;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import br.com.iprice.R;
import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.InvalidQuantityBuyPointsEvent;
import br.com.iprice.model.event.PointsUpdatedEvent;
import br.com.iprice.model.event.RecoverMyPointsEvent;
import br.com.iprice.model.event.ValidQuantityBuyPointsEvent;
import br.com.iprice.repository.disk.UserRepositoryDisk;
import br.com.iprice.repository.disk.UserRepositoryDiskImpl;
import br.com.iprice.repository.http.PointsRepositoryHttp;
import br.com.iprice.repository.http.PointsRepositoryHttpImpl;
import br.com.iprice.repository.http.RecoverRecordsPointsEvent;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class MyPointsServiceImpl implements MyPointsService {
    private static final String POINTS_REGISTER = "10";
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(UserServiceImpl.class)
    UserService userService;

    @Bean(UserRepositoryDiskImpl.class)
    UserRepositoryDisk userRepositoryDisk;

    @Bean(PointsRepositoryHttpImpl.class)
    PointsRepositoryHttp pointsRepositoryHttp;

    @Override
    public void registerBus() {
        busProvider.getRepositoryBus().register(this);
    }

    @Override
    public void unregisterBus() {
        busProvider.getRepositoryBus().unregister(this);
    }

    @Override
    @Background
    public void recoverMyPoints() {
        pointsRepositoryHttp.recoverRecordsPoints();
    }

    @Override
    @Background
    public void validateBuyPoints(String points) {
        final int buyPoints = Integer.parseInt(points);
        busProvider.getServiceBus().post(buyPoints < 100 ? new InvalidQuantityBuyPointsEvent() : new ValidQuantityBuyPointsEvent(buyPoints));
    }

    @Override
    public void addNewPoints(String points) {
        UserVO userVO = userRepositoryDisk.loadUser();
        userVO.setPoints(userVO.getPoints() + Integer.parseInt(points));

        pointsRepositoryHttp.updatePoints(userVO, new PointsVO(userVO.getPoints()));
    }

    @Override
    @UiThread(delay = 100)
    public void addNewPoints() {
        UserVO userVO = userRepositoryDisk.loadUser();
        userVO.setPoints(userVO.getPoints() + Integer.parseInt(POINTS_REGISTER));

        pointsRepositoryHttp.updatePoints(userVO, new PointsVO(userVO.getPoints()));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadRecoverRecordsPointsEvent(RecoverRecordsPointsEvent recoverRecordsPointsEvent) {
        busProvider.getServiceBus().post(new RecoverMyPointsEvent(userService.recoverMyUser(),
                recoverListPoints(),
                recoverRecordsPoints(recoverRecordsPointsEvent.getUserVOList())));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoadPointsUpdatedEvent(PointsUpdatedEvent pointsUpdatedEvent) {
        UserVO userVO = userRepositoryDisk.loadUser();
        userVO.setPoints(pointsUpdatedEvent.getPointsVO().getPoint());
        userRepositoryDisk.saveUser(userVO);

        busProvider.getServiceBus().post(pointsUpdatedEvent);
    }

    private int recoverRecordsPoints(List<UserVO> userVOList) {
        int recordsPoints = 0;

        if (userVOList != null && !userVOList.isEmpty()) {
            for (UserVO userVO : userVOList) {
                if (userVO.getPoints() > recordsPoints) {
                    recordsPoints = userVO.getPoints();
                }
            }
        }

        return recordsPoints;
    }

    private List<PointsVO> recoverListPoints() {
        List<PointsVO> pointsVOList = new ArrayList<>();
        pointsVOList.add(new PointsVO(PointsVO.BEGINNER, R.drawable.ic_beginner, PointsVO.POINTS_50));
        pointsVOList.add(new PointsVO(PointsVO.RESEARCHER, R.drawable.vector_search, PointsVO.POINTS_100));
        pointsVOList.add(new PointsVO(PointsVO.INTERMEDIATE, R.drawable.ic_intermediate, PointsVO.POINTS_200));
        pointsVOList.add(new PointsVO(PointsVO.TAG, R.drawable.ic_tag, PointsVO.POINTS_300));
        pointsVOList.add(new PointsVO(PointsVO.SUPER_POST, R.drawable.ic_trophy, PointsVO.POINTS_500));
        pointsVOList.add(new PointsVO(PointsVO.EXPERT, R.drawable.ic_trophy, PointsVO.POINTS_700));

        return pointsVOList;
    }
}

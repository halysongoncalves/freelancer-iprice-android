package br.com.iprice.repository.http;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface WishListRepositoryHttp {
    void recoverAllWishList();
}

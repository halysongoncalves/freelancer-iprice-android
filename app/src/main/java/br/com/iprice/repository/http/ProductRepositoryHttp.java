package br.com.iprice.repository.http;

import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.model.entities.ProductVO;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface ProductRepositoryHttp {
    void addComment(String productId, final CommentVO commentVO);

    void exploreAllProduct(String userId, double latitude, double longitude);

    void like(final ProductVO productId, int position);

    void recoverAllFeed();

    void addWishList(String userId, ProductVO productVO, final int position);

    void removeWishList(String userId, String productId, final int position);

    void searchItems(final String query);

    void addProduct(String userId, final ProductVO productVO);

    void deleteProduct(final String userId, final String productId);

    void deleteProductWishList(String userId, final String productId);
}

package br.com.iprice.repository.http;

import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface PointsRepositoryHttp {
    void recoverRecordsPoints();

    void updatePoints(UserVO userVO, final PointsVO pointsVO);
}

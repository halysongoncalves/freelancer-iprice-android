package br.com.iprice.repository.disk;

import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.iprice.model.entities.ProductVO;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class ProductRepositoryDiskImpl implements ProductRepositoryDisk {
    @Override
    public void saveProductsId(List<String> productId) {
        Hawk.put(ProductVO.KEY, productId);
    }

    @Override
    public List<String> loadProductsId() {
        return Hawk.get(ProductVO.KEY, null);
    }

    @Override
    public void clear() {
        Hawk.remove(ProductVO.KEY);
    }
}

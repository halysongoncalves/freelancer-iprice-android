package br.com.iprice.repository.http;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.model.event.AddParticipateSortEvent;
import br.com.iprice.model.event.AllAvailableSortEvent;
import br.com.iprice.model.event.AllParticipateSortEvent;
import br.com.iprice.model.event.RefreshAvailableSortEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.repository.ws.WebService;
import br.com.iprice.repository.ws.WebServiceImpl;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class SortRepositoryHttpImpl implements SortRepositoryHttp {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(WebServiceImpl.class)
    WebService webService;

    @Override
    public void recoverAllAvailableSort() {
        webService.getInstance().availableSort().enqueue(new CallbackRequest<List<SortVO>>() {
            @Override
            protected void success(Response<List<SortVO>> response) {
                busProvider.getRepositoryBus().post(new AllAvailableSortEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<SortVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void recoverAllSortParticipate() {
        webService.getInstance().participateSort().enqueue(new CallbackRequest<List<SortVO>>() {
            @Override
            protected void success(Response<List<SortVO>> response) {
                busProvider.getRepositoryBus().post(new AllParticipateSortEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<SortVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void participateSort(String sortId, SortVO sortVO) {
        webService.getInstance().participateSort(sortId, sortVO).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new AddParticipateSortEvent());
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void refreshAvailableSort() {
        webService.getInstance().availableSort().enqueue(new CallbackRequest<List<SortVO>>() {
            @Override
            protected void success(Response<List<SortVO>> response) {
                busProvider.getRepositoryBus().post(new RefreshAvailableSortEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<SortVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }
}

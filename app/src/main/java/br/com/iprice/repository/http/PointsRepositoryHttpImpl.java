package br.com.iprice.repository.http;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.PointsUpdatedEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.repository.ws.WebService;
import br.com.iprice.repository.ws.WebServiceImpl;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class PointsRepositoryHttpImpl implements PointsRepositoryHttp {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(WebServiceImpl.class)
    WebService webService;

    @Override
    public void recoverRecordsPoints() {
        webService.getInstance().recoverRecordsPoints().enqueue(new CallbackRequest<List<UserVO>>() {
            @Override
            protected void success(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new RecoverRecordsPointsEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void updatePoints(UserVO userVO, final PointsVO pointsVO) {
        webService.getInstance().updatePoints(userVO.getUserId(), pointsVO).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new PointsUpdatedEvent(pointsVO));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }
}

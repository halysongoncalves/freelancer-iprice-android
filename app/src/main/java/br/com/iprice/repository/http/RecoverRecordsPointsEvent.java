package br.com.iprice.repository.http;

import java.util.List;

import br.com.iprice.model.entities.UserVO;

public class RecoverRecordsPointsEvent {
    private final List<UserVO> userVOList;

    public RecoverRecordsPointsEvent(List<UserVO> userVOList) {
        this.userVOList = userVOList;
    }

    public List<UserVO> getUserVOList() {
        return userVOList;
    }
}

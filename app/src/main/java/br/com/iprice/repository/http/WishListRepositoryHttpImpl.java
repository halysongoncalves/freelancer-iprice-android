package br.com.iprice.repository.http;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.RecoverAllUsersWishListEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.repository.ws.WebService;
import br.com.iprice.repository.ws.WebServiceImpl;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class WishListRepositoryHttpImpl implements WishListRepositoryHttp {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(WebServiceImpl.class)
    WebService webService;

    @Override
    public void recoverAllWishList() {
        webService.getInstance().allWishList().enqueue(new CallbackRequest<List<UserVO>>() {
            @Override
            protected void success(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new RecoverAllUsersWishListEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }
}

package br.com.iprice.repository.http;

import android.os.Bundle;

import com.facebook.AccessToken;

import br.com.iprice.model.entities.DeviceVO;
import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface UserRepositoryHttp {
    void login(UserVO userVO);

    void loginFacebook(UserVO userVO);

    void register(UserVO userVO);

    void recoverUserFacebook(AccessToken accessToken, Bundle bundleParameters);

    void recoverMyPost(UserVO userVO);

    void registerDeviceUser(String userId, DeviceVO deviceVO);
}

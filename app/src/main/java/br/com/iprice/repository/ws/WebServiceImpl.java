package br.com.iprice.repository.ws;


import android.content.Context;

import com.github.aurae.retrofit2.LoganSquareConverterFactory;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.iprice.BuildConfig;
import br.com.iprice.common.okhttp.OkHttp;
import br.com.iprice.common.okhttp.OkHttpImpl;
import retrofit2.Retrofit;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class WebServiceImpl implements WebService {
    private static WebServiceApi webServiceApi;
    private static Retrofit retrofit;

    @RootContext
    Context context;

    @Bean(OkHttpImpl.class)
    OkHttp okHttp;

    @AfterInject
    void afterInject() {
        if (retrofit == null || webServiceApi == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.HOST)
                    .addConverterFactory(LoganSquareConverterFactory.create())
                    .client(okHttp.getInstance())
                    .build();

            webServiceApi = retrofit.create(WebServiceApi.class);
        }
    }

    @Override
    public WebServiceApi getInstance() {
        return webServiceApi;
    }
}

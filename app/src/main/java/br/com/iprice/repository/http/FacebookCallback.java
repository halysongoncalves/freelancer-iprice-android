package br.com.iprice.repository.http;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.splunk.mint.Mint;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import br.com.iprice.model.event.UnexpectedErrorFacebookEvent;


/**
 * Created by halysongoncalves on 09/02/16.
 */
public abstract class FacebookCallback implements GraphRequest.GraphJSONObjectCallback {
    private static final String TAG = FacebookCallback.class.getSimpleName();

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        try {
            HttpURLConnection httpURLConnection = response.getConnection();
            if (httpURLConnection != null) {
                switch (httpURLConnection.getResponseCode()) {
                    case Kind.HTTP_200:
                        if (response.getJSONObject() != null && response.getRawResponse() != null) {
                            success(response.getJSONObject());
                            break;
                        }
                        failure(new UnexpectedErrorFacebookEvent());
                    default:
                        failure(new UnexpectedErrorFacebookEvent());
                        break;
                }
            }
        } catch (IOException ioException) {
            Mint.logException(TAG, "", ioException);
        }
    }

    protected abstract void success(JSONObject jsonObject);

    protected abstract void failure(UnexpectedErrorFacebookEvent unexpectedErrorFacebookEvent);

    interface Kind {
        int HTTP_200 = 200;
    }
}



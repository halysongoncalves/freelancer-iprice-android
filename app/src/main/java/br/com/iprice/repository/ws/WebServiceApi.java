package br.com.iprice.repository.ws;


import java.util.List;

import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.model.entities.DeviceVO;
import br.com.iprice.model.entities.FeedVO;
import br.com.iprice.model.entities.PointsVO;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.SortVO;
import br.com.iprice.model.entities.UserVO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface WebServiceApi {
    //=================================== POST ===================================//
    // === Login === //
    @POST("v1/usuario/login")
    Call<UserVO> login(@Body UserVO userVO);

    @POST("v1/usuarios")
    Call<UserVO> loginFacebook(@Body UserVO userVO);

    // === Register === //
    @POST("v1/usuarios")
    Call<UserVO> register(@Body UserVO userVO);

    // === Product === //
    @POST("v1/product/{productId}/comment/add")
    Call<Void> addComent(@Path("productId") String productId, @Body CommentVO commentVO);

    @POST("v1/usuario/{userId}/product/add")
    Call<Void> addProduct(@Path("userId") String userId, @Body ProductVO productVO);

    // === WishList === //
    @POST("v1/usuario/{userId}/wishlist/add")
    Call<Void> addWishList(@Path("userId") String userId, @Body ProductVO productVO);

    // === Sort === //
    @POST("v1/sorteio/usuario/{sortId}/add")
    Call<Void> participateSort(@Path("sortId") String sortId, @Body SortVO sortVO);

    // === Points === //
    @POST("v1/usuario/{userId}/pointsupdate")
    Call<Void> updatePoints(@Path("userId") String userId, @Body PointsVO pointsVO);

    // === Push === //
    @PUT("v1/usuario/{userId}")
    Call<Void> registerDevice(@Path("userId") String userId, @Body DeviceVO deviceVO);


    //=================================== GET ===================================//
    // === Feed === //
    @GET("v1/feed")
    Call<List<FeedVO>> feed();

    // === Points === //
    @GET("v1/usuarios")
    Call<List<UserVO>> recoverRecordsPoints();

    // === WishList === //
    @GET("v1/usuarios")
    Call<List<UserVO>> allWishList();

    @GET("v1/usuario/{userId}/{productId}/removewishlistitem")
    Call<Void> removeWishList(@Path("userId") String userId, @Path("productId") String productId);

    // === Product === //
    @GET("v1/usuario/{userId}/findlocation")
    Call<List<UserVO>> explore(@Path("userId") String userId, @Query("longitude") double longitude, @Query("latitude") double latitude);

    @GET("v1/produtos/search/{query}")
    Call<List<UserVO>> searchItems(@Path("query") String query);

    @GET("v1/usuario/{userId}/{productId}/removewishlistitem")
    Call<Void> deleteProductWishList(@Path("userId") String userId, @Path("productId") String productId);

    // === Sort === //
    @GET("v1/sorteios")
    Call<List<SortVO>> availableSort();

    @GET("v1/sorteios")
    Call<List<SortVO>> participateSort();

    @GET("v1/product/{productId}/likesUpdate")
    Call<Void> like(@Path("productId") String productId);


    //=================================== DELETE ===================================//
    // === Product === //
    @DELETE("v1/usuario/{userId}/{productId}/removeproduto")
    Call<Void> deleteProduct(@Path("userId") String userId, @Path("productId") String productId);
}

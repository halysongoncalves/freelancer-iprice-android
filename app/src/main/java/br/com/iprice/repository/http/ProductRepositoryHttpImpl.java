package br.com.iprice.repository.http;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.CommentVO;
import br.com.iprice.model.entities.FeedVO;
import br.com.iprice.model.entities.ProductVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.AddCommentEvent;
import br.com.iprice.model.event.AddNewProductEvent;
import br.com.iprice.model.event.AddProductWishListEvent;
import br.com.iprice.model.event.LikedProductEvent;
import br.com.iprice.model.event.RecoverAllUsersExploreEvent;
import br.com.iprice.model.event.RecoverAllUsersFeedEvent;
import br.com.iprice.model.event.RemoveProductWishListEvent;
import br.com.iprice.model.event.RemovedProductEvent;
import br.com.iprice.model.event.RemovedProductWishListEvent;
import br.com.iprice.model.event.SearchItemsEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.repository.ws.WebService;
import br.com.iprice.repository.ws.WebServiceImpl;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class ProductRepositoryHttpImpl implements ProductRepositoryHttp {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(WebServiceImpl.class)
    WebService webService;

    @Override
    public void addComment(String productId, final CommentVO commentVO) {
        webService.getInstance().addComent(productId, commentVO).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new AddCommentEvent(commentVO));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void recoverAllFeed() {
        webService.getInstance().feed().enqueue(new CallbackRequest<List<FeedVO>>() {
            @Override
            protected void success(Response<List<FeedVO>> response) {
                busProvider.getRepositoryBus().post(new RecoverAllUsersFeedEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<FeedVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void exploreAllProduct(String userId, double latitude, double longitude) {
        webService.getInstance().explore(userId, longitude, latitude).enqueue(new CallbackRequest<List<UserVO>>() {
            @Override
            protected void success(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new RecoverAllUsersExploreEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void like(final ProductVO productVO, final int position) {
        webService.getInstance().like(productVO.getId()).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new LikedProductEvent(productVO, position));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void addWishList(String userId, ProductVO productVO, final int position) {
        webService.getInstance().addWishList(userId, productVO).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new AddProductWishListEvent(position));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void removeWishList(String userId, String productId, final int position) {
        webService.getInstance().removeWishList(userId, productId).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new RemoveProductWishListEvent(position));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void searchItems(final String query) {
        webService.getInstance().searchItems(query).enqueue(new CallbackRequest<List<UserVO>>() {
            @Override
            protected void success(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new SearchItemsEvent(response.body(), query));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<List<UserVO>> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void addProduct(String userId, final ProductVO productVO) {
        webService.getInstance().addProduct(userId, productVO).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new AddNewProductEvent(productVO));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void deleteProduct(final String userId, final String productId) {
        webService.getInstance().deleteProduct(userId, productId).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new RemovedProductEvent(userId, productId));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void deleteProductWishList(String userId, final String productId) {
        webService.getInstance().deleteProductWishList(userId, productId).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
                busProvider.getRepositoryBus().post(new RemovedProductWishListEvent(productId));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<Void> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }
}

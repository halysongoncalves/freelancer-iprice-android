package br.com.iprice.repository.disk;

import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.EBean;

import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class UserRepositoryDiskImpl implements UserRepositoryDisk {
    @Override
    public void saveUser(UserVO userVO) {
        Hawk.put(UserVO.KEY, userVO);
    }

    @Override
    public UserVO loadUser() {
        return Hawk.get(UserVO.KEY, null);
    }

    @Override
    public void clear() {
        Hawk.remove(UserVO.KEY);
    }
}

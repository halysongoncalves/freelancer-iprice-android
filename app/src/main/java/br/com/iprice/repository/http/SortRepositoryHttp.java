package br.com.iprice.repository.http;

import br.com.iprice.model.entities.SortVO;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface SortRepositoryHttp {

    void recoverAllAvailableSort();

    void recoverAllSortParticipate();

    void participateSort(String sortId, SortVO sortVO);

    void refreshAvailableSort();
}

package br.com.iprice.repository.http;


import br.com.iprice.model.event.RedirectionEvent;
import br.com.iprice.model.event.http.BadRequestEvent;
import br.com.iprice.model.event.http.ConvertErrorEvent;
import br.com.iprice.model.event.http.ForbiddenEvent;
import br.com.iprice.model.event.http.GenericErrorEvent;
import br.com.iprice.model.event.http.NetworkErrorEvent;
import br.com.iprice.model.event.http.NotFoundEvent;
import br.com.iprice.model.event.http.ServerErrorEvent;
import br.com.iprice.model.event.http.UnauthorizedEvent;

/**
 * Created by halysongoncalves on 04/01/16.
 */
public interface HttpFailure {

    void onLoadNetworkErrorEvent(NetworkErrorEvent networkErrorEvent);

    void onLoadCovertErrorEvent(ConvertErrorEvent convertErrorEvent);

    void onLoadGenericErrorEventEvent(GenericErrorEvent genericErrorEvent);

    interface Redirection {
        void onLoadRedirectionEvent(RedirectionEvent redirectionEvent);
    }

    interface ClientError {
        void onLoadBadRequestEvent(BadRequestEvent badRequestEvent);

        void onLoadUnauthorizedEvent(UnauthorizedEvent unauthorizedEvent);

        void onLoadForbiddenEvent(ForbiddenEvent forbiddenEvent);

        void onLoadNotFoundEvent(NotFoundEvent notFoundEvent);
    }

    interface ServerError {
        void onLoadServerErrorEvent(ServerErrorEvent serverErrorEvent);
    }
}

package br.com.iprice.repository.http;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.json.JSONObject;

import br.com.iprice.common.bus.BusProvider;
import br.com.iprice.common.bus.BusProviderImpl;
import br.com.iprice.model.entities.DeviceVO;
import br.com.iprice.model.entities.UserVO;
import br.com.iprice.model.event.DuplicateUserErrorEvent;
import br.com.iprice.model.event.FailRecoverPostEvent;
import br.com.iprice.model.event.RecoverMyPostEvent;
import br.com.iprice.model.event.RecoverUserFacebookEvent;
import br.com.iprice.model.event.RegisterSuccessEvent;
import br.com.iprice.model.event.UnexpectedErrorFacebookEvent;
import br.com.iprice.model.event.http.HttpFailureEvent;
import br.com.iprice.model.event.http.InputOutputFailureEvent;
import br.com.iprice.repository.ws.WebService;
import br.com.iprice.repository.ws.WebServiceImpl;
import retrofit2.Response;

/**
 * Created by halysongoncalves on 12/12/15.
 */
@EBean
public class UserRepositoryHttpImpl implements UserRepositoryHttp {
    @Bean(BusProviderImpl.class)
    BusProvider busProvider;

    @Bean(WebServiceImpl.class)
    WebService webService;

    @Override
    public void login(UserVO userVO) {
        webService.getInstance().login(userVO).enqueue(new CallbackRequest<UserVO>() {
            @Override
            protected void success(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new DuplicateUserErrorEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void loginFacebook(UserVO userVO) {
        webService.getInstance().loginFacebook(userVO).enqueue(new CallbackRequest<UserVO>() {
            @Override
            protected void success(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new DuplicateUserErrorEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void register(UserVO userVO) {
        webService.getInstance().register(userVO).enqueue(new CallbackRequest<UserVO>() {
            @Override
            protected void success(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new RegisterSuccessEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new InputOutputFailureEvent(throwable));
            }

            @Override
            protected void failureHttp(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new HttpFailureEvent<>(response));
            }
        });
    }

    @Override
    public void recoverUserFacebook(final AccessToken accessToken, Bundle bundleParameters) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new FacebookCallback() {
            @Override
            protected void success(JSONObject jsonObject) {
                busProvider.getRepositoryBus().post(new RecoverUserFacebookEvent(jsonObject,accessToken.getToken()));
            }

            @Override
            protected void failure(UnexpectedErrorFacebookEvent unexpectedErrorFacebookEvent) {
                busProvider.getRepositoryBus().post(unexpectedErrorFacebookEvent);
            }
        });
        request.setParameters(bundleParameters);
        request.executeAsync();
    }

    @Override
    public void recoverMyPost(UserVO userVO) {
        webService.getInstance().login(userVO).enqueue(new CallbackRequest<UserVO>() {
            @Override
            protected void success(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new RecoverMyPostEvent(response.body()));
            }

            @Override
            protected void failure(Throwable throwable) {
                busProvider.getRepositoryBus().post(new FailRecoverPostEvent());
            }

            @Override
            protected void failureHttp(Response<UserVO> response) {
                busProvider.getRepositoryBus().post(new FailRecoverPostEvent());
            }
        });
    }

    @Override
    public void registerDeviceUser(String userId, DeviceVO deviceVO) {
        webService.getInstance().registerDevice(userId, deviceVO).enqueue(new CallbackRequest<Void>() {
            @Override
            protected void success(Response<Void> response) {
            }

            @Override
            protected void failure(Throwable throwable) {
            }

            @Override
            protected void failureHttp(Response<Void> response) {
            }
        });
    }
}

package br.com.iprice.repository.disk;

import br.com.iprice.model.entities.UserVO;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface UserRepositoryDisk {
    void saveUser(UserVO userVO);

    UserVO loadUser();

    void clear();
}

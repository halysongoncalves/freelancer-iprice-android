package br.com.iprice.repository.disk;

import java.util.List;

/**
 * Created by halysongoncalves on 12/12/15.
 */
public interface ProductRepositoryDisk {
    void saveProductsId(List<String> likeId);

    List<String> loadProductsId();

    void clear();


}

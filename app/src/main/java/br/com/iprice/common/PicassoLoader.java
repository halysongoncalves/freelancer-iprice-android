package br.com.iprice.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EBean;

import br.com.iprice.R;
import cn.lightsky.infiniteindicator.loader.ImageLoader;

/**
 * Created by halysongoncalves on 26/07/16.
 */

@EBean
public class PicassoLoader implements ImageLoader {
    @Override
    public void initLoader(Context context) {
    }

    @Override
    public void load(Context context, ImageView targetView, Object photoUrl) {
        if (photoUrl == null) {
            return;
        }

        final Drawable drawableProduct = ContextCompat.getDrawable(context, R.drawable.placeholder_image);

        Picasso.with(context)
                .load((String) photoUrl)
                .resize(drawableProduct.getIntrinsicWidth(), drawableProduct.getIntrinsicHeight())
                .centerCrop()
                .placeholder(R.drawable.placeholder_image)
                .into(targetView);

    }
}

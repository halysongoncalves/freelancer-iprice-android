package br.com.iprice.common.bus;

/**
 * Created by Halyson on 11/05/16.
 */
public interface Bus {
    void registerBus();

    void unregisterBus();
}

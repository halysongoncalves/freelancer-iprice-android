package br.com.iprice.common.okhttp;


import android.content.Context;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.File;
import java.util.concurrent.TimeUnit;

import br.com.iprice.BuildConfig;
import br.com.iprice.repository.ws.GzipRequestInterceptor;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by halysongoncalves on 21/01/16.
 */
@EBean(scope = EBean.Scope.Singleton)
public class OkHttpImpl implements OkHttp {
    private static final long TIME_OUT = 60;
    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024;
    private static final String CACHE_NAME = "HTTP";
    private static OkHttpClient okHttpClient = null;

    @RootContext
    Context context;

    @Bean
    GzipRequestInterceptor gzipRequestInterceptor;

    @AfterInject
    void afterInject() {
        builderOkHttpClient(context.getCacheDir(), CACHE_NAME, DISK_CACHE_SIZE);
    }

    @Override
    public OkHttpClient getInstance() {
        return okHttpClient;
    }

    private void builderOkHttpClient(File cacheDir, String cacheName, int cacheSize) {
        okHttpClient = new OkHttpClient.Builder()
                .cache(new Cache(new File(cacheDir, cacheName), cacheSize))
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                .followRedirects(false)
                .retryOnConnectionFailure(true)
                .addInterceptor(gzipRequestInterceptor)
                .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG
                        ? HttpLoggingInterceptor.Level.BODY
                        : HttpLoggingInterceptor.Level.NONE))
                .build();
    }
}

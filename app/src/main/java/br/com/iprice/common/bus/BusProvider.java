package br.com.iprice.common.bus;


import org.greenrobot.eventbus.EventBus;

public interface BusProvider {
    EventBus getServiceBus();

    EventBus getRepositoryBus();
}

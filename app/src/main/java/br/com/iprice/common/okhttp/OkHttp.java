package br.com.iprice.common.okhttp;


import okhttp3.OkHttpClient;

/**
 * Created by halysongoncalves on 21/01/16.
 */
public interface OkHttp {
    OkHttpClient getInstance();

}

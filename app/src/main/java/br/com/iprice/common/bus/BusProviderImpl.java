package br.com.iprice.common.bus;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.EventBus;

@EBean(scope = EBean.Scope.Singleton)
public class BusProviderImpl implements BusProvider {
    private EventBus serviceBus;
    private EventBus repositoryBus;

    @AfterInject
    void afterInject() {
        serviceBus = EventBus.builder().throwSubscriberException(true).build();
        repositoryBus = EventBus.builder().throwSubscriberException(true).build();
    }

    @Override
    public EventBus getServiceBus() {
        return serviceBus;
    }

    @Override
    public EventBus getRepositoryBus() {
        return repositoryBus;
    }

}